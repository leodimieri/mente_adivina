import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreeGameViewComponent } from './free-game-view.component';

describe('FreeGameInfoViewComponent', () => {
  let component: FreeGameViewComponent;
  let fixture: ComponentFixture<FreeGameViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeGameViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeGameViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
