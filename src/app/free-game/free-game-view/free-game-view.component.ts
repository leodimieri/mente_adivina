import { Component, OnInit } from '@angular/core';
import {GameService} from '../../shared/services/game.service';
import {AudioService} from '../../shared/services/audio.service';
import {CardName} from '../../shared/models/cards/card';

@Component({
  selector: 'app-free-game-view',
  templateUrl: './free-game-view.component.html',
  styleUrls: ['./free-game-view.component.scss']
})
export class FreeGameViewComponent implements OnInit {
  public currentGueswordText: string;
  public currentPhraseText: string;
  public currentSpellingText: string;
  public currentRiddleText: string;
  constructor(public game: GameService, private audio: AudioService) {
  }

  ngOnInit() {
    this.setTexts();
  }

  public onClickRiddle() {
    this.audio.playClick();
    this.game.generateRiddle();
  }
  public onClickSpelling() {
    this.audio.playClick();
    this.game.generateSpelling();
  }
  public onClickGuessword() {
    this.audio.playClick();
    this.game.generateGuessword();
  }
  public onClickPhrases() {
    this.audio.playClick();
    this.game.generatePhrase();
  }

  public onClickRandom() {
    this.audio.playClick();
    const random = Math.random();
    if (random < 0.25) {
      this.game.generateRiddle();
    } else if (random < 0.5) {
      this.game.generateSpelling();
    } else if (random < 0.75) {
      this.game.generateGuessword();
    } else {
      this.game.generatePhrase();
    }
  }

  private setTexts() {
    this.currentSpellingText = CardName.Deletreo;
    switch (this.game.level) {
      case 1:
        this.currentGueswordText = CardName.AdivinaPalabra;
        this.currentPhraseText = CardName.JugandoConPalabras;
        this.currentRiddleText = CardName.Adivinanzas;
        break;
      case 2:
        this.currentGueswordText = CardName.AdivinaPalabra;
        this.currentPhraseText = CardName.FrasesHechas;
        this.currentRiddleText = CardName.Adivinanzas;
        break;
      default:
        this.currentGueswordText = CardName.DescubreElObjeto;
        this.currentPhraseText = CardName.Refranes;
        this.currentRiddleText = CardName.Acertijos;
        break;
    }
  }
}
