import { NgModule } from '@angular/core';
import { FreeGameViewComponent } from './free-game-view/free-game-view.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [FreeGameViewComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: FreeGameViewComponent
      }
    ])
  ]
})
export class FreeGameModule { }
