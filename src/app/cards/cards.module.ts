import { NgModule } from '@angular/core';
import { CardHeaderComponent } from './components/card-header/card-header.component';
import {SharedModule} from '../shared/shared.module';
import { CharacterActionComponent } from './components/character-action/character-action.component';
import {RouterModule} from '@angular/router';
import { SpellingCardComponent } from './components/spelling-card/spelling-card.component';
import { RiddleCardComponent } from './components/riddle-card/riddle-card.component';
import { GuesswordCardComponent } from './components/guessword-card/guessword-card.component';
import { CardViewComponent } from './components/card-view/card-view.component';
import { WrongButtonComponent } from './components/wrong-button/wrong-button.component';
import { CorrectButtonComponent } from './components/correct-button/correct-button.component';
import { AnswerButtonComponent } from './components/answer-button/answer-button.component';
import { OptionsButtonComponent } from './components/options-button/options-button.component';
import { PhraseCardComponent } from './components/phrase-card/phrase-card.component';
import { CardDirective } from './directives/card.directive';
import {AngularFittextModule} from 'angular-fittext';
import {ExitFromCardGuard} from '../guards/exit-from-card.guard';

@NgModule({
  declarations: [CardHeaderComponent, CharacterActionComponent, SpellingCardComponent, RiddleCardComponent,
    GuesswordCardComponent, CardViewComponent, WrongButtonComponent, CorrectButtonComponent, AnswerButtonComponent, OptionsButtonComponent, PhraseCardComponent, CardDirective],
  imports: [
    SharedModule,
    AngularFittextModule,
    RouterModule.forChild([
      {
        path: '',
        component: CardViewComponent,
        canDeactivate: [ExitFromCardGuard],
        children: [
          {
            path: 'riddle',
            component: RiddleCardComponent
          },
          {
            path: 'spelling',
            component: SpellingCardComponent
          },
          {
            path: 'phrase',
            component: PhraseCardComponent
          },
          {
            path: 'guessword',
            component: GuesswordCardComponent
          }
        ]
      }
    ])
  ]
})
export class CardsModule { }
