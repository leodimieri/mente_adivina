import {Directive, OnInit} from '@angular/core';
import {CardService} from '../../shared/services/card.service';
import {GameService} from '../../shared/services/game.service';
import {Router} from '@angular/router';
import {Card} from '../../shared/models/cards/card';
import {AudioService} from '../../shared/services/audio.service';
import {timer} from 'rxjs';
import {take} from 'rxjs/operators';
import {FirebaseAnalytics} from '@ionic-native/firebase-analytics/ngx';

@Directive({
  selector: '[appCard]'
})
export class CardDirective implements OnInit {

  public pointsToReward: number;
  public card: Card;
  public answered: boolean;

  constructor(public cardService: CardService, public gameService: GameService, public router: Router, public audio: AudioService,
              private firebaseAnalytics: FirebaseAnalytics) { }

  ngOnInit() {
    this.card = this.cardService.currentCard;
    this.cardService.shoeExtraTexts.next(false);
    this.pointsToReward = 2;
  }

  correctPointsAnimationAndReturnToGame() {
    this.cardService.playStarAnimation.emit(this.pointsToReward);
    if (this.pointsToReward === 1) {
      this.audio.playSfx('476178__unadamlar__correct-choice.wav');
    } else {
      this.audio.playSfx('131660__bertrof__game-sound-correct.wav');
    }
    timer(2000).pipe(take(1)).subscribe(value => {
      this.returnToGame();
    });
  }
  addPoints() {
    // this.firebaseAnalytics.logEvent('solved_card', {'game_id': this.gameService.gameId, 'card_name': this.card.cardName,
    //                                                               'game_mode': this.gameService.gameMode,
    //                                                               'level': this.card.level, 'points': this.pointsToReward});
    this.gameService.addPoints(this.pointsToReward);
  }

  wrongAnimation() {
    this.audio.playSfx('131657__bertrof__game-sound-wrong.wav');
  }

  onClickOptions() {
    if (this.answered) {
      return;
    }
    this.audio.playClick();
    this.pointsToReward--;
  }
  returnToGame() {
    this.cardService.currentCardIsAnswered = true;
    this.gameService.backToGame();
  }
}
