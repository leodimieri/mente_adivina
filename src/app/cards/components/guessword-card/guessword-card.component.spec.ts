import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuesswordCardComponent } from './guessword-card.component';

describe('GuesswordCardComponent', () => {
  let component: GuesswordCardComponent;
  let fixture: ComponentFixture<GuesswordCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuesswordCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuesswordCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
