import { Component, OnInit } from '@angular/core';
import {CardService} from '../../../shared/services/card.service';
import {GameService} from '../../../shared/services/game.service';
import {Router} from '@angular/router';
import {CardDirective} from '../../directives/card.directive';
import {AudioService} from '../../../shared/services/audio.service';
import {FirebaseAnalytics} from '@ionic-native/firebase-analytics/ngx';

@Component({
  selector: 'app-guessword-card',
  templateUrl: './guessword-card.component.html',
  styleUrls: ['./guessword-card.component.scss']
})
export class GuesswordCardComponent extends CardDirective implements OnInit {


  constructor(public cardService: CardService, public gameService: GameService, public router: Router, public audio: AudioService,
              firebaseAnalytics: FirebaseAnalytics) {
    super(cardService, gameService, router, audio, firebaseAnalytics);
  }

  ngOnInit() {
    super.ngOnInit();

  }

  correctAnswer() {
    if (this.answered) {
      return;
    }
    this.answered = true;
    this.addPoints();
    super.correctPointsAnimationAndReturnToGame();
  }

  wrongAnswer() {
    if (this.answered) {
      return;
    }
    this.answered = true;
    super.wrongAnimation();
    super.returnToGame();
  }

  onClickOptions() {
    super.onClickOptions();
  }
}
