import { Component, OnInit } from '@angular/core';
import {GameService} from '../../../shared/services/game.service';
import {CardService} from '../../../shared/services/card.service';
import {CardDirective} from '../../directives/card.directive';
import {Router} from '@angular/router';
import {AudioService} from '../../../shared/services/audio.service';
import {FirebaseAnalytics} from '@ionic-native/firebase-analytics/ngx';
@Component({
  selector: 'app-riddle-card',
  templateUrl: './riddle-card.component.html',
  styleUrls: ['./riddle-card.component.scss']
})
export class RiddleCardComponent extends CardDirective implements OnInit {

  constructor(public cardService: CardService, public router: Router, public gameService: GameService, public audio: AudioService,
              firebaseAnalytics: FirebaseAnalytics) {
    super(cardService, gameService, router, audio, firebaseAnalytics);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  correctAnswer() {
    if (this.answered) {
      return;
    }
    this.answered = true;
    super.addPoints();
    super.correctPointsAnimationAndReturnToGame();
  }

  wrongAnswer() {
    if (this.answered) {
      return;
    }
    this.answered = true;
    super.wrongAnimation();
    super.returnToGame();
  }

  onClickOptions() {
    super.onClickOptions();
  }
}
