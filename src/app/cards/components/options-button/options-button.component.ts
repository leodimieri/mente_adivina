import {Component, HostListener, OnInit} from '@angular/core';
import {GameService} from '../../../shared/services/game.service';
import {CardService} from '../../../shared/services/card.service';

@Component({
  selector: 'app-options-button',
  templateUrl: './options-button.component.html',
  styleUrls: ['./options-button.component.scss']
})
export class OptionsButtonComponent implements OnInit {

  @HostListener('click', ['$event.target'])
  onClick(btn) {
    this.cardService.shoeExtraTexts.next(true);
  }

  constructor(private cardService: CardService) { }

  ngOnInit() {
  }

}
