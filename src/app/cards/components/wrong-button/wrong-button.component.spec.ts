import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WrongButtonComponent } from './wrong-button.component';

describe('WrongButtonComponent', () => {
  let component: WrongButtonComponent;
  let fixture: ComponentFixture<WrongButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WrongButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrongButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
