import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../../shared/models/player';
import {GameService} from '../../../shared/services/game.service';

@Component({
  selector: 'app-character-action',
  templateUrl: './character-action.component.html',
  styleUrls: ['./character-action.component.scss']
})
export class CharacterActionComponent implements OnInit {

  @Input()
  public player: Player;
  @Input()
  public color: string;
  @Input()
  public action: string;
  constructor(public game: GameService) { }

  ngOnInit() {
  }

}
