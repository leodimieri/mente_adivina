import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorrectButtonComponent } from './correct-button.component';

describe('CorrectButtonComponent', () => {
  let component: CorrectButtonComponent;
  let fixture: ComponentFixture<CorrectButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorrectButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorrectButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
