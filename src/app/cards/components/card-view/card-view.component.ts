import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {GameService} from '../../../shared/services/game.service';
import {CardService} from '../../../shared/services/card.service';
import {Router} from '@angular/router';
import {NavController} from '@ionic/angular';
import {CardDirective} from '../../directives/card.directive';
import {take} from 'rxjs/operators';
import anime from 'animejs';
import {Subscription, timer} from 'rxjs';

@Component({
  selector: 'app-card-view',
  templateUrl: './card-view.component.html',
  styleUrls: ['./card-view.component.scss']
})
export class CardViewComponent implements OnInit, OnDestroy {

  public showHalfStars: boolean;
  public showAllStars: boolean;
  public starSubscription: Subscription;
  @ViewChild(CardDirective, {static: true}) public card: CardDirective;
  constructor(public gameService: GameService, public cardService: CardService) { }

  ngOnInit() {
    this.starSubscription = this.cardService.playStarAnimation.pipe(take(1)).subscribe(value => {
      if (value === 1) {
        this.showHalfStars = true;
      } else {
        this.showAllStars = true;
      }
      timer(100).subscribe(value1 => {
        anime({
          targets: '.stars path',
          easing: 'linear',
          duration: 500,
          direction: 'alternate',
          rotate: [0, function(el, i, l) {
            return (Math.random() < 0.5 ? 1 : -1) * (Math.random() * 2);
          }],
          scale: [1, .95],
          autoplay: true,
          loop: 5
        });
      });

    });
  }

  canDeactivate(): boolean {
    return this.cardService.currentCardIsAnswered;
  }

    ngOnDestroy(): void {
      if (this.starSubscription) {
          this.starSubscription.unsubscribe();
      }

    }
}
