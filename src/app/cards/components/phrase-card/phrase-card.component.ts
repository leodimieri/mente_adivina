import { Component, OnInit } from '@angular/core';
import {Card} from '../../../shared/models/cards/card';
import {CardService} from '../../../shared/services/card.service';
import {CardDirective} from '../../directives/card.directive';
import {GameService} from '../../../shared/services/game.service';
import {Router} from '@angular/router';
import {AudioService} from '../../../shared/services/audio.service';
import {FirebaseAnalytics} from '@ionic-native/firebase-analytics/ngx';

@Component({
  selector: 'app-phrase-card',
  templateUrl: './phrase-card.component.html',
  styleUrls: ['./phrase-card.component.scss']
})
export class PhraseCardComponent extends CardDirective implements OnInit {

  constructor(public cardService: CardService, public router: Router, public gameService: GameService, public audio: AudioService,
              firebaseAnalytics: FirebaseAnalytics) {
    super(cardService, gameService, router, audio, firebaseAnalytics);
  }

  ngOnInit() {
    super.ngOnInit();

  }
  correctAnswer() {
    if (this.answered) {
      return;
    }
    this.answered = true;
    this.addPoints();
    super.correctPointsAnimationAndReturnToGame();
  }

  wrongAnswer() {
    if (this.answered) {
      return;
    }
    this.answered = true;
    super.wrongAnimation();
    super.returnToGame();
  }

  onClickOptions() {
    super.onClickOptions();
  }
}
