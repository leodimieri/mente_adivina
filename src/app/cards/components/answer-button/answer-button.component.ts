import {Component, HostListener, Input, OnInit} from '@angular/core';
import {AudioService} from '../../../shared/services/audio.service';
import {CardService} from '../../../shared/services/card.service';

@Component({
  selector: 'app-answer-button',
  templateUrl: './answer-button.component.html',
  styleUrls: ['./answer-button.component.scss']
})
export class AnswerButtonComponent implements OnInit {
  constructor(private audio: AudioService, private card: CardService) { }

  @Input()
  public answer: string;

  @Input()
  public cardColor: string;

  public show: boolean;

  @HostListener('click', ['$event.target'])
  onClick(btn) {
    this.audio.playClick();
    this.show = !this.show;
    this.card.currentAnswerIsViewed = true;
  }

  ngOnInit() {
    this.show = false;
  }

}
