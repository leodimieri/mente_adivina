import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpellingCardComponent } from './spelling-card.component';

describe('SpellingCardComponent', () => {
  let component: SpellingCardComponent;
  let fixture: ComponentFixture<SpellingCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpellingCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpellingCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
