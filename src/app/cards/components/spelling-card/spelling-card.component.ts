import { Component, OnInit } from '@angular/core';
import {Card} from '../../../shared/models/cards/card';
import {CardService} from '../../../shared/services/card.service';
import {CardDirective} from '../../directives/card.directive';
import {GameService} from '../../../shared/services/game.service';
import {Router} from '@angular/router';
import {AudioService} from '../../../shared/services/audio.service';
import {timer} from 'rxjs';
import {take} from 'rxjs/operators';
import {FirebaseAnalytics} from '@ionic-native/firebase-analytics/ngx';

@Component({
  selector: 'app-spelling-card',
  templateUrl: './spelling-card.component.html',
  styleUrls: ['./spelling-card.component.scss']
})
export class SpellingCardComponent extends CardDirective implements OnInit {

  public currentWordIndex: number;
  constructor(public cardService: CardService, public router: Router, public gameService: GameService, public audio: AudioService,
              firebaseAnalytics: FirebaseAnalytics) {
    super(cardService, gameService, router, audio, firebaseAnalytics);
  }

  ngOnInit() {
    super.ngOnInit();
    // la primera siempre es lvl 2
    this.currentWordIndex = 1;
  }


  correctAnswer() {
    if (this.answered) {
      return;
    }
    this.answered = true;
    if (this.currentWordIndex === 1) {
      this.currentWordIndex = 2;
      timer(2000).pipe(take(1)).subscribe(value => {
        this.answered = false;
      });
      this.audio.playSfx('476178__unadamlar__correct-choice.wav');
    } else {
      super.correctPointsAnimationAndReturnToGame();
      super.addPoints();
    }
  }

  wrongAnswer() {
    if (this.answered) {
      return;
    }
    this.answered = true;
    this.pointsToReward--;
    if (this.currentWordIndex === 1) {
      this.currentWordIndex = 0;
      timer(2000).pipe(take(1)).subscribe(value => {
        this.answered = false;
      });
    } else {
      if (this.pointsToReward > 0) {
        super.addPoints();
        this.correctPointsAnimationAndReturnToGame();
      } else {
        super.returnToGame();
      }
    }
    super.wrongAnimation();
  }
}
