import { NgModule } from '@angular/core';
import { FreeGameInfoViewComponent } from './free-game-info-view/free-game-info-view.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [FreeGameInfoViewComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: FreeGameInfoViewComponent
      }
    ])
  ]
})
export class FreeGameInfoModule { }
