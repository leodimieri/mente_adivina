import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreeGameInfoViewComponent } from './free-game-info-view.component';

describe('FreeGameInfoViewComponent', () => {
  let component: FreeGameInfoViewComponent;
  let fixture: ComponentFixture<FreeGameInfoViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeGameInfoViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeGameInfoViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
