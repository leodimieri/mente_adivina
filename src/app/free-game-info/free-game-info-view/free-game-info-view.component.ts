import { Component, OnInit } from '@angular/core';
import {AudioService} from '../../shared/services/audio.service';
import {Router} from '@angular/router';
import {GameService} from '../../shared/services/game.service';

@Component({
  selector: 'app-free-game-info-view',
  templateUrl: './free-game-info-view.component.html',
  styleUrls: ['./free-game-info-view.component.scss']
})
export class FreeGameInfoViewComponent implements OnInit {

  constructor(public audio: AudioService, public router: Router, public game: GameService) { }

  ngOnInit() {
  }

  public onClickOk() {
    this.audio.playClick();
    this.game.setMode('free');
  }

}
