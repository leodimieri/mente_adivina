import {Directive, HostListener} from '@angular/core';
import {Country} from '../../shared/models/language.enum';
import {LanguageService} from '../../shared/services/language.service';
import {AudioService} from '../../shared/services/audio.service';
import {Router} from '@angular/router';

@Directive({
  selector: '[appLanguageButton]'
})
export class LanguageButtonDirective {
  protected country: Country;
  @HostListener('click') private setCountry() {
    this.sound.playClick();
    this.languageService.currentCountry = this.country;
    this.router.navigate(['cover']);
  }
  constructor(private languageService: LanguageService, private sound: AudioService,
              private router: Router) {
  }

}
