import {Component, OnInit} from '@angular/core';
import {LanguageButtonDirective} from '../language-button.directive';
import {Country} from '../../../shared/models/language.enum';

@Component({
  selector: 'app-spanish-button',
  templateUrl: './spanish-button.component.html',
  styleUrls: ['./spanish-button.component.scss'],
})
export class SpanishButtonComponent extends LanguageButtonDirective implements OnInit {

  ngOnInit() {
    this.country = Country.Spanish;
  }

}
