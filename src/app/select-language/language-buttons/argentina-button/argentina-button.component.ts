import { Component, OnInit } from '@angular/core';
import {Country} from '../../../shared/models/language.enum';
import {LanguageButtonDirective} from '../language-button.directive';

@Component({
  selector: 'app-argentina-button',
  templateUrl: './argentina-button.component.html',
  styleUrls: ['./argentina-button.component.scss'],
})
export class ArgentinaButtonComponent extends LanguageButtonDirective implements OnInit {

  ngOnInit() {
    this.country = Country.Argentina;
  }

}
