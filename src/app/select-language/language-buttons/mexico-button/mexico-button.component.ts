import { Component, OnInit } from '@angular/core';
import {Country} from '../../../shared/models/language.enum';
import {LanguageButtonDirective} from '../language-button.directive';

@Component({
  selector: 'app-mexico-button',
  templateUrl: './mexico-button.component.html',
  styleUrls: ['./mexico-button.component.scss'],
})
export class MexicoButtonComponent extends LanguageButtonDirective implements OnInit {

  ngOnInit() {
    this.country = Country.Mexico;
  }

}
