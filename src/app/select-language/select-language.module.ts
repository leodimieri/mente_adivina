import { NgModule } from '@angular/core';
import {SelectLanguageComponent} from './select-language/select-language.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import { LanguageButtonDirective } from './language-buttons/language-button.directive';
import {ArgentinaButtonComponent} from './language-buttons/argentina-button/argentina-button.component';
import {SpanishButtonComponent} from './language-buttons/spanish-button/spanish-button.component';
import {MexicoButtonComponent} from './language-buttons/mexico-button/mexico-button.component';

@NgModule({
  declarations: [SelectLanguageComponent, LanguageButtonDirective, ArgentinaButtonComponent,
                  SpanishButtonComponent, MexicoButtonComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: SelectLanguageComponent
      }
    ])

  ]
})
export class SelectLanguageModule { }
