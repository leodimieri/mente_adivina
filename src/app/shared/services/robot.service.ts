import { Injectable } from '@angular/core';
import {Robot} from '../models/robot';

@Injectable({
    providedIn: 'root'
})
export class RobotService {

    public robotsByLevel: Map<number, Robot[]>;
    constructor() {
    }

    generateRobots() {
        this.robotsByLevel = new Map();
        this.robotsByLevel.set(1, [
            new Robot(1, 'assets/images/robots/level1/1.svg'),
            new Robot(2, 'assets/images/robots/level1/2.svg'),
            new Robot(3, 'assets/images/robots/level1/3.svg'),
            new Robot(4, 'assets/images/robots/level1/4.svg'),
            new Robot(5, 'assets/images/robots/level1/5.svg')
        ]);
        this.robotsByLevel.set(2, [
            new Robot(1, 'assets/images/robots/level2/6.svg'),
            new Robot(2, 'assets/images/robots/level2/7.svg'),
            new Robot(3, 'assets/images/robots/level2/8.svg'),
            new Robot(4, 'assets/images/robots/level2/9.svg'),
            new Robot(5, 'assets/images/robots/level2/10.svg')
        ]);
        this.robotsByLevel.set(3, [
            new Robot(1, 'assets/images/robots/level3/11.svg'),
            new Robot(2, 'assets/images/robots/level3/12.svg'),
            new Robot(3, 'assets/images/robots/level3/13.svg'),
            new Robot(4, 'assets/images/robots/level3/14.svg'),
            new Robot(5, 'assets/images/robots/level3/15.svg')
        ]);
    }
}
