import {Injectable, OnDestroy} from '@angular/core';
import {Howl} from 'howler';
import {SettingsService} from '../../settings/services/settings.service';
import {Subscription} from 'rxjs';
import {NativeAudio} from '@ionic-native/native-audio/ngx';
import {Platform} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AudioService implements OnDestroy {
  private sfx: Howl;
  private claps: Howl;
  private click: Howl;
  private htmlAudioElement: HTMLAudioElement;

  public musicSubscription: Subscription;

  constructor(private settings: SettingsService, private nativeAudio: NativeAudio, private platform: Platform) {
    this.platform.ready().then(() => {
      this.htmlAudioElement = document.createElement('audio');
      this.htmlAudioElement.loop = true;
      this.htmlAudioElement.src = 'assets/audio/music/PodingtonBear04SquirrelCommotion.mp3';
      this.htmlAudioElement.load();
      this.musicSubscription = this.settings.onChangeMusic.subscribe(value => {
        if (value) {
          this.playMusic();
        } else {
          this.pauseMusic();
        }
      });
      this.claps = new Howl({
        src: ['assets/audio/sounds/cheer5.mp3'],
        autoplay: false,
        volume: 1,
        loop: true,
        html5: true,
        preload: true
      });
    });
  }

  public playMusic() {
    this.htmlAudioElement.play();
  }

  public pauseMusic() {
    this.htmlAudioElement.pause();
  }

  public playClick() {
    if (this.settings.onChangeSfx.value) {
      if (this.click) {
        this.click.play();
      } else {
        this.click = new Howl({
          src: ['assets/audio/sounds/click.mp3'],
          autoplay: true,
          volume: 1
        });
      }
    }
  }
  public playClaps() {
    if (this.settings.onChangeSfx.value) {
      if (this.settings.onChangeSfx.value) {
        if (this.sfx && this.sfx !== this.claps) {
          this.sfx.unload();
        } else if (this.sfx) {
          this.sfx.stop();
        }
        this.sfx = this.claps;
        this.sfx.play();
      }

    }
  }
  public playSfx(path: string, loop: boolean = false) {
    if (this.settings.onChangeSfx.value) {
      if (this.sfx) {
        this.sfx.unload();
      }
      this.sfx = new Howl({
        src: ['assets/audio/sounds/' + path],
        autoplay: true,
        volume: 1,
        loop: loop
      });
    }
  }

  public pauseSfx() {
    if (this.sfx) {
      this.sfx.pause();
    }
  }

  ngOnDestroy(): void {
    this.musicSubscription.unsubscribe();
  }

}
