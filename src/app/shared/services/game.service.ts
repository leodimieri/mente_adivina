import {EventEmitter, Injectable} from '@angular/core';
import {Player} from '../models/player';
import {Router} from '@angular/router';
import {CardService} from './card.service';
import {CardType} from '../models/cards/card';
import {timer} from 'rxjs';
import {take} from 'rxjs/operators';
import {AudioService} from './audio.service';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import {Storage} from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})
export class GameService {
    public loggedFreeWithOneCard: boolean;
    public loggedCompleteWithOneCard: boolean;
    public gameId: string;
    private _level: number;
    private _players: Player[];
    public winner: Player;
    public showBackButton: boolean;
    public settedPlayers: EventEmitter<Player[]> = new EventEmitter();
    public currentTurn: {
        read: number,
        answer: number,
    };
    canPlayNextTurn = true;

    get level(): number {
        return this._level;
    }

    set level(value: number) {
        this._level = value;
        if (this.gameMode === 'free') {
            this.gameId = this.generateId();
            // this.firebaseAnalytics.logEvent('game_start', {'game_id': this.gameId, 'mode': this.gameMode});
            this.router.navigate(['freeGame']);
        } else {
            this.router.navigate(['selectPlayers']);
        }
    }

    public tokenPositions: Map<number, { currentIndex: number, delta: { x: number, y: number } }>;
    public pendingPoints: { player: Player, points: number }[];
    public gameMode: string;
    public gameEnd: boolean;

    constructor(private router: Router, private cardService: CardService, private audio: AudioService,
                private firebaseAnalytics: FirebaseAnalytics, private storage: Storage) {
        this.pendingPoints = [];
        this.currentTurn = {
            answer: 0,
            read: 1
        };
        this.tokenPositions = new Map();
    }

    addPoints(points: number) {
        if (this.gameMode === 'free') {
            if (!this.loggedFreeWithOneCard) {
                // this.firebaseAnalytics.logEvent('solved_at_least_one_card', {'game_id': this.gameId, 'mode': this.gameMode}).then(value => {
                //     this.loggedFreeWithOneCard = true;
                // });

            }
            return;
        }
        if (!this.loggedCompleteWithOneCard) {
            // this.firebaseAnalytics.logEvent('solved_at_least_one_card', {'game_id': this.gameId, 'mode': this.gameMode}).then(value => {
            //     this.loggedCompleteWithOneCard = true;
            // });
        }
        // todo podria ser toekn id
        this.pendingPoints.push({
            player: this.currentAnswerPlayer,
            points: points
        });
    }

    public backToGame() {
        if (this.gameMode === 'free') {
            this.router.navigate(['/freeGame']);
        } else {
            if (this.pendingPoints.length === 0) {
                this.changeTurn();
            }
            this.router.navigate(['/game']);
        }
    }

    public onBoardIsLoaded() {
        this.canPlayNextTurn = false;
        if (this.pendingPoints.length === 0) {
            this.canPlayNextTurn = true;
            return;
        }
        const pending = this.pendingPoints[0];
        this.tokenPositions.get(pending.player.robot.id).currentIndex++;
        pending.points--;
        if (this.tokenPositions.get(pending.player.robot.id).currentIndex >= 12) {
            this.pendingPoints = [];

            this.winner = this.players.sort(
                (a, b) => this.tokenPositions.get(b.robot.id).currentIndex - this.tokenPositions.get(a.robot.id).currentIndex)[0];
            // this.firebaseAnalytics.logEvent('game_end', {'game_id': this.gameId});
            this.gameId = undefined;
            timer(1000).pipe(take(1)).subscribe(value => {
                this.gameEnd = true;
                this.audio.playSfx('flash.mp3', true);
            });
            timer(4300).pipe(take(1)).subscribe(value => {
                this.router.navigate(['/complete']);
            });
        }
        if (this.pendingPoints.find(e => e.points === 0)) {
            timer(950).pipe(take(1)).subscribe(value => {
                if (this.router.routerState.snapshot.url.includes('game')) {
                    this.audio.playSfx('434756__notarget__wood-step-sample-1.wav');
                }
                this.changeTurn();
                this.canPlayNextTurn = true;
            });
        }
        this.pendingPoints = this.pendingPoints.filter(e => e.points > 0);
    }

    get currentAnswerPlayer() {
        return this._players[this.currentTurn.answer];
    }

    get currentReadPlayer() {
        return this._players[this.currentTurn.read];
    }

    get players(): Player[] {
        return this._players;
    }

    set players(value: Player[]) {
        this.gameId = this.generateId();
        // this.firebaseAnalytics.logEvent('game_start', {'game_id': this.gameId, 'mode': this.gameMode});
        this.showBackButton = true;
        this.settedPlayers.emit(value);
        this.canPlayNextTurn = true;
        this._players = value;
        const initialPositions = [{x: 25, y: 245}, {x: 75, y: 245}, {x: 25, y: 295}, {x: 75, y: 295}];

        this.players.forEach(e => {
            this.tokenPositions.set(e.robot.id,
                {currentIndex: 0, delta: initialPositions.splice(0, 1)[0]});
        });
        this.router.navigate(['/game']).then(() => {
            this.storage.get('played_at_least_once').then(value1 => {
                if (!value1) {
                    this.router.navigate(['/tutorial']).then((valueTUtorial) => {
                    }, reason => {
                    });
                    this.storage.set('played_at_least_once', true);
                }
            }, () => {
                this.router.navigate(['/tutorial']).then((valueTUtorial) => {
                });
                this.storage.set('played_at_least_once', true);
            });
        });
    }

    private changeTurn() {
        if (this.currentTurn.answer + 1 > this.players.length - 1) {
            this.currentTurn.answer = 0;
        } else {
            this.currentTurn.answer++;
        }
        if (this.currentTurn.read + 1 > this.players.length - 1) {
            this.currentTurn.read = 0;
        } else {
            this.currentTurn.read++;
        }
        this.showBackButton = false;
    }

    generateRiddle() {
        this.cardService.generateCurrentCard(CardType.Riddle, this.level);
        this.router.navigate(['/cards/riddle']);
    }

    onLoseTurn() {
        this.router.navigate(['/game']);
        this.changeTurn();
    }

    generatePhrase() {
        this.cardService.generateCurrentCard(CardType.Phrase, this.level);
        this.router.navigate(['/cards/phrase']);
    }

    generateSpelling() {
        this.cardService.generateCurrentCard(CardType.Spelling, this.level);
        this.router.navigate(['/cards/spelling']);
    }

    generateGuessword() {
        this.cardService.generateCurrentCard(CardType.Guessword, this.level);
        this.router.navigate(['/cards/guessword']);
    }

    setMode(mode: string) {
        this.gameMode = mode;
        this.router.navigate(['/selectLevel']);
    }

    restart() {
        this.gameId = this.generateId();
        // this.firebaseAnalytics.logEvent('game_start', {'game_id': this.gameId, 'mode': this.gameMode});
        this.changeTurn();
        this.showBackButton = true;
        this.canPlayNextTurn = true;
        this.settedPlayers.emit(this.players);
        this.gameEnd = false;
        this.tokenPositions.forEach((value, key) => {
            value.currentIndex = 0;
        });
        this.router.navigate(['/game']);
    }

    clearGame() {
        this.router.navigate(['/cover']).then(value => {
            this.pendingPoints = [];
            this.currentTurn = {
                answer: 0,
                read: 1
            };
            this.tokenPositions = new Map();
            this.gameEnd = false;
        });
    }
    private generateId(): string {
        this.loggedCompleteWithOneCard = false;
        this.loggedFreeWithOneCard = false;
        let text = '';

        const charset = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ@#$%&';
        for (let i = 0; i < 36; i++) {
            text += charset.charAt(Math.floor(Math.random() * charset.length));
        }
        return text;
    }
}
