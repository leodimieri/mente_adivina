import {EventEmitter, Injectable} from '@angular/core';
import {Card, CardName, CardType} from '../models/cards/card';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Country} from '../models/language.enum';
import {LanguageService} from './language.service';
import {filter} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CardService {
  public currentCard: Card;
  public shoeExtraTexts: BehaviorSubject<boolean>;
  public cards: Map<CardType, Map<number, Card[]>>;
  public usedCards: Map<CardType, Map<number, Card[]>>;
  public currentCardIsAnswered: boolean;
  public currentAnswerIsViewed: boolean;
  public playStarAnimation: EventEmitter<number>;
  constructor(private http: HttpClient, private languageService: LanguageService) {
    this.playStarAnimation = new EventEmitter();
    this.languageService.settedCountry.pipe(filter(e => e !== undefined)).subscribe(value => {
      this.setCurrentCards();
    });
  }

  private setCurrentCards() {

    this.cards = new Map([[
      CardType.Riddle, new Map([[1, []], [2, []], [3, []]])
    ], [
      CardType.Spelling, new Map([[1, []], [2, []], [3, []]])
    ], [
      CardType.Guessword, new Map([[1, []], [2, []], [3, []]])
    ], [
      CardType.Phrase, new Map([[1, []], [2, []], [3, []]])
    ]]);
    this.usedCards = new Map([[
      CardType.Riddle, new Map([[1, []], [2, []], [3, []]])
    ], [
      CardType.Spelling, new Map([[1, []], [2, []], [3, []]])
    ], [
      CardType.Guessword, new Map([[1, []], [2, []], [3, []]])
    ], [
      CardType.Phrase, new Map([[1, []], [2, []], [3, []]])
    ]]);
    this.loadCards(CardName.Adivinanzas, undefined, [0, 1, 2, 3], [5, 6, 7], 8, 1, CardType.Riddle, 'adivinanzas_1.tsv').subscribe(value => {
      this.cards.get(CardType.Riddle).set(1, this.cards.get(CardType.Riddle).get(1).concat(value));
    });
    this.loadCards(CardName.Adivinanzas, 0, [1, 2, 3, 4, 5], [7, 8, 9], 10, 2, CardType.Riddle, 'adivinanzas_2.tsv').subscribe(value => {
      this.cards.get(CardType.Riddle).set(2, this.cards.get(CardType.Riddle).get(2).concat(value));
    });
    this.loadCards(CardName.Acertijos, undefined, [0], [2, 3, 4], 5, 3, CardType.Riddle, 'acertijos_3.tsv').subscribe(value => {
      this.cards.get(CardType.Riddle).set(3, this.cards.get(CardType.Riddle).get(3).concat(value));
    });
    this.loadCards(CardName.Deletreo, undefined, [0, 1, 2], [], undefined, 1, CardType.Spelling, 'deletreo_1.tsv').subscribe(value => {
      this.cards.get(CardType.Spelling).set(1, this.cards.get(CardType.Spelling).get(1).concat(value));

    });
    this.loadCards(CardName.Deletreo, undefined, [0, 1, 2], [], undefined, 2, CardType.Spelling, 'deletreo_2.tsv').subscribe(value => {
      this.cards.get(CardType.Spelling).set(2, this.cards.get(CardType.Spelling).get(2).concat(value));
    });

    this.loadCards(CardName.Deletreo, undefined, [0, 1, 2], [], undefined, 3, CardType.Spelling, 'deletreo_3.tsv').subscribe(value => {
      this.cards.get(CardType.Spelling).set(3, this.cards.get(CardType.Spelling).get(3).concat(value));
    });

    this.loadCards(CardName.AdivinaPalabra, undefined, [0, 1], [3, 4], 5, 1, CardType.Guessword, 'adivina_palabra_1.tsv').subscribe(value => {
      this.cards.get(CardType.Guessword).set(1, this.cards.get(CardType.Guessword).get(1).concat(value));
    });
    this.loadCards(CardName.AdivinaPalabra, undefined, [0, 1, 2], [4, 5, 6], 7, 2, CardType.Guessword, 'adivina_palabra_2.tsv').subscribe(value => {
      this.cards.get(CardType.Guessword).set(2, this.cards.get(CardType.Guessword).get(2).concat(value));
    });
    this.loadCards(CardName.DescubreElObjeto, undefined, [0, 1, 2, 3], [5, 6, 7], 8, 3, CardType.Guessword, 'descubre_el_objeto_3.tsv').subscribe(value => {
      this.cards.get(CardType.Guessword).set(3, this.cards.get(CardType.Guessword).get(3).concat(value));
    });
    this.loadCards(CardName.JugandoConPalabras, undefined, [0, 1, 2, 3], [4, 5, 6, 7], 8, 1, CardType.Phrase, 'jugando_con_palabras_1.tsv').subscribe(value => {
      this.cards.get(CardType.Phrase).set(1, this.cards.get(CardType.Phrase).get(1).concat(value));
    });
    this.loadCards(CardName.FrasesHechas, undefined, [0], [2, 3, 4], 5, 2, CardType.Phrase, 'frases_hechas_2.tsv').subscribe(value => {
      this.cards.get(CardType.Phrase).set(2, this.cards.get(CardType.Phrase).get(2).concat(value));
    });
    this.loadCards(CardName.Refranes, undefined, [0], [2, 3, 4], 5, 3, CardType.Phrase, 'refranes_3.tsv').subscribe(value => {
      this.cards.get(CardType.Phrase).set(3, this.cards.get(CardType.Phrase).get(3).concat(value));
    });

    this.shoeExtraTexts = new BehaviorSubject(false);
  }

  private loadCards(cardName: CardName, hintIndex: number, initialIndexes: number[], extraIndexes: number[],
                           answerIndex: number, level: number, type: CardType,
                           filePathname: string): Observable<Card[]> {
    return new Observable(obs => {
      const cards: Card[] = [];

      this.http.get('assets/cards/' + this.languageService.settedCountry.value + '/' + filePathname, {responseType: 'blob'}).subscribe(value => {
        const fileReader = new FileReader();
        fileReader.addEventListener('loadend', (loaded: any) => {
          const rows: string[] = loaded.srcElement.result.split('\n').slice(1).filter(e => e && e.length > 0)
          rows.forEach(row => {
            const columns = row.split('\t').map(e => e.trim());
            const card = new Card();
            card.cardName = cardName;
            card.inititalTexts = [];
            initialIndexes.forEach(i => {
              if (columns[i]) {
                card.inititalTexts.push(columns[i]);
              }
            });
            card.extraTexts = [];
            extraIndexes.forEach(i => {
              if (columns[i] && columns[i].length > 0) {
                card.extraTexts.push(columns[i]);
              }
            });
            card.level = level;
            card.type = type;
            if (answerIndex >= 0) {
              card.answer = columns[answerIndex];
            }
            if (hintIndex >= 0) {
              card.hint = columns[hintIndex];
            }
            cards.push(
                card
            );
          });
          obs.next(cards);
          obs.complete();
        });
        fileReader.readAsText(value);
      });
    });
  }

  generateCurrentCard(type: CardType, level: number) {
    if (this.cards.get(type).get(level).length === 0 ) {
      const deleteCount = Math.round(this.usedCards.get(type).get(level).length / 2);
      this.cards.get(type).set(level, this.usedCards.get(type).get(level).splice(0, deleteCount));
    }
    const indexCard = Math.floor(Math.random() * this.cards.get(type).get(level).length);
    this.currentCard = this.cards.get(type).get(level).splice(indexCard, 1)[0];
    /* para elegir las cartas con mas longitud total
     this.currentCard = this.cards.get(type).get(level).sort((a, b) => {
      const totalLlengthB = b.inititalTexts.map(e => e.length).concat(b.extraTexts.
        map(value => value.length)).reduce((previousValue, currentValue) => previousValue + currentValue);
      const totalLlengthA = a.inititalTexts.map(e => e.length).concat
        (a.extraTexts.map(value => value.length)).reduce((previousValue, currentValue) => previousValue + currentValue);
      return totalLlengthB - totalLlengthA
    })[0];
    */
    this.usedCards.get(type).get(level).push(this.currentCard);
    this.currentCardIsAnswered = false;
    this.currentAnswerIsViewed = type === CardType.Spelling;
  }
}
