import { Injectable } from '@angular/core';
import {Country} from '../models/language.enum';
import { Storage } from '@ionic/storage';
import {BehaviorSubject, from, Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  public settedCountry: BehaviorSubject<Country> = new BehaviorSubject<Country>(undefined);

  set currentCountry(value: Country) {
    this.storage.set('current-country', value).then(() => {
      this.settedCountry.next(value);
    }, reason => {
    });
  }
  constructor(private storage: Storage) {
  }

  public isSelected(): Observable<boolean> {
    return from(this.storage.get('current-country')).pipe(map(value => {
      if (value !== this.settedCountry.value) {
        this.settedCountry.next(value);
        console.log(value);
      }
      return value !== undefined && value !== null;
    }));
  }
}
