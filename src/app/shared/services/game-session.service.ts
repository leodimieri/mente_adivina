import { Injectable } from '@angular/core';
import {Player} from '../models/player';
import {CardType, RouletteStep} from '../models/cards/card';
import {GameService} from './game.service';
@Injectable({
  providedIn: 'root'
})
export class GameSessionService {
  private challengesByPlayer: Map<Player, RouletteStep[]>;

  constructor(private game: GameService) {
    this.game.settedPlayers.subscribe(players => {
      this.startSession(players);
    });
  }

  public startSession(players: Player[]) {
    this.challengesByPlayer = new Map<Player, RouletteStep[]>();
    players.forEach(value => {
      this.challengesByPlayer.set(value, []);
    });
  }

  getNextCategoryRoulette(): RouletteStep {
    const allSteps = Object.values(RouletteStep);
    let possibilities = [];
    const prevTurns = this.challengesByPlayer.get(this.game.currentAnswerPlayer);
    let lastTurnOfOtherPlayer;
    if (this.game.players.length === 2) {
      const prevTurnsOtherPlayer = this.challengesByPlayer.get(this.game.currentReadPlayer);
      lastTurnOfOtherPlayer = prevTurnsOtherPlayer[prevTurnsOtherPlayer.length - 1];
    }
    if (prevTurns.length >= 2) {
      // forzar los que no salieron
      const unusedCategories = allSteps.filter(e => !prevTurns.includes(e) && e !== lastTurnOfOtherPlayer);
      if (unusedCategories.filter(value => value !== RouletteStep.FreeChoose && value !== RouletteStep.LooseTurn).length > 0) {
        possibilities = unusedCategories;
      } else {
        possibilities = allSteps.filter(e => e !== prevTurns[prevTurns.length - 1] && e !== lastTurnOfOtherPlayer);
      }
    } else {
      possibilities = allSteps.filter(e => e !== prevTurns[prevTurns.length - 1] && e !== lastTurnOfOtherPlayer
                                  && e !== RouletteStep.LooseTurn && e !== RouletteStep.FreeChoose);
    }
/*    // si son los primeros turnos o salieron todos (por eso no pongo el else
    const differentFromLastOne = ;
    const randomToReturn = differentFromLastOne[Math.floor(Math.random() * differentFromLastOne.length)];
    this.challengesByPlayer.get(this.game.currentAnswerPlayer).push(randomToReturn);
    console.log('randomToReturn', randomToReturn);
    return randomToReturn;*/

    const toReturn = possibilities[Math.floor(Math.random() * possibilities.length)];
    this.challengesByPlayer.get(this.game.currentAnswerPlayer).push(toReturn);
    return toReturn;
  }
}
