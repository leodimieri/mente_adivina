import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../models/player';
import {GameService} from '../../services/game.service';

@Component({
  selector: 'app-card-player',
  templateUrl: './card-player.component.html',
  styleUrls: ['./card-player.component.scss']
})
export class CardPlayerComponent implements OnInit {

  @Input()
  public data: Player;
  @Input()
  public state: 'closed' | 'add';
  constructor(public game: GameService) { }

  ngOnInit() {
  }

}
