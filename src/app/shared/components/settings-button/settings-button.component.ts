import { Component, OnInit } from '@angular/core';
import {AudioService} from '../../services/audio.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-settings-button',
  templateUrl: './settings-button.component.html',
  styleUrls: ['./settings-button.component.scss']
})
export class SettingsButtonComponent implements OnInit {

  constructor(private router: Router, private audio: AudioService) { }

  ngOnInit() {
  }

  goToSettings() {
    this.audio.playClick();
    this.router.navigate(['/settings']);

  }

}
