import {Component, Input, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {AudioService} from '../../services/audio.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-back-button',
  templateUrl: './back-button.component.html',
  styleUrls: ['./back-button.component.scss']
})
export class BackButtonComponent implements OnInit {

  @Input()
  public backUrl: string;
  constructor(private _location: Location, private audio: AudioService, private router: Router) { }

  ngOnInit() {
  }

  goBack() {
    this.audio.playClick();
    if (this.backUrl) {
      this.router.navigate([this.backUrl]);
    } else {
      this._location.back();
    }
  }

}
