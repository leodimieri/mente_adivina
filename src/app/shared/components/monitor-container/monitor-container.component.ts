import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-monitor-container',
  templateUrl: './monitor-container.component.html',
  styleUrls: ['./monitor-container.component.scss'],
})
export class MonitorContainerComponent implements OnInit {

  @Input() public logo: 'mente-adivina' | 'divermente' | 'categories';
  constructor() { }

  ngOnInit() {}

}
