import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import {IonicModule} from '@ionic/angular';
import {MatDialogModule, MatIconModule} from '@angular/material';
import {SettingsButtonComponent} from './components/settings-button/settings-button.component';
import {BackButtonComponent} from './components/back-button/back-button.component';
import {CardPlayerComponent} from './components/card-player/card-player.component';
import {MonitorContainerComponent} from './components/monitor-container/monitor-container.component';
import {CategoriesLogoComponent} from './components/categories-logo/categories-logo.component';
import {DivermenteLogoComponent} from './components/divermente-logo/divermente-logo.component';
import {MenteAdivinaLogoComponent} from './components/mente-adivina-logo/mente-adivina-logo.component';

@NgModule({
    declarations: [SettingsButtonComponent, BackButtonComponent, CardPlayerComponent, MonitorContainerComponent, CategoriesLogoComponent, DivermenteLogoComponent, MenteAdivinaLogoComponent],
    imports: [
        CommonModule,
        IonicModule,
        FlexLayoutModule,
        MatIconModule,
        MatDialogModule,
    ],
    exports: [
        CommonModule,
        IonicModule,
        FlexLayoutModule,
        MatIconModule,
        MatDialogModule,
        SettingsButtonComponent,
        BackButtonComponent,
        CardPlayerComponent,
        MonitorContainerComponent
    ]
})
export class SharedModule {
}
