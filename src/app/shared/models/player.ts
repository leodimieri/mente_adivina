import {Robot} from './robot';

export class Player {
    constructor(public id: number, public name: string, public robot: Robot, public color: string) {}
}
