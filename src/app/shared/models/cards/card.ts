export class Card {
    public cardName: CardName;
    public answer: string;
    public inititalTexts: string[];
    public extraTexts: string[];
    public hint: string;
    public level: number;
    public type: CardType;
}

export enum CardType {
    Spelling, Riddle, Phrase, Guessword
}

export enum RouletteStep {
    Blue = 'blue', Orange = 'orange', Green = 'green' , Fuchsia = 'fuchsia' , FreeChoose = 'free-to-choose' , LooseTurn = 'loose-turn'
}

export enum CardName {
    Adivinanzas = 'Adivinanzas',
    Acertijos = 'Acertijos',
    Deletreo = 'Deletreo',
    JugandoConPalabras = 'Jugando con palabras',
    FrasesHechas = 'Frases Hechas',
    Refranes = 'Refranes',
    AdivinaPalabra = 'Adivina palabra',
    DescubreElObjeto =  'Descubre el objeto'
}
