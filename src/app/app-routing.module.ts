import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ExitFromBoardGuard} from './guards/exit-from-board.guard';
import {SelectedCountryGuard} from './guards/selected-country.guard';

const routes: Routes = [
  { path: '', redirectTo: 'cover', pathMatch: 'full' },
  { path: 'cover', loadChildren: './cover/cover.module#CoverModule', canActivate: [SelectedCountryGuard] },
  { path: 'selectLevel', loadChildren: './select-level/select-level.module#SelectLevelModule' },
  { path: 'selectLanguage', loadChildren: './select-language/select-language.module#SelectLanguageModule' },
  { path: 'selectPlayers', loadChildren: './select-players/select-players.module#SelectPlayersModule' },
  { path: 'game', loadChildren: './game/game.module#GameModule' },
  { path: 'roulette', loadChildren: './roulette/roulette.module#RouletteModule' },
  { path: 'cards', loadChildren: './cards/cards.module#CardsModule' },
  { path: 'settings', loadChildren: './settings/settings.module#SettingsModule' },
  { path: 'tutorial', loadChildren: './tutorial/tutorial.module#TutorialModule' },
  { path: 'complete', loadChildren: './game-complete/game-complete.module#GameCompleteModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'freeInfo', loadChildren: './free-game-info/free-game-info.module#FreeGameInfoModule' },
  { path: 'freeGame', loadChildren: './free-game/free-game.module#FreeGameModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
