import { NgModule } from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import { SelectLevelViewComponent } from './select-level-view/select-level-view.component';
import {RouterModule} from '@angular/router';
import { FirstLevelButtonComponent } from './first-level-button/first-level-button.component';
import { SecondLevelButtonComponent } from './second-level-button/second-level-button.component';
import { ThirdLevelButtonComponent } from './third-level-button/third-level-button.component';

@NgModule({
  declarations: [SelectLevelViewComponent, FirstLevelButtonComponent, SecondLevelButtonComponent, ThirdLevelButtonComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: SelectLevelViewComponent
      }
    ])
  ]
})
export class SelectLevelModule { }
