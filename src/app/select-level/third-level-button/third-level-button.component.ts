import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AudioService} from '../../shared/services/audio.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-third-level-button',
  templateUrl: './third-level-button.component.html',
  styleUrls: ['./third-level-button.component.scss']
})
export class ThirdLevelButtonComponent implements OnInit {
  @Output()
  public clickLevel: EventEmitter<void> = new EventEmitter();

  constructor(private audio: AudioService, private router: Router) {
  }

  ngOnInit() {
  }

  moreInfo() {
    this.audio.playClick();
    this.router.navigate(['cover', 'levelInfo', 3], {queryParams: {'hide-close-button': true}});
  }
}
