import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdLevelButtonComponent } from './third-level-button.component';

describe('ThirdLevelButtonComponent', () => {
  let component: ThirdLevelButtonComponent;
  let fixture: ComponentFixture<ThirdLevelButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThirdLevelButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdLevelButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
