import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AudioService} from '../../shared/services/audio.service';
import {GameService} from '../../shared/services/game.service';

@Component({
  selector: 'app-select-level-view',
  templateUrl: './select-level-view.component.html',
  styleUrls: ['./select-level-view.component.scss']
})
export class SelectLevelViewComponent implements OnInit {

  constructor(private router: Router, private audio: AudioService, private game: GameService) { }

  ngOnInit() {
  }

  selectLevel(number: number) {
    this.audio.playClick();
    this.game.level = number;
  }
}
