import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectLevelViewComponent } from './select-level-view.component';

describe('SelectLevelViewComponent', () => {
  let component: SelectLevelViewComponent;
  let fixture: ComponentFixture<SelectLevelViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectLevelViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectLevelViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
