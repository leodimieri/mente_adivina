import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AudioService} from '../../shared/services/audio.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-first-level-button',
    templateUrl: './first-level-button.component.html',
    styleUrls: ['./first-level-button.component.scss']
})
export class FirstLevelButtonComponent implements OnInit {
    @Output()
    public clickLevel: EventEmitter<void> = new EventEmitter();

    constructor(private audio: AudioService, private router: Router) {
    }

    ngOnInit() {
    }

    moreInfo() {
        this.audio.playClick();
        this.router.navigate(['cover', 'levelInfo', 1], {queryParams: {'hide-close-button': true}});
    }
}
