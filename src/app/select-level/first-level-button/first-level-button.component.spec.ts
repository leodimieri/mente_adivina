import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstLevelButtonComponent } from './first-level-button.component';

describe('FirstLevelButtonComponent', () => {
  let component: FirstLevelButtonComponent;
  let fixture: ComponentFixture<FirstLevelButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstLevelButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstLevelButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
