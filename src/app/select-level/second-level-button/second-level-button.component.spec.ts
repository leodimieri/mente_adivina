import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondLevelButtonComponent } from './second-level-button.component';

describe('SecondLevelButtonComponent', () => {
  let component: SecondLevelButtonComponent;
  let fixture: ComponentFixture<SecondLevelButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondLevelButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondLevelButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
