import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AudioService} from '../../shared/services/audio.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-second-level-button',
  templateUrl: './second-level-button.component.html',
  styleUrls: ['./second-level-button.component.scss']
})
export class SecondLevelButtonComponent implements OnInit {
  @Output()
  public clickLevel: EventEmitter<void> = new EventEmitter();

  constructor(private audio: AudioService, private router: Router) {
  }

  ngOnInit() {
  }

  moreInfo() {
    this.audio.playClick();
    this.router.navigate(['cover', 'levelInfo', 2], {queryParams: {'hide-close-button': true}});
  }
}
