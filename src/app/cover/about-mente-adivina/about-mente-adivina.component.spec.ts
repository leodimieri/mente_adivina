import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutMenteAdivinaComponent } from './about-mente-adivina.component';

describe('AboutMenteAdivinaComponent', () => {
  let component: AboutMenteAdivinaComponent;
  let fixture: ComponentFixture<AboutMenteAdivinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutMenteAdivinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutMenteAdivinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
