import {Router} from '@angular/router';
import {AudioService} from '../../shared/services/audio.service';
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-about-mente-adivina',
  templateUrl: './about-mente-adivina.component.html',
  styleUrls: ['./about-mente-adivina.component.scss']
})
export class AboutMenteAdivinaComponent implements OnInit {

  public message = '';
  public counter = 0;
  constructor(private router: Router, private audio: AudioService) { }

  ngOnInit() {
  }


  public moreInfo() {
    this.audio.playClick();
    this.router.navigate(['cover', 'moreInfo']);
  }

/*
  playVideo(i: number) {
    return;
    let url: string;
    switch (i) {
      case 1: {
        // todo try file:///storage/emulated/0/YOUR_FOLDER/YOUR_FILE
            url = 'file:///android_asset/assets/videos/video.mp4';
        break;
      }
      case 2: {
        url = 'file:///android_asset/www/assets/videos/video.mp4';
        break;
      }
      case 3: {
        url = 'file:///android_asset/www/assets/videos/video.ogv';
        break;
      }
      case 4: {
        url = 'file:///assets/public/assets/videos/video.mp4';
        break;
      }
      case 5: {
        url = 'assets/public/assets/videos/video.mp4';
        break;
      }
    }
    this.message = url;
    this.videoPlayer.play(url, {scalingMode: 1, volume: 1}).then(() => {
      this.message = 'video completed';
      console.log('video completed');
    }).catch(err => {
      this.message = 'error' + err;
      console.log(err);
    }).finally(() => {
      console.log('finally');
    });
  /!*  this.counter++;
    if (this.counter < 4) {
      this.audio.playClick();
;
    } else {
      this.audio.playClick();
      this.videoPlayer.play().then(() => {
        this.message = 'video completed';
        console.log('video completed');
      }).catch(err => {
        this.message = 'error' + err;
        console.log(err);
      }).finally(() => {
        console.log('finally');
      });

    }

    return;

    if (this.counter < 2) {
      // Playing a video.
      this.videoPlayer.play('file:///android_asset/www/assets/videos/video.mp4').then(() => {
        this.message = 'video completed';
        console.log('video completed');
      }).catch(err => {
        this.message = 'error' + err;
        console.log(err);
      }).finally(() => {
        console.log('finally');
      });
    }
    else if (this.counter < 4) {
      // Playing a video.
      this.videoPlayer.play('file:///assets/videos/video.mp4').then(() => {
        this.message = 'video completed';
        console.log('video completed');
      }).catch(err => {
        this.message = 'error' + err;
        console.log(err);
      }).finally(() => {
        console.log('finally');
      });
    }
    else if (this.counter < 6) {
      // Playing a video.
      this.videoPlayer.play('/assets/videos/video.mp4').then(() => {
        this.message = 'video completed';
        console.log('video completed');
      }).catch(err => {
        this.message = 'error' + err;
        console.log(err);
      }).finally(() => {
        console.log('finally');
      });
    }
    else if (this.counter < 8) {
      // Playing a video.

    } else {
      this.videoPlayer.play('file:///android_asset/www/video.mp4').then(() => {
        this.message = 'video completed';
        console.log('video completed');
      }).catch(err => {
        this.message = 'error' + err;
        console.log(err);
      }).finally(() => {
        console.log('finally');
      });
    }*!/
  }
*/

}

