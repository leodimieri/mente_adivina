import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoAndButtonsComponent } from './logo-and-buttons.component';

describe('LogoAndButtonsComponent', () => {
  let component: LogoAndButtonsComponent;
  let fixture: ComponentFixture<LogoAndButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoAndButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoAndButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
