import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AudioService} from '../../shared/services/audio.service';
import {GameService} from '../../shared/services/game.service';

@Component({
  selector: 'app-logo-and-buttons',
  templateUrl: './logo-and-buttons.component.html',
  styleUrls: ['./logo-and-buttons.component.scss']
})
export class LogoAndButtonsComponent implements OnInit {

  constructor(private router: Router, private audioService: AudioService, private game: GameService) { }

  ngOnInit() {
  }

  goToCompleteGame() {
    this.audioService.playClick();
    this.game.setMode('complete');
  }

  goToFreeGameInfo() {
    this.audioService.playClick();
    this.router.navigate(['freeInfo']);
  }

}
