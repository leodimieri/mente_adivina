import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {SettingsService} from '../../settings/services/settings.service';
import {AudioService} from '../../shared/services/audio.service';
import {FirebaseAnalytics} from '@ionic-native/firebase-analytics/ngx';

@Component({
  selector: 'app-cover-view',
  templateUrl: './cover-view.component.html',
  styleUrls: ['./cover-view.component.scss']
})
export class CoverViewComponent implements OnInit {

  constructor(private router: Router, private audioService: AudioService, public settings: SettingsService) { }

  ngOnInit() {
  }

  public toggleMusic() {
    this.audioService.playClick();
    this.settings.toggleMusic();
/*
    this.videoPlayer.play('file:///android_asset/www/video.mp4').then(() => {
      console.log('video completed');
    }).catch(err => {
      console.log(err);
    });
*/
  }
}
