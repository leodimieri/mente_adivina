import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoverViewComponent } from './cover-view.component';

describe('CoverViewComponent', () => {
  let component: CoverViewComponent;
  let fixture: ComponentFixture<CoverViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoverViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
