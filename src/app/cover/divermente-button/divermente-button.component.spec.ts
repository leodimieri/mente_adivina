import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DivermenteButtonComponent } from './divermente-button.component';

describe('DivermenteButtonComponent', () => {
  let component: DivermenteButtonComponent;
  let fixture: ComponentFixture<DivermenteButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DivermenteButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DivermenteButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
