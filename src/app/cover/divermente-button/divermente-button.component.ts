import {Component, HostListener, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AudioService} from '../../shared/services/audio.service';

@Component({
  selector: 'app-divermente-button',
  templateUrl: './divermente-button.component.html',
  styleUrls: ['./divermente-button.component.scss']
})
export class DivermenteButtonComponent implements OnInit {

  @HostListener('click', ['$event.target'])
  onClick() {
    this.showDivermenteScreen();
  }

  constructor(private router: Router, private audioService: AudioService) {
  }

  ngOnInit() {
  }

  showDivermenteScreen() {
    this.audioService.playClick();
    this.router.navigate(['cover/divermente']);
  }

}
