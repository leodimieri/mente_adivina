import {Component, HostListener, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AudioService} from '../../shared/services/audio.service';

@Component({
  selector: 'app-close-button',
  templateUrl: './close-button.component.html',
  styleUrls: ['./close-button.component.scss'],
})
export class CloseButtonComponent implements OnInit {
  @HostListener('click') public close() {
    this.audio.playClick();
    this.router.navigate(['/cover']);
  }
  constructor(private router: Router, private audio: AudioService) { }

  ngOnInit() {}

}
