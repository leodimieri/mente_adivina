import { CoverViewComponent } from './cover-view/cover-view.component';
import {SharedModule} from '../shared/shared.module';
import { AboutDivermenteComponent } from './about-divermente/about-divermente.component';
import { AboutMenteAdivinaComponent } from './about-mente-adivina/about-mente-adivina.component';
import { InfoButtonComponent } from './info-button/info-button.component';
import { LogoAndButtonsComponent } from './logo-and-buttons/logo-and-buttons.component';
import { DivermenteButtonComponent } from './divermente-button/divermente-button.component';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {MoreInfoGeneralComponent} from './more-info/more-info-general/more-info-general.component';
import {LevelInfoComponent} from './more-info/level-info/level-info.component';
import {CloseButtonComponent} from './close-button/close-button.component';

@NgModule({
  declarations: [CoverViewComponent, AboutDivermenteComponent, AboutMenteAdivinaComponent, InfoButtonComponent, LogoAndButtonsComponent,
    DivermenteButtonComponent, MoreInfoGeneralComponent, LevelInfoComponent, CloseButtonComponent],
  imports: [
      SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: CoverViewComponent
      },
      {
        path: 'divermente',
        component: AboutDivermenteComponent
      },
      {
        path: 'menteadivina',
        component: AboutMenteAdivinaComponent,
      },
      {
        path: 'moreInfo',
        component: MoreInfoGeneralComponent,
      },
      {
        path: 'levelInfo/:level',
        component: LevelInfoComponent,
      }
    ])
  ],
  providers: []
})
export class CoverModule { }
