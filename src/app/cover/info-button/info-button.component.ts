import {Component, HostListener, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AudioService} from '../../shared/services/audio.service';

@Component({
  selector: 'app-info-button',
  templateUrl: './info-button.component.html',
  styleUrls: ['./info-button.component.scss']
})
export class InfoButtonComponent implements OnInit {

  @HostListener('click', ['$event.target'])
  onClick() {
    this.showMenteAdivinaScreen();
  }

  constructor(private router: Router, private audioService: AudioService) { }

  ngOnInit() {
  }

  showMenteAdivinaScreen() {
    this.audioService.playClick();
    this.router.navigate(['cover/menteadivina']);

  }

}
