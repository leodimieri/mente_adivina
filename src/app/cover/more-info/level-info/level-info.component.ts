import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-level-info',
  templateUrl: './level-info.component.html',
  styleUrls: ['./level-info.component.scss'],
})
export class LevelInfoComponent implements OnInit {
  public info: {categories: {name: string, text: string}[], age: number, description: string};
  public level: number;
  public hideCloseButton: boolean;
  constructor(private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParamMap.subscribe(value => {
      if (value.has('hide-close-button')) {
        this.hideCloseButton = value.get('hide-close-button') === 'true';
      }
    });
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(value => {
      this.setInfo(+value.get('level'));
    });
  }

  private setInfo(level: number) {
    this.level = level;
    switch (level) {
      case 1:
        this.info = {
          age: 6,
          description: 'IMPRENTA MAYÚSCULA',
          categories: [
              {name: 'JUGANDO CON PALABRAS', text: 'Habilidades fonológicas y semánticas. Refuerzo de la lectura inicial.'},
              {name: 'DELETREO', text: 'Segmentación fonémica y ortográfica. Atención, concentración y memoria visual.'},
              {name: 'ADIVINA PALABRA', text: 'Comprensión verbal, razonamiento lógico. Integración y síntesis de información.'},
              {name: 'ADIVINANZAS', text: 'Razonamiento lógico, inferencias y fluidez verbal.'}
              ]
        };
        break;
      case 2:
        this.info = {
          age: 8,
          description: 'IMPRENTA MINÚSCULA',
          categories: [
            {name: 'FRASES HECHAS', text: 'Pensamiento metafórico y simbólico. Reflexión activa de frases del lenguaje cotidiano.'},
            {name: 'DELETREO', text: 'Segmentación fonémica y ortográfica. Atención, concentración y memoria visual.'},
            {name: 'ADIVINA PALABRA', text: 'Comprensión verbal, razonamiento lógico. Integración y síntesis de información.'},
            {name: 'ADIVINANZAS', text: 'Razonamiento lógico, inferencias y fluidez verbal.'}
          ]
        };
        break;
      case 3:

        this.info = {
          age: 11,
          description: 'IMPRENTA MINÚSCULA',
          categories: [
            {name: 'REFRANES', text: 'Pensamiento metafórico y simbólico. Reflexión activa de frases del lenguaje cotidiano.'},
            {name: 'DELETREO', text: 'Segmentación fonémica y ortográfica. Atención, concentración y memoria visual.'},
            {name: 'DESCUBRE EL OBJETO', text: 'Comprensión y abstracción verbal, jerarquización de conceptos.'},
            {name: 'ACERTIJOS', text: 'Funciones ejecutivas, razonamiento lógico, inferencias y fluidez verbal.'}
          ]
        };
        break;
    }
  }
}
