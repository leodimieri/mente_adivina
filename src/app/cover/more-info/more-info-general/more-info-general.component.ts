import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AudioService} from '../../../shared/services/audio.service';

@Component({
  selector: 'app-more-info-general',
  templateUrl: './more-info-general.component.html',
  styleUrls: ['./more-info-general.component.scss'],
})
export class MoreInfoGeneralComponent implements OnInit {

  constructor(private router: Router, private audio: AudioService) { }

  ngOnInit() {}

  onClickLevel(number: number) {
    this.audio.playClick();
    this.router.navigate(['cover', 'levelInfo', number]);
  }
}
