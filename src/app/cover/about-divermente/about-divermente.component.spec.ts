import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutDivermenteComponent } from './about-divermente.component';

describe('AboutDivermenteComponent', () => {
  let component: AboutDivermenteComponent;
  let fixture: ComponentFixture<AboutDivermenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutDivermenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutDivermenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
