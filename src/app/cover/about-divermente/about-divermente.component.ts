import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AudioService} from '../../shared/services/audio.service';

@Component({
  selector: 'app-about-divermente',
  templateUrl: './about-divermente.component.html',
  styleUrls: ['./about-divermente.component.scss']
})
export class AboutDivermenteComponent implements OnInit {

  constructor(private router: Router, private audio: AudioService) { }

  ngOnInit() {
  }

}
