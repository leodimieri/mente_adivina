import { NgModule } from '@angular/core';
import { GameCompleteViewComponent } from './game-complete-view/game-complete-view.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [GameCompleteViewComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: GameCompleteViewComponent
      }
    ])
  ]
})
export class GameCompleteModule { }
