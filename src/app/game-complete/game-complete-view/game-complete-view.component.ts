import {AfterViewInit, Component, ElementRef, OnInit} from '@angular/core';
import {GameService} from '../../shared/services/game.service';
import {Player} from '../../shared/models/player';
import {AudioService} from '../../shared/services/audio.service';
import anime from 'animejs';
import {timer} from 'rxjs';
@Component({
  selector: 'app-game-complete-view',
  templateUrl: './game-complete-view.component.html',
  styleUrls: ['./game-complete-view.component.scss']
})
export class GameCompleteViewComponent implements OnInit, AfterViewInit {

  public winners: Player[];
  constructor(public game: GameService, public audioService: AudioService, private element: ElementRef) {
  }

  ngOnInit() {
    this.audioService.playClaps();
  }

  ionViewWillEnter() {
    this.winners = this.game.players.sort(
        (a, b) => this.game.tokenPositions.get(b.robot.id).currentIndex - this.game.tokenPositions.get(a.robot.id).currentIndex);
  }
  ionViewWillLeave() {
    this.audioService.pauseSfx();
  }

  onClickHome() {
    this.audioService.playClick();
    this.game.clearGame();
  }

  onClickRestart() {
    this.audioService.playClick();
    this.game.restart();
  }

  ngAfterViewInit(): void {
    timer(100).subscribe(value => {
      anime({
        targets: '.papel-picado .group',
        easing: 'linear',
        duration: 4000,
        translateY: [-350, 400],
        delay: function(el, i, l) {
          return  i * 2750;
        },
        autoplay: true,
        loop: true
      });
      anime({
        targets: '.papel-picado path',
        easing: 'linear',
        duration: 500,
        direction: 'alternate',
        rotate: [0, function(el, i, l) {
          return (Math.random() < 0.5 ? 1 : -1) * (l % 10);
        }],
        autoplay: true,
        loop: true
      });
    });

  }
}
