import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameCompleteViewComponent } from './game-complete-view.component';

describe('GameCompleteViewComponent', () => {
  let component: GameCompleteViewComponent;
  let fixture: ComponentFixture<GameCompleteViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameCompleteViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameCompleteViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
