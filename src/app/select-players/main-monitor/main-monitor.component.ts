import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Player} from '../../shared/models/player';
import {Robot} from '../../shared/models/robot';
import {AudioService} from '../../shared/services/audio.service';
import {GameService} from '../../shared/services/game.service';

@Component({
  selector: 'app-main-monitor',
  templateUrl: './main-monitor.component.html',
  styleUrls: ['./main-monitor.component.scss']
})
export class MainMonitorComponent implements OnInit, AfterViewInit {

  @ViewChild('nameInputElement', {static: true})
  public input: ElementRef<HTMLInputElement>;
  @Input()
  public allUnusedRobots: Robot[];
  @Input() public canRemove: boolean;
  private _currentPlayer: Player;
  get currentPlayer(): Player {
    return this._currentPlayer;
  }
  @Input()
  set currentPlayer(value: Player) {
    this._currentPlayer = value;
    this.updateNameInputValue();
  }
  @Output()
  public onRemoveRobot: EventEmitter<boolean>;
  @Output()
  public changeName: EventEmitter<string> = new EventEmitter();
  @Output() public clickInputName: EventEmitter<void> = new EventEmitter();
  @Output() public selectRobot: EventEmitter<void> = new EventEmitter();

  constructor(private audioService: AudioService, public game: GameService) {
    this.onRemoveRobot = new EventEmitter<boolean>();
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.input.nativeElement.onkeyup = ev => {
      this._currentPlayer.name = this.input.nativeElement.value;
      this.changeName.emit(this._currentPlayer.name);
    };
    this.updateNameInputValue();
  }

  private updateNameInputValue() {
    this.input.nativeElement.value = this._currentPlayer ? this._currentPlayer.name : '';
  }

  public nextRobot() {
    if (this.currentPlayer.robot.id) {
      this.allUnusedRobots.push(this.currentPlayer.robot);
      this.currentPlayer.robot = this.allUnusedRobots.splice(0, 1)[0];
    } else {
      this.currentPlayer.robot = this.allUnusedRobots.splice(1, 1)[0];
    }
    this.selectRobot.emit();

    this.audioService.playClick();
    /*if (this.players[this.currentPlayerIndex].robotId > 5) {
        this.players[this.currentPlayerIndex].robotId = 0;
    } else {
        this.players[this.currentPlayerIndex].robotId++;
    }*/
  }

  public prevRobot() {
    if (this.currentPlayer.robot.id) {
      this.allUnusedRobots = [this.currentPlayer.robot].concat(this.allUnusedRobots);
    }
    this.currentPlayer.robot = this.allUnusedRobots.splice(this.allUnusedRobots.length - 1, 1)[0];
    this.selectRobot.emit();
    this.audioService.playClick();
    /*if (this.players[this.currentPlayerIndex].robotId === 0) {
        this.players[this.currentPlayerIndex].robotId = 6;
    } else {
        this.players[this.currentPlayerIndex].robotId--;
    }*/
  }

  removeCurrentRobot() {
    this.audioService.playClick();
    this.onRemoveRobot.next(true);
  }

  onClickInputName() {
    if (!this.currentPlayer.robot.id) {
      this.currentPlayer.robot = this.allUnusedRobots.splice(0, 1)[0];
    }
      this.clickInputName.emit();
  }
}
