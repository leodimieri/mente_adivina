import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectPlayersViewComponent } from './select-players-view.component';

describe('SelectPlayersViewComponent', () => {
  let component: SelectPlayersViewComponent;
  let fixture: ComponentFixture<SelectPlayersViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectPlayersViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectPlayersViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
