import {
    Component, ElementRef,
    OnDestroy,
    OnInit,
    QueryList, ViewChild,
    ViewChildren
} from '@angular/core';
import {Router} from '@angular/router';
import {Robot} from '../../shared/models/robot';
import {RobotService} from '../../shared/services/robot.service';
import {GameService} from '../../shared/services/game.service';
import {Player} from '../../shared/models/player';
import {AudioService} from '../../shared/services/audio.service';
import {CardPlayerComponent} from '../../shared/components/card-player/card-player.component';
import {Subscription, timer} from 'rxjs';
import {take} from 'rxjs/operators';
import {Plugins} from '@capacitor/core';
import {MainMonitorComponent} from '../main-monitor/main-monitor.component';
import anime from 'animejs';
@Component({
    selector: 'app-select-players-view',
    templateUrl: './select-players-view.component.html',
    styleUrls: ['./select-players-view.component.scss']
})
export class SelectPlayersViewComponent implements OnInit, OnDestroy {

    public currentPlayerIndex: number;
    public allUnusedRobots: Robot[];
    public players: Player[];
    public colors: string[] = ['#ed1941', '#00aeef', '#ffe600', '#0db14b'];
    public allowedPlayButton: boolean;
    private subscription: Subscription;
    @ViewChildren(CardPlayerComponent) cardPlayers: QueryList<CardPlayerComponent>;
    @ViewChild(MainMonitorComponent, {static: false}) public mainMonitor: MainMonitorComponent;
    @ViewChild('mainInputName', {static: false}) mainInputName: ElementRef;
    private _showInputName: boolean;
    public canRemovePlayer: boolean;

    get showInputName(): boolean {
        return this._showInputName;
    }

    set showInputName(value: boolean) {
        this._showInputName = value;
        if (!value && this.players.filter(e => e.name.length > 0).length < this.players.length) {
            do {
                this.currentPlayerIndex++;
                if (this.currentPlayerIndex >= this.players.length) {
                    this.currentPlayerIndex = 0;
                }
            } while (this.players[this.currentPlayerIndex].name.length > 0);
        } else {
            timer(50).subscribe(() => {
                if (!this.mainInputName) {
                    timer(100).subscribe(() => {
                        this.mainInputName.nativeElement.focus();
                        Plugins.StatusBar.hide();
                    });
                } else {
                    this.mainInputName.nativeElement.focus();
                    Plugins.StatusBar.hide();
                }
            });
        }
        this.updatePlayButton();
        Plugins.StatusBar.hide();
    }

    constructor(private router: Router, private robotService: RobotService, public game: GameService,
                private audioService: AudioService) {

    }

    ngOnInit() {
        this.robotService.generateRobots();
        this.allUnusedRobots = this.robotService.robotsByLevel.get(this.game.level);
        this.players = [];
        this.addPlayer();
        this.addPlayer();
        this.addPlayer();
        this.addPlayer();
        this.currentPlayerIndex = 0;
        this.allowedPlayButton = false;
    }

    public changeCurrentPlayer(index: number) {
        if (!this.isValidPlayer(this.players[this.currentPlayerIndex])) {
            this.wrongNameAnimation();
        } else {
            this.currentPlayerIndex = index;
            this.audioService.playClick();
            this.updatePlayButton();
        }
    }

    goToGame() {
        if (this.allowedPlayButton) {
            this.audioService.playClick();
            const validPlayers = this.players.filter(e => this.isValidPlayer(e));
            this.game.players = validPlayers;
        } else {
            // todo play cant click?
        }
        Plugins.StatusBar.hide();
    }

    private isValidPlayer(player: Player) {
        return player.name.length > 0;
    }

    public addPlayer() {
        this.players.push({
            id: this.players.length + 1,
            robot: {id: undefined, path: ''}, name: '', color: this.colors[this.players.length]
        });
    }

    public onClickAddPlayer() {
        this.addPlayer();
        this.audioService.playClick();
        // workaround porque no cargaba la imagen del robot en el main monitor la primera vez
        this.subscription = timer(50).pipe(take(1)).subscribe(value => {
            this.currentPlayerIndex = this.players.length - 1;
            this.subscription.unsubscribe();
        });
    }

    onRemoveCurrentRobot() {
        const currentRobot = this.players[this.currentPlayerIndex].robot;
        if (currentRobot.id && this.allUnusedRobots.every(e => e.id !== currentRobot.id)) {
            this.allUnusedRobots = [this.players[this.currentPlayerIndex].robot].concat(this.allUnusedRobots);
        }
        this.players[this.currentPlayerIndex].robot = {id: undefined, path: ''};
        this.players[this.currentPlayerIndex].name = '';
        this.currentPlayerIndex = this.players.findIndex(e => e.robot.id > 0);
        this.updatePlayButton();
    }

    ngOnDestroy(): void {
    }

    private updatePlayButton() {
        const validPlayers = this.players.filter(e => e.name.length > 0);
        const invalidPlayers = this.players.filter(e => e.robot.id && e.name.length === 0);
        this.canRemovePlayer = validPlayers.length > 2 || (validPlayers.length === 2 && !this.players[this.currentPlayerIndex].robot.id);
        this.allowedPlayButton = validPlayers.length >= 2 && invalidPlayers.length === 0;
    }

    public onSelectRobot() {
        this.updatePlayButton();
    }

    onChangeName(currentName: string) {
        if (currentName.length > 0) {
            this.updatePlayButton();
        } else {
            this.allowedPlayButton = false;
        }
    }

    private wrongNameAnimation() {
        this.audioService.playSfx('cantContinue.mp3');

        const backgroundColor = 'rgba(255,25,206,0.62)';
        anime({
            targets: this.mainMonitor.input.nativeElement,
            direction: 'alternate',
            keyframes: [
                {translateX: 0, translateY: 0, rotate: '0deg', 'backgroundColor': backgroundColor},
                {translateX: -1, translateY: 0, rotate: '0.5deg', 'backgroundColor': backgroundColor},
                {translateX: 2, translateY: 1, rotate: '-0.5deg', 'backgroundColor': backgroundColor},
                {translateX: -3, translateY: 0, rotate: '0.5deg', 'backgroundColor': backgroundColor},
                {translateX: 3, translateY: -1, rotate: '-0.5deg', 'backgroundColor': backgroundColor},
            ],
            complete: anim => {this.mainMonitor.input.nativeElement.style.backgroundColor = 'white'; },
            duration: 300,
            loop: 2
        });
    }
}
