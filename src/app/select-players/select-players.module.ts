import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectPlayersViewComponent } from './select-players-view/select-players-view.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import { MainMonitorComponent } from './main-monitor/main-monitor.component';
import { CheckButtonComponent } from './check-button/check-button.component';
import {TicButtonComponent} from './tic-button/tic-button.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [SelectPlayersViewComponent, MainMonitorComponent, CheckButtonComponent, TicButtonComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: SelectPlayersViewComponent
      }
    ]),
    FormsModule
  ]
})
export class SelectPlayersModule { }
