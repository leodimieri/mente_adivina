import {AfterViewInit, Component} from '@angular/core';

import {NavController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {CardService} from './shared/services/card.service';
import {GameService} from './shared/services/game.service';
import {ScreenOrientation} from '@ionic-native/screen-orientation/ngx';
import {Insomnia} from '@ionic-native/insomnia/ngx';
import {GameSessionService} from './shared/services/game-session.service';
import {AudioService} from './shared/services/audio.service';
import {SettingsService} from './settings/services/settings.service';
import {
    Plugins,
} from '@capacitor/core';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {
    message: string;
    platformReady: boolean;
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private game: GameService,
        private gameSessionService: GameSessionService,
        private cardsService: CardService,
        private screen: ScreenOrientation,
        private navCtrl: NavController,
        private insomnia: Insomnia,
        private sound: AudioService,
        private settingsService: SettingsService,
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.splashScreen.hide();
        this.platform.ready().then(() => {
            this.splashScreen.hide();
            this.screen.lock(this.screen.ORIENTATIONS.LANDSCAPE);
            this.platform.backButton.subscribeWithPriority(9999999, () => {
            });
            this.platform.pause.subscribe(value => {
                this.sound.pauseMusic();
                this.sound.pauseSfx();
            });
            this.platform.resume.subscribe(value => {
                if (this.settingsService.onChangeMusic.getValue()) {
                    this.settingsService.onChangeMusic.next(true);
                }
            });
            Plugins.SplashScreen.hide().finally(() => this.platformReady = true);
            this.keepAwake();
        });
    }

    private keepAwake() {
        this.insomnia.keepAwake()
            .then(
                () => console.log('success'),
                (error) => console.log('error', error)
            );

    }
}
