import {Component, HostListener, OnInit} from '@angular/core';
import {AudioService} from '../../shared/services/audio.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-language-button',
  templateUrl: './language-button.component.html',
  styleUrls: ['./language-button.component.scss'],
})
export class LanguageButtonComponent implements OnInit {
  @HostListener('click')
  private toggleMusic() {
    this.audio.playClick();
    this.router.navigate(['/selectLanguage']);
  }
  constructor(private audio: AudioService, private router: Router) { }

  ngOnInit() {}

}
