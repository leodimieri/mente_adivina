import { Component, OnInit } from '@angular/core';
import {SettingsService} from '../services/settings.service';
import {AudioService} from '../../shared/services/audio.service';
import {Platform} from '@ionic/angular';
import { Plugins, AppState } from '@capacitor/core';
import {GameService} from '../../shared/services/game.service';

const { App } = Plugins;

@Component({
  selector: 'app-settings-view',
  templateUrl: './settings-view.component.html',
  styleUrls: ['./settings-view.component.scss']
})
export class SettingsViewComponent implements OnInit {

  constructor(public settings: SettingsService, private audio: AudioService,
              public game: GameService) { }

  ngOnInit() {
  }

  public toggleMusic() {
    this.audio.playClick();
    this.settings.toggleMusic();
  }

  public toggleSound() {
    this.settings.toggleSfx();
    this.audio.playClick();
  }

  public exit() {
    // App.exitApp();
    window.close();
  }

}
