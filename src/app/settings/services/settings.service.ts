import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  public onChangeMusic: BehaviorSubject<boolean>;
  public onChangeSfx: BehaviorSubject<boolean>;

  constructor() {
    this.onChangeMusic = new BehaviorSubject(true);
    this.onChangeSfx = new BehaviorSubject(true);
  }

  public toggleMusic() {
    this.onChangeMusic.next(!this.onChangeMusic.value);
  }
  public toggleSfx() {
    this.onChangeSfx .next(!this.onChangeSfx.value);
  }
}
