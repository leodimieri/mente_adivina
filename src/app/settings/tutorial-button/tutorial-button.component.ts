import {Component, HostListener, OnInit} from '@angular/core';
import {AudioService} from '../../shared/services/audio.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tutorial-button',
  templateUrl: './tutorial-button.component.html',
  styleUrls: ['./tutorial-button.component.scss'],
})
export class TutorialButtonComponent implements OnInit {
  @HostListener('click') private toggleMusic() {
    this.audio.playClick();
    this.router.navigate(['/tutorial']);
  }
  constructor(private audio: AudioService, private router: Router) { }

  ngOnInit() {}

}
