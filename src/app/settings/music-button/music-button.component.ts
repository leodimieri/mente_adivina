import {Component, HostListener, OnInit} from '@angular/core';
import {SettingsService} from '../services/settings.service';
import {AudioService} from '../../shared/services/audio.service';

@Component({
  selector: 'app-music-button',
  templateUrl: './music-button.component.html',
  styleUrls: ['./music-button.component.scss'],
})
export class MusicButtonComponent implements OnInit {
  @HostListener('click') private toggleMusic() {
    this.audio.playClick();
    this.settings.toggleMusic();
  }
  constructor(public settings: SettingsService, private audio: AudioService) { }

  ngOnInit() {}

}
