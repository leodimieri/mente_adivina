import {Component, HostListener, OnInit} from '@angular/core';
import {SettingsService} from '../services/settings.service';
import {AudioService} from '../../shared/services/audio.service';

@Component({
    selector: 'app-sound-button',
    templateUrl: './sound-button.component.html',
    styleUrls: ['./sound-button.component.scss'],
})
export class SoundButtonComponent implements OnInit {
    @HostListener('click')
    private toggleMusic() {
        this.settings.toggleSfx();
        this.audio.playClick();
    }

    constructor(public settings: SettingsService, private audio: AudioService) {
    }

    ngOnInit() {
    }

}

