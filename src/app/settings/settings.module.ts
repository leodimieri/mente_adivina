import { NgModule } from '@angular/core';
import { SettingsViewComponent } from './settings-view/settings-view.component';
import {RouterModule} from '@angular/router';
import {SelectPlayersViewComponent} from '../select-players/select-players-view/select-players-view.component';
import {SharedModule} from '../shared/shared.module';
import {MusicButtonComponent} from './music-button/music-button.component';
import {SoundButtonComponent} from './sound-button/sound-button.component';
import {LanguageButtonComponent} from './language-button/language-button.component';
import {TutorialButtonComponent} from './tutorial-button/tutorial-button.component';

@NgModule({
  declarations: [SettingsViewComponent, MusicButtonComponent, SoundButtonComponent, LanguageButtonComponent, TutorialButtonComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: SettingsViewComponent
      }
    ])

  ]
})
export class SettingsModule { }
