import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Location} from '@angular/common';
import {AudioService} from '../../shared/services/audio.service';
import {SettingsService} from '../../settings/services/settings.service';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.scss'],
})
export class TutorialComponent implements OnInit {
  private previousMusicEnabled: boolean;
  @ViewChild('videoElement', {static: false}) private video: ElementRef;
  constructor(private _location: Location, private audio: AudioService,
              private settings: SettingsService) { }

  ngOnInit() {
    if (this.settings.onChangeMusic.getValue()) {
      this.settings.toggleMusic();
      this.previousMusicEnabled = true;
    }
  }

  goBack() {
    this.video.nativeElement.pause();
    if (this.previousMusicEnabled) {
      this.settings.toggleMusic();
    }
    this._location.back();
  }
}
