import { NgModule } from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import {TutorialComponent} from './tutorial/tutorial.component';



@NgModule({
  declarations: [TutorialComponent],
  imports: [
    SharedModule,
      RouterModule.forChild([{path: '', component: TutorialComponent}])
  ]
})
export class TutorialModule { }
