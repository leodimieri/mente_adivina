import { TestBed, async, inject } from '@angular/core/testing';

import { ExitFromBoardGuard } from './exit-from-board.guard';

describe('ExitFromBoardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExitFromBoardGuard]
    });
  });

  it('should ...', inject([ExitFromBoardGuard], (guard: ExitFromBoardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
