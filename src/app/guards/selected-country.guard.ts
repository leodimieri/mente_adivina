import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {LanguageService} from '../shared/services/language.service';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SelectedCountryGuard implements CanActivate {
  constructor(protected selectedCountry: LanguageService, private router: Router) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.selectedCountry.isSelected().pipe(map(x => {
      if (!x) {
        return this.router.parseUrl('/selectLanguage');
      } else {
        return true;
      }
    }));
  }
}
