import { TestBed, async, inject } from '@angular/core/testing';

import { ExitFromCardGuard } from './exit-from-card.guard';

describe('ExitFromCardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExitFromCardGuard]
    });
  });

  it('should ...', inject([ExitFromCardGuard], (guard: ExitFromCardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
