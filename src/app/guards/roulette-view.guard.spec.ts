import { TestBed, async, inject } from '@angular/core/testing';

import { RouletteViewGuard } from './roulette-view.guard';

describe('RouletteViewGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RouletteViewGuard]
    });
  });

  it('should ...', inject([RouletteViewGuard], (guard: RouletteViewGuard) => {
    expect(guard).toBeTruthy();
  }));
});
