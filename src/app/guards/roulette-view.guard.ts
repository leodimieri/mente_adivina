import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {RouletteViewComponent} from '../roulette/roulette-view/roulette-view.component';

@Injectable({
  providedIn: 'root'
})
export class RouletteViewGuard implements CanActivate, CanDeactivate<RouletteViewComponent> {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }

  canDeactivate(component: RouletteViewComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return component.isDone;
  }
}
