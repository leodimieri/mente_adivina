import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {CardViewComponent} from '../cards/components/card-view/card-view.component';

@Injectable({
  providedIn: 'root'
})
export class ExitFromCardGuard implements CanActivate, CanDeactivate<CardViewComponent> {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }

  canDeactivate(component: CardViewComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return nextState.url.includes('settings') || component.canDeactivate();
  }
}
