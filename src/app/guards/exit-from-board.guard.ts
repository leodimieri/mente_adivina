import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {GameViewComponent} from '../game/game-view/game-view.component';

@Injectable({
  providedIn: 'root'
})
export class ExitFromBoardGuard implements CanActivate, CanDeactivate<GameViewComponent> {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }

  canDeactivate(component: GameViewComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const validNextState = ['settings', 'players', 'roulette', 'complete', 'tutorial'];
    return validNextState.some(e => nextState.url.toLowerCase().includes(e));
  }
}
