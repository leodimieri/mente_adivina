import { TestBed, async, inject } from '@angular/core/testing';

import { SelectedCountryGuard } from './selected-country.guard';

describe('SelectedCountryGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SelectedCountryGuard]
    });
  });

  it('should ...', inject([SelectedCountryGuard], (guard: SelectedCountryGuard) => {
    expect(guard).toBeTruthy();
  }));
});
