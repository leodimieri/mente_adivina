import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenFourComponent } from './token-four.component';

describe('TokenFourComponent', () => {
  let component: TokenFourComponent;
  let fixture: ComponentFixture<TokenFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TokenFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TokenFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
