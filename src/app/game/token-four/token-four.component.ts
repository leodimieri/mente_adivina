import { Component, OnInit } from '@angular/core';
import {GameService} from '../../shared/services/game.service';

@Component({
  selector: 'app-token-four',
  templateUrl: './token-four.component.html',
  styleUrls: ['./token-four.component.scss']
})
export class TokenFourComponent implements OnInit {

  constructor(public gameService: GameService) { }

  ngOnInit() {
  }

}
