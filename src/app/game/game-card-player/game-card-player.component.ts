import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../shared/models/player';
import {GameService} from '../../shared/services/game.service';

@Component({
  selector: 'app-game-card-player',
  templateUrl: './game-card-player.component.html',
  styleUrls: ['./game-card-player.component.scss']
})
export class GameCardPlayerComponent implements OnInit {

  @Input()
  public data: Player;
  constructor(public game: GameService) { }

  ngOnInit() {
  }
}
