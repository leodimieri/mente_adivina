import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameCardPlayerComponent } from './game-card-player.component';

describe('GameCardPlayerComponent', () => {
  let component: GameCardPlayerComponent;
  let fixture: ComponentFixture<GameCardPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameCardPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameCardPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
