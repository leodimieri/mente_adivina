import { NgModule } from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import { GameViewComponent } from './game-view/game-view.component';
import { BoardComponent } from './board/board.component';
import { GameCardPlayerComponent } from './game-card-player/game-card-player.component';
import { TokenOneComponent } from './token-one/token-one.component';
import { TokenTwoComponent } from './token-two/token-two.component';
import { TokenThreeComponent } from './token-three/token-three.component';
import { TokenFourComponent } from './token-four/token-four.component';
import { TokenFiveComponent } from './token-five/token-five.component';
import { TrolleyComponent } from './trolley/trolley.component';
import {ExitFromBoardGuard} from '../guards/exit-from-board.guard';
import { BoardDirective } from './board/board.directive';
import {BoardLevelOneComponent} from './board/board-level-one/board-level-one.component';
import {BoardLevelTwoComponent} from './board/board-level-two/board-level-two.component';
import {BoardLevelThreeComponent} from './board/board-level-three/board-level-three.component';
import {RouletteStartComponent} from './board/roulette-start/roulette-start.component';
import {TokenComponent} from './board/token/token.component';

@NgModule({
    declarations: [GameViewComponent, BoardComponent, GameCardPlayerComponent, TokenOneComponent, TokenTwoComponent, TokenThreeComponent,
        TokenFourComponent, TokenFiveComponent, TrolleyComponent, BoardDirective, BoardLevelOneComponent, BoardLevelTwoComponent,
        BoardLevelThreeComponent, RouletteStartComponent, TokenComponent],
    exports: [],
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: '',
                component: GameViewComponent,
                canDeactivate: [ExitFromBoardGuard]
            }
        ])
    ]
})
export class GameModule { }
