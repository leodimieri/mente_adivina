import {AfterViewInit, Component, ElementRef, Input, OnInit} from '@angular/core';
import anime from 'animejs';
@Component({
  selector: 'app-trolley',
  templateUrl: './trolley.component.html',
  styleUrls: ['./trolley.component.scss']
})
export class TrolleyComponent implements OnInit, AfterViewInit {

  @Input() public level: number;
  @Input() public robotId: number;
  constructor(private element: ElementRef) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    const ratio = window.innerWidth / window.innerHeight;
    anime({
      targets: this.element.nativeElement,
      easing: 'easeInElastic',
      duration: 3000,
      keyframes: [
        {'translateX': [ratio < 1.45 ? '-90px' : (ratio < 1.60 ? '-120px' : '-220px'), ratio < 1.60 ? '0' : '-80px']},
        {'scale': '1.05'},
        {'translateX': '400px'}
      ],
      autoplay: true,
    });
  }

}
