import { Component, OnInit } from '@angular/core';
import {GameService} from '../../shared/services/game.service';

@Component({
  selector: 'app-token-five',
  templateUrl: './token-five.component.html',
  styleUrls: ['./token-five.component.scss']
})
export class TokenFiveComponent implements OnInit {

  constructor(public gameService: GameService) { }

  ngOnInit() {
  }

}
