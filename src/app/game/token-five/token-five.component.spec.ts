import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenFiveComponent } from './token-five.component';

describe('TokenFiveComponent', () => {
  let component: TokenFiveComponent;
  let fixture: ComponentFixture<TokenFiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TokenFiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TokenFiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
