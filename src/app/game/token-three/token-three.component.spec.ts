import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenThreeComponent } from './token-three.component';

describe('TokenThreeComponent', () => {
  let component: TokenThreeComponent;
  let fixture: ComponentFixture<TokenThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TokenThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TokenThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
