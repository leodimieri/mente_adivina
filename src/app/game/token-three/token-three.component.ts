import { Component, OnInit } from '@angular/core';
import {GameService} from '../../shared/services/game.service';

@Component({
  selector: 'app-token-three',
  templateUrl: './token-three.component.html',
  styleUrls: ['./token-three.component.scss']
})
export class TokenThreeComponent implements OnInit {

  constructor(public gameService: GameService) { }

  ngOnInit() {
  }

}
