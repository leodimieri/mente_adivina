import { Component, OnInit } from '@angular/core';
import {BoardDirective} from '../board.directive';

@Component({
  selector: 'app-board-level-two',
  templateUrl: './board-level-two.component.html',
  styleUrls: ['./board-level-two.component.scss'],
})
export class BoardLevelTwoComponent extends BoardDirective implements OnInit {

  ngOnInit() {
    super.ngOnInit();
  }

}
