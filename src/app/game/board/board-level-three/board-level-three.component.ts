import { Component, OnInit } from '@angular/core';
import {BoardDirective} from '../board.directive';

@Component({
  selector: 'app-board-level-three',
  templateUrl: './board-level-three.component.html',
  styleUrls: ['./board-level-three.component.scss'],
})
export class BoardLevelThreeComponent extends BoardDirective implements OnInit {

  ngOnInit() {
    super.ngOnInit();
  }

}
