import {Component, OnInit} from '@angular/core';
import {BoardDirective} from '../board.directive';

@Component({
  selector: 'app-board-level-one',
  templateUrl: './board-level-one.component.html',
  styleUrls: ['./board-level-one.component.scss']
})
export class BoardLevelOneComponent extends BoardDirective implements OnInit {


  ngOnInit() {
    super.ngOnInit();
  }

}
