import {AfterViewInit, Component, OnInit} from '@angular/core';
import {GameService} from '../../shared/services/game.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
  public boardPositions: {x: number, y: number}[];

  constructor(public gameService: GameService) { }

  ngOnInit() {
    this.boardPositions = [
      {x: 0, y: -110},
      {x: 0, y: 0},
      {x: 0, y: 110},
      {x: 110, y: 110},
      {x: 220, y: 110},
      {x: 220, y: 0},
      {x: 220, y: -110},
      {x: 330, y: -110},
      {x: 440, y: -110},
      {x: 440, y: 0},
      {x: 440, y: 110}
    ];
  }

  getXPosition(number: number) {
    return (this.boardPositions[this.gameService.tokenPositions.get(number).currentIndex].x
        + this.gameService.tokenPositions.get(number).delta.x);

  }
  getYPosition(number: number) {
    return (this.boardPositions[this.gameService.tokenPositions.get(number).currentIndex].y
        + this.gameService.tokenPositions.get(number).delta.y);

  }

}
