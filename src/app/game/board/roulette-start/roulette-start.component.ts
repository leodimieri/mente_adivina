import {AfterViewInit, Component, ElementRef, HostListener, OnInit} from '@angular/core';
import anime from 'animejs';
import {Router} from '@angular/router';
import {AudioService} from '../../../shared/services/audio.service';
import {GameService} from '../../../shared/services/game.service';
@Component({
  selector: 'app-roulette-start',
  templateUrl: './roulette-start.component.html',
  styleUrls: ['./roulette-start.component.scss'],
})
export class RouletteStartComponent implements OnInit, AfterViewInit {
  @HostListener('click') private onClick() {
    if (this.gameService.canPlayNextTurn) {
      this.router.navigate(['/roulette']);
      this.audio.playClick();
    }
  }
  constructor(private element: ElementRef, private router: Router, private audio: AudioService,
              private gameService: GameService) { }

  ngOnInit() {}

  ngAfterViewInit(): void {
    anime({
      targets: this.element.nativeElement,
      easing: 'linear',
      duration: 1000,
      direction: 'alternate',
      scale: 1.1,
      filter: ['brightness(1)', 'brightness(1.1)'],
      autoplay: true,
      loop: true
    });

  }

}
