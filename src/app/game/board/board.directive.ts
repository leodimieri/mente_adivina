import {Directive, OnInit} from '@angular/core';
import {GameService} from '../../shared/services/game.service';

@Directive({
  selector: '[appBoard]'
})
export class BoardDirective implements OnInit {

  public boardPositions: {x: number, y: number}[];

  constructor(public gameService: GameService) { }

  ngOnInit() {
    this.boardPositions = [
      {x: 0, y: -120},
      {x: 0, y: 30},
      {x: 0, y: 140},
      {x: 110, y: 140},
      {x: 220, y: 140},
      {x: 220, y: 30},
      {x: 220, y: -80},
      {x: 220, y: -190},
      {x: 330, y: -190},
      {x: 440, y: -190},
      {x: 440, y: -80},
      {x: 440, y: 30},
      {x: 440, y: 140}
    ];
  }

  getXPosition(number: number) {
    return (this.boardPositions[this.gameService.tokenPositions.get(number).currentIndex].x
        + this.gameService.tokenPositions.get(number).delta.x);

  }
  getYPosition(number: number) {
    return (this.boardPositions[this.gameService.tokenPositions.get(number).currentIndex].y
        + this.gameService.tokenPositions.get(number).delta.y);

  }
}
