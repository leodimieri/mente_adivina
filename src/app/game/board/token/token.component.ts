import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-token',
  templateUrl: './token.component.html',
  styleUrls: ['./token.component.scss'],
})
export class TokenComponent implements OnInit {
  @Input() public color: string;
  constructor() { }

  ngOnInit() {}

}
