import { Component, OnInit } from '@angular/core';
import {GameService} from '../../shared/services/game.service';

@Component({
  selector: 'app-token-two',
  templateUrl: './token-two.component.html',
  styleUrls: ['./token-two.component.scss']
})
export class TokenTwoComponent implements OnInit {

  constructor(public gameService: GameService) { }

  ngOnInit() {
  }

}
