import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenTwoComponent } from './token-two.component';

describe('TokenTwoComponent', () => {
  let component: TokenTwoComponent;
  let fixture: ComponentFixture<TokenTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TokenTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TokenTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
