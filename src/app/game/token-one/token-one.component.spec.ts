import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenOneComponent } from './token-one.component';

describe('TokenOneComponent', () => {
  let component: TokenOneComponent;
  let fixture: ComponentFixture<TokenOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TokenOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TokenOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
