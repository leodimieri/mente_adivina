import { Component, OnInit } from '@angular/core';
import {GameService} from '../../shared/services/game.service';

@Component({
  selector: 'app-token-one',
  templateUrl: './token-one.component.html',
  styleUrls: ['./token-one.component.scss']
})
export class TokenOneComponent implements OnInit {

  constructor(public gameService: GameService) { }

  ngOnInit() {
  }

}
