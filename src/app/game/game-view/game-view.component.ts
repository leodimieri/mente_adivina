import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {GameService} from '../../shared/services/game.service';
import {Subscription, timer} from 'rxjs';
import {AudioService} from '../../shared/services/audio.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-game-view',
  templateUrl: './game-view.component.html',
  styleUrls: ['./game-view.component.scss']
})
export class GameViewComponent implements OnInit {
    private subscription: Subscription;
  constructor(private router: Router, public gameService: GameService,
              private audioService: AudioService) { }

  ngOnInit() {
  }

  public ionViewDidEnter() {
    if (this.gameService.pendingPoints.length === 0) {
      return;
    }
    this.gameService.onBoardIsLoaded();
      this.subscription = timer(1000).pipe(take(1)).subscribe(val => {
        this.ionViewDidEnter();
        this.subscription.unsubscribe();
      });
  }
}
