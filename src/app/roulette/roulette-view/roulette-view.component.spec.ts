import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouletteViewComponent } from './roulette-view.component';

describe('RouletteViewComponent', () => {
  let component: RouletteViewComponent;
  let fixture: ComponentFixture<RouletteViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouletteViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouletteViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
