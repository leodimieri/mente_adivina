import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouletteCardPlayerComponent } from './roulette-card-player.component';

describe('RouletteCardPlayerComponent', () => {
  let component: RouletteCardPlayerComponent;
  let fixture: ComponentFixture<RouletteCardPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouletteCardPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouletteCardPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
