import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../shared/models/player';

@Component({
  selector: 'app-roulette-card-player',
  templateUrl: './roulette-card-player.component.html',
  styleUrls: ['./roulette-card-player.component.scss']
})
export class RouletteCardPlayerComponent implements OnInit {


  @Input()
  public data: Player;
  constructor() { }

  ngOnInit() {
    console.log(this.data);
  }
}
