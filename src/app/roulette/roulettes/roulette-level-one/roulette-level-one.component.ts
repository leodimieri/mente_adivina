import {Component, ElementRef, OnInit} from '@angular/core';
import {RouletteDirectiveDirective} from '../roulette-directive.directive';
import {AudioService} from '../../../shared/services/audio.service';
import {GameService} from '../../../shared/services/game.service';
import {GameSessionService} from '../../../shared/services/game-session.service';

@Component({
  selector: 'app-roulette-level-one',
  templateUrl: './roulette-level-one.component.html',
  styleUrls: ['./roulette-level-one.component.scss'],
})
export class RouletteLevelOneComponent extends RouletteDirectiveDirective {

  constructor(element: ElementRef, audioService: AudioService, gameService: GameService, gameSession: GameSessionService) {
    super(element, audioService, gameService, gameSession);
  }

}
