import {Component, ElementRef, OnInit} from '@angular/core';
import {RouletteDirectiveDirective} from '../roulette-directive.directive';
import {AudioService} from '../../../shared/services/audio.service';
import {GameService} from '../../../shared/services/game.service';
import {GameSessionService} from '../../../shared/services/game-session.service';

@Component({
  selector: 'app-roulette-level-three',
  templateUrl: './roulette-level-three.component.html',
  styleUrls: ['./roulette-level-three.component.scss'],
})
export class RouletteLevelThreeComponent extends RouletteDirectiveDirective {
  constructor(element: ElementRef, audioService: AudioService, gameService: GameService, gameSession: GameSessionService) {
    super(element, audioService, gameService, gameSession);
  }

}
