import {Directive, ElementRef, EventEmitter, HostListener, Output, ViewChild} from '@angular/core';
import {AudioService} from '../../shared/services/audio.service';
import {GameService} from '../../shared/services/game.service';
import {timer} from 'rxjs';
import {first} from 'rxjs/operators';
import anime from 'animejs';
import {GameSessionService} from '../../shared/services/game-session.service';
import {RouletteStep} from '../../shared/models/cards/card';
@Directive({
  selector: '[appRouletteDirective]'
})
export class RouletteDirectiveDirective {
  private currentStepToGo: RouletteStep;
  constructor(private element: ElementRef, private audioService: AudioService,
              private gameService: GameService, private gameSession: GameSessionService) { }
  private degsToRotate: number;
  public freeChoose: boolean;
  @ViewChild('toRotate', {static: false}) toRotate: ElementRef;
  @Output() public doneAnimation: EventEmitter<void> = new EventEmitter<void>();
  public animationEnd: boolean;
  private freeChooseAnimation;
  @HostListener('click') public rotate() {
    if (this.degsToRotate || this.freeChoose) {
      return;
    }
    this.audioService.playClick();
    this.audioService.playSfx('197965__augdog__waterdrop.wav', true);
    this.currentStepToGo = this.gameSession.getNextCategoryRoulette();
    this.degsToRotate = 735 + this.converStepToRouletteNumber() * 30 + Math.floor(Math.max(Math.random(), Math.random(), Math.random()) * 100) * 360;
    /*
    const 0 = ((735 + (Math.floor(Math.max(Math.random(), Math.random(), Math.random()) * 100)) * 30) - 720) / 30 % 12 - quantity;
*/

    anime({
      targets: this.toRotate.nativeElement,
      easing: 'easeOutCirc',
      duration: 2000,
      direction: 'alternate',
      rotate: this.degsToRotate,
      autoplay: true,
      loop: false,
      complete: function () {
        this.audioService.pauseSfx();
        this.onRouletteDone();
        this.animationEnd = true;
      }.bind(this)
    });
  }

  ngOnInit() {}

  onRouletteDone() {
    this.doneAnimation.emit();
    const quantity = (this.degsToRotate - 720) / 30 % 12;
    timer(2000).pipe(first()).subscribe(value => {
      this.executeRouletteAction(quantity);
      this.degsToRotate = undefined;
    });
  }

  private executeRouletteAction(quantity) {
    if (quantity < 1) {
       this.audioService.playSfx('350984__cabled-mess__lose-c-03.wav');
       this.gameService.onLoseTurn();
    } else if (quantity < 3) {
      this.gameService.generatePhrase();
    } else if (quantity < 5) {
      this.gameService.generateGuessword();
    } else if (quantity < 7) {
      this.gameService.generateSpelling();
    } else if (quantity < 9) {
      this.gameService.generateRiddle();
    } else if (quantity < 11) {
      this.freeChoose = true;
      this.startFreeToChoose();
       this.audioService.playSfx('396193__plasterbrain__ui-cute-select-major-6th (mp3cut.net).mp3');
     } else {
       this.audioService.playSfx('350984__cabled-mess__lose-c-03.wav');
       this.gameService.onLoseTurn();
    }
  }

  public onChooseGuessword() {
    if (this.freeChoose) {
      this.freeChooseAnimation.pause();
      this.freeChooseAnimation = undefined;
      this.audioService.playClick();
      this.gameService.generateGuessword();
    }
  }

  public onChoosePhrase() {
    if (this.freeChoose) {
      this.freeChooseAnimation.pause();
      this.freeChooseAnimation = undefined;
      this.audioService.playClick();
      this.gameService.generatePhrase();
    }
  }

  public onChooseRiddle() {
    if (this.freeChoose) {
      this.freeChooseAnimation.pause();
      this.freeChooseAnimation = undefined;
      this.audioService.playClick();
      this.gameService.generateRiddle();
    }
  }

  public onChooseSpelling() {
    if (this.freeChoose) {
      this.freeChooseAnimation.pause();
      this.freeChooseAnimation = undefined;
      this.audioService.playClick();
      this.gameService.generateSpelling();
    }

  }

  private converStepToRouletteNumber(): number {
    let possibilities: number[];
    switch (this.currentStepToGo) {
      case RouletteStep.LooseTurn: {
        possibilities = [0, 11];
        break;
      }
      case RouletteStep.FreeChoose: {
        possibilities = [9, 10];
        break;
      }
      case RouletteStep.Orange: {
        possibilities = [7, 8];
        break;
      }
      case RouletteStep.Blue: {
        possibilities = [5, 6];
        break;
      }
      case RouletteStep.Fuchsia: {
        possibilities = [3, 4];
        break;
      }
      case RouletteStep.Green: {
        possibilities = [1, 2];
        break;
      }
      default: {
        console.log('Error! No entro en ninguna! Error!');
      }
    }
    return possibilities[Math.floor(Math.random() * possibilities.length)];
  }

  private startFreeToChoose() {
    this.freeChooseAnimation = anime({
      targets: '.clickable-section',
      easing: 'linear',
      direction: 'alternate',
      duration: 2000,
      keyframes: [
        {
          opacity: '0.25',
          filter: '',
        },
        {
          filter: 'brightness(2)',
        },
        {
          filter: '',
          opacity: '1',
        },
      ],
      autoplay: true,
      loop: true,
    });

  }
}
