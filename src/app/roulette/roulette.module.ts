import { NgModule } from '@angular/core';
import { RouletteViewComponent } from './roulette-view/roulette-view.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import { RouletteCardPlayerComponent } from './roulette-card-player/roulette-card-player.component';
import {RouletteViewGuard} from '../guards/roulette-view.guard';
import { RouletteDirectiveDirective } from './roulettes/roulette-directive.directive';
import {RouletteLevelOneComponent} from './roulettes/roulette-level-one/roulette-level-one.component';
import {RouletteLevelTwoComponent} from './roulettes/roulette-level-two/roulette-level-two.component';
import {RouletteLevelThreeComponent} from './roulettes/roulette-level-three/roulette-level-three.component';

@NgModule({
    declarations: [RouletteViewComponent, RouletteCardPlayerComponent, RouletteDirectiveDirective, RouletteLevelOneComponent,
        RouletteLevelTwoComponent, RouletteLevelThreeComponent],
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: '',
                component: RouletteViewComponent,
                canDeactivate: [RouletteViewGuard]
            }
        ])
    ]
})
export class RouletteModule { }
