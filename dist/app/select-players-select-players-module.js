(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["select-players-select-players-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/select-players/check-button/check-button.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/select-players/check-button/check-button.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 80.8 77.3\" style=\"height: 8vmax\">\n  <circle cx=\"38.4\" cy=\"38.3\" r=\"37.5\" opacity=\".24\" fill=\"#a7a9ac\"></circle>\n  <circle cx=\"41.4\" cy=\"38.3\" r=\"37.5\" fill=\"#a7a9ac\"></circle>\n  <circle cx=\"41.4\" cy=\"38.3\" r=\"28.5\" fill=\"#f68b1f\"></circle>\n  <path d=\"M41.3 57.1c-1.1 0-2.1-.5-2.7-1.3L25.5 39.2c-1.2-1.5-.9-3.7.6-4.8 1.5-1.2 3.7-.9 4.8.6l10 12.7 12.8-20.9c1-1.6 3.1-2.1 4.7-1.1s2.1 3.1 1.1 4.7l-15.3 25c-.6 1-1.6 1.6-2.7 1.6-.1.1-.2.1-.2.1z\"\n        fill=\"#db7c1b\" stroke=\"#db7c1b\" stroke-width=\"2\" stroke-miterlimit=\"10\"></path>\n  <path d=\"M39.3 55.1c-1.1 0-2.1-.5-2.7-1.3L23.5 37.2c-1.2-1.5-.9-3.7.6-4.8 1.5-1.2 3.7-.9 4.8.6l10 12.7 12.8-20.9c1-1.6 3.1-2.1 4.7-1.1s2.1 3.1 1.1 4.7l-15.3 25c-.6 1-1.6 1.6-2.7 1.6-.1.1-.2.1-.2.1z\"\n        fill=\"#fff\" stroke=\"#fff\" stroke-width=\"2\" stroke-miterlimit=\"10\"></path>\n</svg>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/select-players/main-monitor/main-monitor.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/select-players/main-monitor/main-monitor.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div fxFlex=\"grow\" fxLayout=\"column\" fxLayoutAlign=\"center center\" fxLayoutGap=\"0\"\n     xmlns:xhtml=\"http://www.w3.org/1999/xhtml\">\n<!--  <svg (click)=\"prevRobot()\" fxFlex=\"20\" xmlns=\"http://www.w3.org/2000/svg\" style=\"enable-background:new 0 0 178 118\" viewBox=\"0 0 178 118\">\n    <path style=\"fill:#8a8c8e\" d=\"M129.5 7.8H48.9L8.6 57.3l40.3 49.4h80.6l40.3-49.4z\"></path>\n    <path d=\"M65.4 39.3l-13 18.2 12.4 17.4c.5.6.9 1.2 1.3 1.7s.7 1.1.9 1.7c.2.6.3 1.2.3 1.8 0 1.4-.6 2.5-1.9 3.5-1.3 1-2.7 1.5-4.4 1.5-2.6 0-5.2-1.6-7.6-4.9L40.7 63.6c-2-2.5-3-4.6-3-6.2 0-1.2 1-3.2 3-5.8l12.1-16.1c1.6-2.2 3.1-3.7 4.3-4.5 1.2-.8 2.5-1.2 3.9-1.2 1.8 0 3.4.5 4.6 1.5 1.2 1 1.9 2.3 1.9 3.7-.1 1.4-.7 2.8-2.1 4.3z\" style=\"fill:#fff\"></path>\n    <path style=\"fill:#77787b\" d=\"M105.4 106.3H93.7L92.7 8h11.7z\"></path>\n  </svg>-->\n\n  <svg preserveAspectRatio=\"xMidYMax\" fxFlex=\"grow\" xmlns=\"http://www.w3.org/2000/svg\"\n       viewBox=\"-90 0 528 438\">\n    <g id=\"prev-arrow\" transform=\"translate(-77,0)\" (click)=\"prevRobot()\">\n      <path style=\"fill: rgba(255,255,255,0.02)\" class=\"arrow-st0\" d=\"M127.5 118.1H46.9L6.7 167.5 46.9 217h80.6l40.3-49.5z\"></path>\n      <path class=\"arrow-st1\"\n            d=\"M63.5 149.6l-13.1 18.2 12.4 17.4c.5.6.9 1.2 1.3 1.7s.7 1.1.9 1.7c.2.6.3 1.2.3 1.8 0 1.4-.6 2.5-1.9 3.5-1.3 1-2.7 1.5-4.4 1.5-2.6 0-5.2-1.6-7.6-4.9l-12.6-16.6c-2-2.5-3-4.6-3-6.2 0-1.2 1-3.2 3-5.8l12.1-16.1c1.6-2.2 3.1-3.7 4.3-4.5 1.2-.8 2.5-1.2 3.9-1.2 1.8 0 3.4.5 4.6 1.5 1.2 1 1.9 2.3 1.9 3.7-.1 1.4-.8 2.8-2.1 4.3z\"></path>\n      <path class=\"arrow-st2\" style=\"fill: rgba(255,255,255,0.02)\" d=\"M103.5 216.5H91.7l-1-98.2h11.8z\"></path>\n    </g>\n    <g id=\"next-arrow\" transform=\"translate(-95,0)\" (click)=\"nextRobot()\">\n      <path style=\"fill: rgba(255,255,255,0.02)\" d=\"M392.9 217h80.6l40.3-49.5-40.3-49.4h-80.6l-40.2 49.4z\"></path>\n      <path class=\"arrow-st1\"\n            d=\"M457 185.5l13.1-18.2-12.4-17.4c-.5-.6-.9-1.2-1.3-1.7s-.7-1.1-.9-1.7c-.2-.6-.3-1.2-.3-1.8 0-1.4.6-2.5 1.9-3.5 1.3-1 2.7-1.5 4.4-1.5 2.6 0 5.2 1.6 7.6 4.9l12.6 16.6c2 2.5 3 4.6 3 6.2 0 1.2-1 3.2-3 5.8l-12.1 16.1c-1.6 2.2-3.1 3.7-4.3 4.5-1.2.8-2.5 1.2-3.9 1.2-1.8 0-3.4-.5-4.6-1.5-1.2-1-1.9-2.3-1.9-3.7.1-1.4.7-2.8 2.1-4.3z\"></path>\n      <path style=\"fill: rgba(255,255,255,0.02)\" d=\"M414.2 216.5H426l1-98.2h-11.8z\"></path>\n    </g>\n    <g>\n      <path class=\"st1\" d=\"M100.5 522.3l28.6 17s143.2-186.8 7.4-384.8l-58-14.5c-.1 0 152.7 235.3 22 382.3z\"></path>\n      <path d=\"M105.6 520.1l12.4 7.4s125.5-173-7.5-379.2l-26.1-6c0-.1 151.9 230.8 21.2 377.8z\" opacity=\".29\"\n            fill=\"#848689\"></path>\n      <path class=\"st3\"\n            d=\"M112.7 510.1c3.1 3.6 6.6 6.7 10.5 9.2 1.9 1.3 4 2.3 6.1 3.2 2.1.9 4.4 1.5 6.7 2.1-2.4.1-4.8-.1-7.1-.9l-.9-.2c-.3-.1-.6-.2-.8-.3-.6-.2-1.1-.4-1.7-.7-1.1-.6-2.2-1.1-3.2-1.8-2-1.3-3.9-2.9-5.5-4.6-1.7-1.9-3.1-3.9-4.1-6zM117.1 504.3c3.7 3 7.6 5.5 11.8 7.3 2.1.9 4.3 1.6 6.6 2.1 2.3.6 4.6.7 6.9.9-2.3.4-4.8.6-7.1.3l-.9-.1c-.3-.1-.6-.1-.9-.2-.6-.1-1.2-.2-1.8-.4-1.2-.4-2.3-.7-3.4-1.2-2.2-1-4.3-2.2-6.2-3.7-1.9-1.3-3.6-3-5-5zM120.7 499.3c3.9 2.7 7.9 5 12.3 6.5 2.2.8 4.4 1.3 6.7 1.7 2.3.4 4.6.4 7 .5-2.3.6-4.7 1-7.1.8h-.9c-.3 0-.6-.1-.9-.1l-1.8-.3c-1.2-.3-2.4-.6-3.5-1-2.3-.8-4.5-1.9-6.4-3.2-2.1-1.4-3.9-3-5.4-4.9z\"></path>\n      <path class=\"st4\"\n            d=\"M125.1 492c2.1 1.1 4.2 2.1 6.4 2.9 2.1.8 4.3 1.6 6.5 2.1 2.2.6 4.4.9 6.7 1.1 2.3.2 4.6.1 7 .1-2.2.9-4.6 1.6-7 1.7l-.9.1h-.9c-.6 0-1.2 0-1.8-.1-1.2-.2-2.4-.3-3.6-.6-2.4-.6-4.6-1.5-6.7-2.7-2.2-1.3-4.1-2.7-5.7-4.6zM133.6 478.3c2.1 1.1 4.2 2.2 6.3 3.1 2.1.9 4.3 1.7 6.5 2.3 2.2.7 4.4 1.1 6.7 1.3 2.3.3 4.6.2 7 .3-2.2.9-4.6 1.5-7 1.5h-.9c-.3 0-.6 0-.9-.1-.6 0-1.2-.1-1.8-.1-1.2-.2-2.4-.4-3.6-.7-2.3-.7-4.6-1.6-6.7-2.9-2.2-1.3-4.1-2.8-5.6-4.7z\"></path>\n      <path class=\"st3\"\n            d=\"M138.6 465.6c4 2.5 8.2 4.4 12.7 5.6 2.2.6 4.5 1 6.8 1.2 2.3.3 4.6.1 7 0-2.2.8-4.6 1.3-7 1.3h-.9c-.3 0-.6 0-.9-.1-.6-.1-1.2-.1-1.8-.1-1.2-.2-2.4-.4-3.5-.7-2.3-.7-4.6-1.6-6.6-2.8-2.2-1.2-4.1-2.7-5.8-4.4zM143 454c3.9 2.3 7.9 4 12.2 5 1.1.3 2.1.4 3.2.6 1.1.1 2.2.2 3.3.2 2.2.1 4.4-.2 6.6-.5-2.1.9-4.3 1.6-6.6 1.7-1.1.2-2.3 0-3.5.1-1.1-.1-2.3-.2-3.4-.5-2.2-.5-4.4-1.3-6.4-2.4-1.9-1.2-3.8-2.5-5.4-4.2z\"></path>\n      <path class=\"st4\"\n            d=\"M145.9 447.1c1.9.8 3.7 1.5 5.5 2.1 1.8.6 3.7 1.1 5.6 1.4.9.2 1.9.3 2.8.4.9 0 1.9.2 2.9.1 1.9.1 3.9-.2 5.9-.4-1.7 1-3.7 1.8-5.8 2.1-1 .2-2.1.2-3.1.3-1-.1-2.1-.1-3.1-.3-2.1-.4-4-1-5.9-2-1.8-.9-3.5-2.1-4.8-3.7zM151.4 419.4c2.4.7 4.8 1.4 7.2 1.9 2.4.5 4.7.8 7.1 1 1.2.1 2.4.1 3.6.1 1.2-.1 2.4-.1 3.6-.2 2.4-.2 4.8-.8 7.3-1.2-2 1.5-4.4 2.8-6.9 3.4-.6.2-1.3.4-1.9.4-.6.1-1.3.2-1.9.3-1.3.1-2.6.1-3.9 0-2.6-.2-5.2-.7-7.6-1.7-2.5-.9-4.7-2.2-6.6-4z\"></path>\n      <path class=\"st3\"\n            d=\"M148.8 435.8c4.1 2.2 8.3 3.8 12.7 4.5 1.1.2 2.2.3 3.3.4 1.1 0 2.2.1 3.3.1 2.3 0 4.5-.4 6.8-.8-2.1 1-4.3 1.8-6.7 2-1.2.2-2.3.2-3.5.2-1.2-.1-2.4-.2-3.5-.4-2.3-.4-4.6-1.1-6.7-2.2-2.1-.9-4.1-2.2-5.7-3.8zM149.7 425.9c4.1 2.6 8.4 4.6 13 5.7 1.1.3 2.3.5 3.5.7 1.2.1 2.3.3 3.5.3 2.4.2 4.7-.1 7.2-.3-2.3.9-4.7 1.6-7.2 1.6-1.2.1-2.5 0-3.7-.1-1.2-.2-2.5-.3-3.7-.7-2.4-.6-4.7-1.5-6.9-2.8-2.1-1-4.1-2.5-5.7-4.4zM152.7 402c2.8 1.1 5.7 2.1 8.6 2.8 2.9.7 5.8 1.3 8.8 1.5 2.9.3 5.9.3 8.9 0 3-.2 5.9-.9 8.9-1.6-2.7 1.5-5.6 2.7-8.7 3.2l-1.1.2c-.4.1-.8.1-1.2.1-.8.1-1.5.2-2.3.2-1.6 0-3.1 0-4.7-.2-3.1-.3-6.1-1-9-2-3-.9-5.8-2.3-8.2-4.2zM154.4 392.8c3 .7 6 1.1 8.9 1.4 3 .2 5.9.3 8.9.1 3-.2 5.9-.7 8.7-1.4 2.9-.7 5.6-1.9 8.5-3-2.4 1.9-5.1 3.5-8 4.5l-1.1.4c-.4.1-.7.2-1.1.3-.8.2-1.5.4-2.3.6-1.5.3-3.1.5-4.6.6-3.1.2-6.2 0-9.3-.6-2.9-.5-5.9-1.4-8.6-2.9zM155.5 385.1c3 .5 6 .7 9 .8 3 .1 6-.1 8.9-.5 2.9-.3 5.8-1 8.6-2 2.9-.9 5.5-2.2 8.3-3.6-2.3 2-4.8 3.9-7.7 5.1l-1.1.5c-.4.1-.7.3-1.1.4-.7.2-1.5.5-2.2.7-1.5.4-3 .7-4.6.9-3.1.4-6.2.4-9.3.1-3-.5-6-1.2-8.8-2.4z\"></path>\n      <path class=\"st4\"\n            d=\"M156.3 374.1c3.1 0 6.1-.1 9-.4 3-.3 5.9-.8 8.7-1.4 1.4-.3 2.8-.8 4.2-1.1.7-.2 1.4-.5 2.1-.7.7-.2 1.4-.5 2-.8 2.8-1.1 5.3-2.6 8-4-1.9 2.4-4.3 4.5-7 6.1l-1 .6c-.3.2-.7.3-1.1.5-.7.3-1.4.7-2.1 1-1.5.5-3 1.1-4.5 1.4-3.1.7-6.2 1-9.3.8-3-.3-6.1-.8-9-2zM158 353.4c3.1.1 6.1 0 9-.1 3-.2 5.9-.6 8.7-1.2 1.4-.2 2.8-.7 4.3-1 .7-.2 1.4-.4 2.1-.7.7-.2 1.4-.5 2.1-.7 2.8-1 5.4-2.4 8.1-3.8-2 2.3-4.4 4.4-7.2 5.8l-1 .6c-.4.2-.7.3-1.1.5-.7.3-1.4.6-2.2.9-1.5.5-3 1-4.5 1.2-3.1.6-6.2.8-9.3.6-3.1-.2-6.2-.8-9-2.1z\"></path>\n      <path class=\"st3\"\n            d=\"M156.3 335.9c3 .2 6 .3 9 .1 3-.2 5.9-.5 8.8-1.1 2.9-.6 5.7-1.5 8.5-2.6s5.3-2.6 8-4.2c-2.1 2.2-4.5 4.2-7.3 5.6l-1 .6c-.3.2-.7.3-1.1.5-.7.3-1.4.6-2.2.9-1.5.5-3 .9-4.5 1.2-3 .6-6.1.9-9.2.7-3.1-.2-6.2-.7-9-1.7zM154.5 320.1c2.9.2 5.8.1 8.6-.1s5.6-.7 8.3-1.4c1.4-.3 2.7-.8 4-1.2 1.3-.5 2.6-1 3.9-1.7 2.6-1.2 4.9-2.8 7.3-4.5-1.8 2.2-4 4.3-6.5 5.8l-.9.6c-.3.2-.7.3-1 .5l-2 1c-1.4.5-2.8 1.1-4.2 1.4-2.9.7-5.8 1.1-8.8 1-3 0-5.9-.4-8.7-1.4z\"></path>\n      <path class=\"st4\"\n            d=\"M153.7 310.5c2.6-.2 5.1-.4 7.6-.8s4.9-.9 7.2-1.7c1.2-.3 2.3-.8 3.5-1.2 1.1-.5 2.3-.9 3.3-1.5 2.3-1 4.3-2.5 6.6-3.9-1.4 2.2-3.2 4.3-5.4 5.8-1 .9-2.3 1.4-3.4 2.1-1.2.5-2.5 1.1-3.7 1.5-2.6.8-5.2 1.2-7.9 1.2-2.7-.1-5.3-.5-7.8-1.5zM143.8 275.6c3.2-.6 6.3-1.2 9.3-2.1 3-.8 5.9-1.8 8.7-3 1.4-.6 2.8-1.3 4.2-2 1.3-.8 2.7-1.5 4-2.4 2.7-1.6 5-3.7 7.6-5.7-1.4 2.9-3.4 5.7-5.9 7.9l-.9.9c-.3.3-.7.5-1 .8-.7.5-1.4 1-2 1.5-1.5.8-2.9 1.7-4.5 2.3-3.1 1.3-6.4 2.2-9.7 2.5-3.3.3-6.7.2-9.8-.7z\"></path>\n      <path class=\"st3\"\n            d=\"M150.4 295.8c3 .1 5.9-.1 8.8-.5 2.9-.4 5.7-.9 8.4-1.8 1.4-.4 2.7-.9 4.1-1.4 1.3-.6 2.6-1.2 3.9-1.9 2.6-1.3 4.9-3.1 7.3-4.9-1.8 2.4-3.9 4.6-6.4 6.2l-.9.7c-.3.2-.7.4-1 .6l-2 1.1c-1.4.6-2.8 1.2-4.2 1.6-2.9.9-5.9 1.4-8.9 1.4-3.2.1-6.2-.2-9.1-1.1zM145.7 284c3.1.3 6.2.3 9.3.2 3.1-.2 6.1-.6 9-1.3 1.5-.3 2.9-.8 4.4-1.2 1.4-.5 2.9-1 4.2-1.7 2.8-1.2 5.4-2.9 8-4.6-2.1 2.3-4.4 4.5-7.2 6.1l-1 .6c-.3.2-.7.3-1.1.5l-2.2 1c-1.5.5-3 1.1-4.6 1.4-3.1.7-6.3 1-9.5.8-3.2-.1-6.4-.6-9.3-1.8z\"></path>\n      <g>\n        <path class=\"st3\"\n              d=\"M137.7 256.8c3.1.3 6.3.5 9.4.4 3.1-.1 6.2-.4 9.2-.9 3-.5 6-1.3 8.9-2.4 2.9-1 5.6-2.6 8.4-4.1-2.3 2.2-4.8 4.2-7.7 5.6l-1.1.6c-.4.2-.7.3-1.1.4-.8.3-1.5.6-2.3.9-1.5.4-3.1.9-4.7 1.2-3.2.5-6.4.7-9.6.5-3.3-.4-6.5-1-9.4-2.2z\"></path>\n        <path class=\"st4\"\n              d=\"M134.3 246.8c3.2-.3 6.2-.8 9.2-1.4 3-.6 5.9-1.5 8.8-2.5 1.4-.5 2.8-1.1 4.2-1.7.7-.3 1.4-.7 2-1 .7-.3 1.4-.7 2-1.1 2.7-1.4 5.2-3.3 7.8-5.1-1.7 2.6-3.9 5.1-6.6 7l-1 .7c-.3.2-.7.4-1 .6-.7.4-1.4.9-2.1 1.2-1.5.7-2.9 1.4-4.5 1.9-3.1 1.1-6.3 1.7-9.5 2-3 .5-6.2.3-9.3-.6z\"></path>\n        <path class=\"st3\"\n              d=\"M133.2 238.8c3.1-.4 6.2-1 9.2-1.7 3-.8 5.9-1.8 8.7-3 2.8-1.2 5.5-2.7 8.1-4.4 2.6-1.7 4.9-3.8 7.3-5.9-1.7 2.7-3.7 5.2-6.3 7.2l-.9.8c-.3.2-.7.5-1 .7-.7.4-1.3.9-2 1.3-1.4.8-2.8 1.6-4.3 2.2-3 1.3-6.1 2.2-9.2 2.7-3.3.4-6.5.5-9.6.1z\"></path>\n        <path class=\"st4\"\n              d=\"M130.9 227.6c3.1-.9 6-1.8 8.9-2.9 2.9-1.1 5.6-2.4 8.3-3.8 1.4-.7 2.6-1.5 3.9-2.3.6-.4 1.2-.9 1.9-1.3l.9-.7.9-.7c2.5-1.8 4.6-4 6.9-6.3-1.3 2.9-3 5.7-5.3 8l-.8.9c-.3.3-.6.5-.9.8-.6.5-1.2 1.1-1.9 1.6l-2 1.4c-.7.5-1.4.8-2.1 1.2-2.8 1.6-5.9 2.7-9 3.4-3.3.8-6.5 1.1-9.7.7zM126.2 213.5c3.1-.8 6.1-1.7 9-2.7 2.9-1 5.7-2.2 8.4-3.6 1.4-.6 2.6-1.5 4-2.2.6-.4 1.3-.8 1.9-1.2l1-.6.9-.7c2.5-1.8 4.7-3.9 7.1-6.1-1.4 2.9-3.2 5.6-5.5 7.8l-.9.9c-.3.3-.6.5-.9.8-.6.5-1.3 1-1.9 1.5l-2 1.3c-.7.5-1.4.8-2.1 1.2-2.9 1.5-6 2.6-9.1 3.2-3.5.6-6.8.8-9.9.4z\"></path>\n        <path class=\"st3\"\n              d=\"M119.3 197.4c3.1-.6 6.1-1.4 9-2.4 2.9-1 5.8-2.2 8.5-3.6s5.3-3.1 7.7-4.9c2.5-1.8 4.6-4.1 6.8-6.4-1.5 2.8-3.3 5.5-5.7 7.6l-.9.8c-.3.3-.6.5-.9.8-.6.5-1.3 1-1.9 1.5-1.3.9-2.7 1.8-4.1 2.5-2.9 1.5-5.9 2.6-9 3.3-3.1.7-6.3 1.1-9.5.8zM112.7 183.3c2.9-.6 5.8-1.5 8.5-2.5s5.4-2.2 7.9-3.7c1.3-.7 2.4-1.5 3.7-2.3 1.1-.9 2.3-1.7 3.4-2.8 2.2-1.9 4.1-4.2 6-6.5-1.2 2.7-2.8 5.4-4.9 7.6l-.8.9c-.3.3-.6.5-.8.8l-1.7 1.5c-1.2.9-2.5 1.8-3.8 2.6-2.7 1.5-5.5 2.7-8.5 3.5-2.9.7-6 1.1-9 .9z\"></path>\n        <path class=\"st4\"\n              d=\"M108.2 169.1c2.7-.6 5.2-1.2 7.7-2s4.8-1.7 7.1-2.8c1.2-.5 2.2-1.2 3.4-1.8 1.1-.7 2.2-1.3 3.2-2.1 2.2-1.4 4-3.2 6.1-4.9-1.1 2.5-2.6 4.9-4.6 6.8-.9 1-2.1 1.8-3.2 2.7-1.2.7-2.3 1.5-3.6 2.1-2.5 1.2-5.2 2-7.9 2.4-2.8.2-5.6.3-8.2-.4z\"></path>\n      </g>\n    </g>\n    <path class=\"st5\" d=\"M210.7 346.7l-1.9 39.2s-18.6 1.6-36.7-.6c-18.2-2.2-31.7-8-31.7-8L145 345l65.7 1.7z\"></path>\n    <path d=\"M208.7 345.8l-2.1 37.7s-8.4 2.4-26.6.2c-5.8-57.1 28.7-37.9 28.7-37.9z\" fill=\"#58595b\"></path>\n    <ellipse fill=\"#b1b3b6\" transform=\"rotate(-84.679 148.597 371.94) scale(1.00001)\" cx=\"148.6\"\n             cy=\"371.9\" rx=\"2.6\" ry=\"2.6\"></ellipse>\n    <path fill=\"none\" stroke=\"#6d6e71\" stroke-width=\"1.23\" stroke-miterlimit=\"10\"\n          d=\"M150.7 374.4l-4.3-4.9\"></path>\n    <g>\n      <ellipse fill=\"#b1b3b6\" transform=\"rotate(-70.641 174.872 377.65)\" cx=\"174.9\" cy=\"377.7\" rx=\"2.6\"\n               ry=\"2.6\"></ellipse>\n      <path fill=\"none\" stroke=\"#6d6e71\" stroke-width=\"1.23\" stroke-miterlimit=\"10\"\n            d=\"M172 379.1l5.8-3\"></path>\n    </g>\n    <g>\n      <ellipse fill=\"#b1b3b6\" transform=\"rotate(-70.641 201.67 379.026)\" cx=\"201.7\" cy=\"379\" rx=\"2.6\" ry=\"2.6\"></ellipse>\n      <path fill=\"none\" stroke=\"#6d6e71\" stroke-width=\"1.23\" stroke-miterlimit=\"10\"\n            d=\"M198.8 380.4l5.8-2.9\"></path>\n    </g>\n    <g>\n      <path d=\"M36.5 10.2C19.9 9.6 6 20.7 5.5 34.7l13.9 300.4c-.4 14 12.8 26 29.4 26.5l252.8.8c16.6.5 25.3-10.5 25.7-24.6l13.3-305.2c.4-14-12.8-26-29.4-26.5L36.5 10.2z\"\n            [attr.fill]=\"currentPlayer.color || '#a7a9ac'\"></path>\n      <path class=\"st11\"\n            d=\"M57.9 352.5l243.1.7s16.2-9.8 16.6-23.1l1.2-26.5H29.3l1.2 23.9c-.4 13.3 11.9 24.5 27.4 25z\"></path>\n      <path d=\"M302.6 17.8L46.4 21.5c-15.5-.5-28.5 9.9-28.9 23.2l11.3 253h290.5L330 42.8c.4-13.3-11.9-24.5-27.4-25z\"\n            fill=\"#fff\"></path>\n    </g>\n    <g *ngIf=\"canRemove\" id=\"delete-button\" (click)=\"removeCurrentRobot()\">\n      <path class=\"st11\"\n            d=\"M298.9 21.2l-35.8.2c-4.2 5.5-6.6 12.1-6.6 19.1 0 19.7 18.8 35.6 42.1 35.6 10.4 0 18.8-3.2 26.2-8.4l1.5-23.3c.3-12.2-12-22.7-27.4-23.2z\"></path>\n      <path class=\"st1\"\n            d=\"M282.4 34.5c1.9-1.9 5.1-1.9 7.1 0l19.2 19.2c1.9 1.9 1.9 5.1 0 7.1-1.9 1.9-5.1 1.9-7.1 0l-19.2-19.2c-2-2-2-5.2 0-7.1z\"></path>\n      <path class=\"st1\"\n            d=\"M282.4 60.7c-1.9-1.9-1.9-5.1 0-7.1l19.2-19.2c1.9-1.9 5.1-1.9 7.1 0 1.9 1.9 1.9 5.1 0 7.1l-19.2 19.2c-2 2-5.2 2-7.1 0z\"></path>\n    </g>\n <!--   <text transform=\"matrix(.95 0 0 1 44.262 336.854)\" class=\"st5\" font-size=\"29.317\" font-family=\"DIN-Medium\">Ingresá\n      tu nombre\n    </text>-->\n\n    <foreignObject height=\"40\" width=\"200\" x=\"74\" y=\"310\">\n    <xhtml:div>\n      <input (click)=\"onClickInputName()\" maxlength=\"10\" style=\"text-align: center; border: 0px; border-radius: 5px; height: 30px; font-size: 20px\" #nameInputElement  class=\"full-width\" type=\"text\" placeholder=\"Nombre\" />\n    </xhtml:div>\n  </foreignObject>\n    <foreignObject height=\"30\" width=\"165\" x=\"95\" y=\"25\">\n    <xhtml:div>\n      <span class=\"select-avatar-title\">ELEGIR AVATAR</span>\n    </xhtml:div>\n  </foreignObject>\n    <g id=\"robots\" class=\"selectable-robot\">\n      <svg x=\"0\" y=\"0\" viewBox=\"0 0 99.2 141.7\" fill=\"#E6E7E8\">\n        <polygon  points=\"49.9,13.9 53.5,37.1 64.1,16.1 60.4,39.4 76.9,22.6 66.2,43.6 87.1,32.8 70.4,49.4 93.7,45.6\n\t\t72.6,56.3 95.9,59.9 72.6,63.5 93.7,74.1 70.4,70.3 87.1,86.9 66.2,76.1 76.9,97.1 60.4,80.4 64.1,103.6 53.5,82.6 49.9,105.9\n\t\t46.3,82.6 35.7,103.6 39.5,80.4 22.9,97.1 33.6,76.1 12.7,86.9 29.4,70.3 6.2,74.1 27.2,63.5 3.9,59.9 27.2,56.3 6.2,45.6\n\t\t29.4,49.4 12.7,32.8 33.6,43.6 22.9,22.6 39.5,39.4 35.7,16.1 46.3,37.1 \t\"></polygon>\n        <ellipse *ngIf=\"currentPlayer.robot.id === 1\" cx=\"50.4\" cy=\"135.4\" rx=\"28.7\" ry=\"4.3\"></ellipse>\n        <ellipse *ngIf=\"currentPlayer.robot.id === 2\" cx=\"55.4\" cy=\"135.4\" rx=\"23.4\" ry=\"4.3\"></ellipse>\n        <ellipse *ngIf=\"currentPlayer.robot.id === 3\" cx=\"49.6\" cy=\"135.4\" rx=\"29.9\" ry=\"4.3\"></ellipse>\n        <ellipse *ngIf=\"currentPlayer.robot.id === 4\" cx=\"48.9\" cy=\"135.4\" rx=\"26.4\" ry=\"4.3\"></ellipse>\n        <ellipse *ngIf=\"currentPlayer.robot.id === 5\" cx=\"49.6\" cy=\"135.4\" rx=\"41.2\" ry=\"4.3\"></ellipse>\n\n      </svg>\n      <use  [attr.xlink:href]=\"'assets/images/robots/robots_level' + game.level + '.svg#robot' + (currentPlayer.robot.id || allUnusedRobots[0].id)\">\n\n      </use>\n     </g>\n</svg>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/select-players/select-players-view/select-players-view.component.html":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/select-players/select-players-view/select-players-view.component.html ***!
  \*****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div style=\"position: relative\" fxFlex=\"grow\" fxLayout=\"column\"  fxLayoutAlign=\"space-between center\" [ngClass]=\"'background-level-' + game.level\"\n     xmlns:xhtml=\"http://www.w3.org/1999/xhtml\">\n  <app-settings-button style=\"position: absolute; top: 0; left: 0\"></app-settings-button>\n  <div *ngIf=\"showInputName\" fxFlex=\"grow\" class=\"full-width\" fxLayout=\"row\" fxLayoutAlign=\"center center\" fxLayoutGap=\"1%\">\n    <input #mainInputName (keydown.enter)=\"showInputName = false\" [(ngModel)]=\"players[currentPlayerIndex].name\"  class=\"inputName\" placeholder=\"Nombre\" />\n    <app-check-button (click)=\"showInputName = !players[currentPlayerIndex].name\" [class.grayscale]=\"!players[currentPlayerIndex].name\"></app-check-button>\n  </div>\n  <div *ngIf=\"!showInputName\" fxFlex=\"30\" class=\"full-width\" style=\"padding: 1%\" fxLayout=\"column\" fxLayoutAlign=\"center center\" fxLayoutGap=\"1%\">\n    <h2 class=\"select-players-title\">De 2 a 4 jugadores</h2>\n    <div fxFlex=\"grow\" class=\"full-width\" fxlayout=\"row\" fxLayoutAlign=\"center center\" fxLayoutGap=\"1%\">\n      <app-card-player  *ngFor=\"let player of players, let i = index\"\n                       [data]=\"player\" (click)=\"changeCurrentPlayer(i)\"></app-card-player>\n\n    <!--  <app-card-player *ngIf=\"players.length < 3\"\n                       [state]=\"'add'\"\n                       (click)=\"onClickAddPlayer() \"></app-card-player>\n      <app-card-player *ngIf=\"players.length < 4\"\n                       [state]=\"'add'\"\n                       (click)=\"onClickAddPlayer() \"></app-card-player>-->\n\n    </div>\n  </div>\n  <app-main-monitor (selectRobot)=\"onSelectRobot()\" *ngIf=\"!showInputName\" (clickInputName)=\"showInputName = true\" (changeName)=\"onChangeName($event)\" [canRemove]=\"canRemovePlayer\" (onRemoveRobot)=\"onRemoveCurrentRobot()\" style=\"z-index: 2\" class=\"full-width\" [allUnusedRobots]=\"allUnusedRobots\" [currentPlayer]=\"players[currentPlayerIndex]\" fxFlex=\"60\"></app-main-monitor>\n  <app-back-button style=\"position: absolute; bottom: 0; left: 0; z-index: 3;\" [backUrl]=\"'/selectLevel'\"></app-back-button>\n\n  <app-tic-button *ngIf=\"!showInputName\" style=\"position: absolute; bottom: 1%; z-index: 2; right: 10%\"  [class.grayscale]=\"!allowedPlayButton\" (click)=\"goToGame()\" fxFlexOffset=\"60\"></app-tic-button>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/select-players/tic-button/tic-button.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/select-players/tic-button/tic-button.component.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<svg id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 106 132.1\" style=\"height: 10vmax\">\n  <text transform=\"translate(9.582 120.3704)\" class=\"st0\" font-size=\"29.2276\">¡Jugar!</text>\n  <circle cx=\"52.9\" cy=\"53.9\" r=\"43.4\" opacity=\".24\" fill=\"#a7a9ac\"></circle>\n  <circle cx=\"52.9\" cy=\"50.4\" r=\"43.4\" fill=\"#bcbec0\"></circle>\n  <circle cx=\"52.9\" cy=\"50.4\" r=\"36.8\" fill=\"#f7941d\"></circle>\n  <path class=\"st0\" d=\"M37 27.4l40 21.2c2.2 1.2 2.2 3.1 0 4.3L37 74\"></path>\n</svg>\n");

/***/ }),

/***/ "./src/app/select-players/check-button/check-button.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/select-players/check-button/check-button.component.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host :hover {\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VsZWN0LXBsYXllcnMvY2hlY2stYnV0dG9uL0M6XFxVc2Vyc1xcaGFkYXNcXERlc2t0b3BcXG1lbnRlX2FkaXZpbmEvc3JjXFxhcHBcXHNlbGVjdC1wbGF5ZXJzXFxjaGVjay1idXR0b25cXGNoZWNrLWJ1dHRvbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvc2VsZWN0LXBsYXllcnMvY2hlY2stYnV0dG9uL2NoZWNrLWJ1dHRvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLGVBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL3NlbGVjdC1wbGF5ZXJzL2NoZWNrLWJ1dHRvbi9jaGVjay1idXR0b24uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gIDpob3ZlciB7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICB9XG59XG4iLCI6aG9zdCA6aG92ZXIge1xuICBjdXJzb3I6IHBvaW50ZXI7XG59Il19 */");

/***/ }),

/***/ "./src/app/select-players/check-button/check-button.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/select-players/check-button/check-button.component.ts ***!
  \***********************************************************************/
/*! exports provided: CheckButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckButtonComponent", function() { return CheckButtonComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

var CheckButtonComponent = /** @class */ (function () {
    function CheckButtonComponent() {
    }
    CheckButtonComponent.prototype.ngOnInit = function () {
    };
    CheckButtonComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-check-button',
            template: __importDefault(__webpack_require__(/*! raw-loader!./check-button.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/select-players/check-button/check-button.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./check-button.component.scss */ "./src/app/select-players/check-button/check-button.component.scss")).default]
        }),
        __metadata("design:paramtypes", [])
    ], CheckButtonComponent);
    return CheckButtonComponent;
}());



/***/ }),

/***/ "./src/app/select-players/main-monitor/main-monitor.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/select-players/main-monitor/main-monitor.component.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".selectable-robot {\n  transform: scale(0.65, 0.55) translate(0px, 65px);\n}\n\n.arrow-st0 {\n  fill: #8a8c8e;\n}\n\n.arrow-st1 {\n  fill: #fff;\n}\n\n.arrow-st2 {\n  fill: #77787b;\n}\n\n.st1 {\n  fill: #b1b3b6;\n}\n\n.st3 {\n  fill: #c7c8ca;\n}\n\n.st4 {\n  fill: #848689;\n}\n\n.st5 {\n  fill: #6d6e71;\n}\n\n.st11 {\n  fill: #e6e6e6;\n}\n\n.select-avatar-title {\n  color: #6d6e71;\n  font-size: 20px;\n  font-weight: 500;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VsZWN0LXBsYXllcnMvbWFpbi1tb25pdG9yL0M6XFxVc2Vyc1xcaGFkYXNcXERlc2t0b3BcXG1lbnRlX2FkaXZpbmEvc3JjXFxhcHBcXHNlbGVjdC1wbGF5ZXJzXFxtYWluLW1vbml0b3JcXG1haW4tbW9uaXRvci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvc2VsZWN0LXBsYXllcnMvbWFpbi1tb25pdG9yL21haW4tbW9uaXRvci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvc2VsZWN0LXBsYXllcnMvbWFpbi1tb25pdG9yL0M6XFxVc2Vyc1xcaGFkYXNcXERlc2t0b3BcXG1lbnRlX2FkaXZpbmEvc3JjXFx2YXJpYWJsZXMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNFLGlEQUFBO0FDQUY7O0FER0E7RUFBVyxhQUFBO0FDQ1g7O0FERHdCO0VBQVcsVUFBQTtBQ0tuQzs7QURMNkM7RUFBVyxhQUFBO0FDU3hEOztBRFJBO0VBQUssYUFBQTtBQ1lMOztBRFhBO0VBQUssYUFBQTtBQ2VMOztBRGRBO0VBQUssYUFBQTtBQ2tCTDs7QURqQkE7RUFBSyxhQUFBO0FDcUJMOztBRHBCQTtFQUFNLGFBQUE7QUN3Qk47O0FEdkJBO0VBQ0UsY0VSVztFRlNYLGVBQUE7RUFDQSxnQkFBQTtBQzBCRiIsImZpbGUiOiJzcmMvYXBwL3NlbGVjdC1wbGF5ZXJzL21haW4tbW9uaXRvci9tYWluLW1vbml0b3IuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vLi4vLi4vdmFyaWFibGVzXCI7XG4uc2VsZWN0YWJsZS1yb2JvdCB7XG4gIHRyYW5zZm9ybTogc2NhbGUoMC42NSwgMC41NSkgdHJhbnNsYXRlKDBweCwgNjVweCk7XG59XG5cbi5hcnJvdy1zdDB7ZmlsbDojOGE4YzhlfS5hcnJvdy1zdDF7ZmlsbDojZmZmfS5hcnJvdy1zdDJ7ZmlsbDojNzc3ODdifVxuLnN0MXtmaWxsOiNiMWIzYjZ9XG4uc3Qze2ZpbGw6I2M3YzhjYX1cbi5zdDR7ZmlsbDojODQ4Njg5fVxuLnN0NXtmaWxsOiM2ZDZlNzF9XG4uc3QxMXtmaWxsOiNlNmU2ZTZ9XG4uc2VsZWN0LWF2YXRhci10aXRsZSB7XG4gIGNvbG9yOiAkZ3JheS1jb2xvcjtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBmb250LXdlaWdodDogNTAwO1xufVxuIiwiLnNlbGVjdGFibGUtcm9ib3Qge1xuICB0cmFuc2Zvcm06IHNjYWxlKDAuNjUsIDAuNTUpIHRyYW5zbGF0ZSgwcHgsIDY1cHgpO1xufVxuXG4uYXJyb3ctc3QwIHtcbiAgZmlsbDogIzhhOGM4ZTtcbn1cblxuLmFycm93LXN0MSB7XG4gIGZpbGw6ICNmZmY7XG59XG5cbi5hcnJvdy1zdDIge1xuICBmaWxsOiAjNzc3ODdiO1xufVxuXG4uc3QxIHtcbiAgZmlsbDogI2IxYjNiNjtcbn1cblxuLnN0MyB7XG4gIGZpbGw6ICNjN2M4Y2E7XG59XG5cbi5zdDQge1xuICBmaWxsOiAjODQ4Njg5O1xufVxuXG4uc3Q1IHtcbiAgZmlsbDogIzZkNmU3MTtcbn1cblxuLnN0MTEge1xuICBmaWxsOiAjZTZlNmU2O1xufVxuXG4uc2VsZWN0LWF2YXRhci10aXRsZSB7XG4gIGNvbG9yOiAjNmQ2ZTcxO1xuICBmb250LXNpemU6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59IiwiJGdyZWVuLWNvbG9yOiAjQ0REQzI5ICFkZWZhdWx0O1xuJG9yYW5nZS1jb2xvcjogI0ZFQkUxMCAhZGVmYXVsdDtcbiRibGFjay1jb2xvcjogYmxhY2sgIWRlZmF1bHQ7XG4kdmlvbGV0LWNvbG9yOiAjOTIyNzhGICFkZWZhdWx0O1xuJGdyYXktY29sb3I6ICM2ZDZlNzEgIWRlZmF1bHQ7XG5cbiRncmVlbi1jYXRlZ29yeTogIzAwQjNBQTtcbiRibHVlLWNhdGVnb3J5OiAjMDBBRUVGO1xuJGZ1Y2hzaWEtY2F0ZWdvcnk6ICNFQzAwOEM7XG4kb3JhbmdlLWNhdGVnb3J5OiAjRkFBNjFBO1xuXG4kcmVkLXNlbGVjdC1wbGF5ZXI6ICNlZDE5NDE7XG4kbGlnaHQtYmx1ZS1zZWxlY3QtcGxheWVyOiAjMDBhZWVmO1xuJHllbGxvdy1zZWxlY3QtcGxheWVyOiAjZmZlNjAwO1xuJGdyZWVuLXNlbGVjdC1wbGF5ZXI6ICMwZGIxNGI7XG4iXX0= */");

/***/ }),

/***/ "./src/app/select-players/main-monitor/main-monitor.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/select-players/main-monitor/main-monitor.component.ts ***!
  \***********************************************************************/
/*! exports provided: MainMonitorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainMonitorComponent", function() { return MainMonitorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_models_player__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/models/player */ "./src/app/shared/models/player.ts");
/* harmony import */ var _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/audio.service */ "./src/app/shared/services/audio.service.ts");
/* harmony import */ var _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/game.service */ "./src/app/shared/services/game.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




var MainMonitorComponent = /** @class */ (function () {
    function MainMonitorComponent(audioService, game) {
        this.audioService = audioService;
        this.game = game;
        this.changeName = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.clickInputName = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.selectRobot = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onRemoveRobot = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    Object.defineProperty(MainMonitorComponent.prototype, "currentPlayer", {
        get: function () {
            return this._currentPlayer;
        },
        set: function (value) {
            this._currentPlayer = value;
            this.updateNameInputValue();
        },
        enumerable: true,
        configurable: true
    });
    MainMonitorComponent.prototype.ngOnInit = function () {
    };
    MainMonitorComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.input.nativeElement.onkeyup = function (ev) {
            _this._currentPlayer.name = _this.input.nativeElement.value;
            _this.changeName.emit(_this._currentPlayer.name);
        };
        this.updateNameInputValue();
    };
    MainMonitorComponent.prototype.updateNameInputValue = function () {
        this.input.nativeElement.value = this._currentPlayer ? this._currentPlayer.name : '';
    };
    MainMonitorComponent.prototype.nextRobot = function () {
        if (this.currentPlayer.robot.id) {
            this.allUnusedRobots.push(this.currentPlayer.robot);
            this.currentPlayer.robot = this.allUnusedRobots.splice(0, 1)[0];
        }
        else {
            this.currentPlayer.robot = this.allUnusedRobots.splice(1, 1)[0];
        }
        this.selectRobot.emit();
        this.audioService.playClick();
        /*if (this.players[this.currentPlayerIndex].robotId > 5) {
            this.players[this.currentPlayerIndex].robotId = 0;
        } else {
            this.players[this.currentPlayerIndex].robotId++;
        }*/
    };
    MainMonitorComponent.prototype.prevRobot = function () {
        if (this.currentPlayer.robot.id) {
            this.allUnusedRobots = [this.currentPlayer.robot].concat(this.allUnusedRobots);
        }
        this.currentPlayer.robot = this.allUnusedRobots.splice(this.allUnusedRobots.length - 1, 1)[0];
        this.selectRobot.emit();
        this.audioService.playClick();
        /*if (this.players[this.currentPlayerIndex].robotId === 0) {
            this.players[this.currentPlayerIndex].robotId = 6;
        } else {
            this.players[this.currentPlayerIndex].robotId--;
        }*/
    };
    MainMonitorComponent.prototype.removeCurrentRobot = function () {
        this.audioService.playClick();
        this.onRemoveRobot.next(true);
    };
    MainMonitorComponent.prototype.onClickInputName = function () {
        if (!this.currentPlayer.robot.id) {
            this.currentPlayer.robot = this.allUnusedRobots.splice(0, 1)[0];
        }
        this.clickInputName.emit();
    };
    MainMonitorComponent.ctorParameters = function () { return [
        { type: _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_2__["AudioService"] },
        { type: _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('nameInputElement', { static: true }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], MainMonitorComponent.prototype, "input", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], MainMonitorComponent.prototype, "allUnusedRobots", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], MainMonitorComponent.prototype, "canRemove", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _shared_models_player__WEBPACK_IMPORTED_MODULE_1__["Player"]),
        __metadata("design:paramtypes", [_shared_models_player__WEBPACK_IMPORTED_MODULE_1__["Player"]])
    ], MainMonitorComponent.prototype, "currentPlayer", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], MainMonitorComponent.prototype, "onRemoveRobot", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], MainMonitorComponent.prototype, "changeName", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], MainMonitorComponent.prototype, "clickInputName", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], MainMonitorComponent.prototype, "selectRobot", void 0);
    MainMonitorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-main-monitor',
            template: __importDefault(__webpack_require__(/*! raw-loader!./main-monitor.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/select-players/main-monitor/main-monitor.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./main-monitor.component.scss */ "./src/app/select-players/main-monitor/main-monitor.component.scss")).default]
        }),
        __metadata("design:paramtypes", [_shared_services_audio_service__WEBPACK_IMPORTED_MODULE_2__["AudioService"], _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"]])
    ], MainMonitorComponent);
    return MainMonitorComponent;
}());



/***/ }),

/***/ "./src/app/select-players/select-players-view/select-players-view.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/select-players/select-players-view/select-players-view.component.scss ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".select-players-title {\n  color: white;\n}\n.select-players-title {\n  font-size: 14px;\n}\n@media screen and (min-width: 500px) {\n  .select-players-title {\n    font-size: calc(14px + 18 * ((100vw - 500px) / 1300));\n  }\n}\n@media screen and (min-width: 1800px) {\n  .select-players-title {\n    font-size: 32px;\n  }\n}\napp-card-player {\n  width: 12%;\n  max-width: 200px;\n  min-width: 80px;\n}\n.grayscale {\n  -webkit-filter: grayscale(1);\n          filter: grayscale(1);\n}\n.inputName {\n  background-color: rgba(0, 0, 0, 0);\n  border: none;\n  text-align: center;\n  color: white;\n  font-size: 30px;\n  width: 200px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VsZWN0LXBsYXllcnMvc2VsZWN0LXBsYXllcnMtdmlldy9DOlxcVXNlcnNcXGhhZGFzXFxEZXNrdG9wXFxtZW50ZV9hZGl2aW5hL3NyY1xcYXBwXFxzZWxlY3QtcGxheWVyc1xcc2VsZWN0LXBsYXllcnMtdmlld1xcc2VsZWN0LXBsYXllcnMtdmlldy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvc2VsZWN0LXBsYXllcnMvc2VsZWN0LXBsYXllcnMtdmlldy9zZWxlY3QtcGxheWVycy12aWV3LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zZWxlY3QtcGxheWVycy9zZWxlY3QtcGxheWVycy12aWV3L0M6XFxVc2Vyc1xcaGFkYXNcXERlc2t0b3BcXG1lbnRlX2FkaXZpbmEvc3JjXFxmbHVpZC10eXBvZ3JhcGh5LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFFRSxZQUFBO0FDREY7QUNTSTtFQUNFLGVGVitCO0FDR3JDO0FDUU07RUFGRjtJQUdJLHFEQUFBO0VETE47QUFDRjtBQ01NO0VBTEY7SUFNSSxlRmZtQztFQ1l6QztBQUNGO0FEVkE7RUFDRSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDYUY7QURYQTtFQUNFLDRCQUFBO1VBQUEsb0JBQUE7QUNjRjtBRFpBO0VBQ0Usa0NBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUNlRiIsImZpbGUiOiJzcmMvYXBwL3NlbGVjdC1wbGF5ZXJzL3NlbGVjdC1wbGF5ZXJzLXZpZXcvc2VsZWN0LXBsYXllcnMtdmlldy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi8uLi9mbHVpZC10eXBvZ3JhcGh5XCI7XG4uc2VsZWN0LXBsYXllcnMtdGl0bGUge1xuICBAaW5jbHVkZSBmbHVpZC10eXBlKDUwMHB4LCAxODAwcHgsIDE0cHgsIDMycHgpO1xuICBjb2xvcjogd2hpdGU7XG59XG5hcHAtY2FyZC1wbGF5ZXIge1xuICB3aWR0aDogMTIlO1xuICBtYXgtd2lkdGg6IDIwMHB4O1xuICBtaW4td2lkdGg6IDgwcHg7XG59XG4uZ3JheXNjYWxlIHtcbiAgZmlsdGVyOiBncmF5c2NhbGUoMSk7XG59XG4uaW5wdXROYW1lIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwKTtcbiAgYm9yZGVyOiBub25lO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAzMHB4O1xuICB3aWR0aDogMjAwcHg7XG59XG4iLCIuc2VsZWN0LXBsYXllcnMtdGl0bGUge1xuICBjb2xvcjogd2hpdGU7XG59XG4uc2VsZWN0LXBsYXllcnMtdGl0bGUge1xuICBmb250LXNpemU6IDE0cHg7XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA1MDBweCkge1xuICAuc2VsZWN0LXBsYXllcnMtdGl0bGUge1xuICAgIGZvbnQtc2l6ZTogY2FsYygxNHB4ICsgMTggKiAoKDEwMHZ3IC0gNTAwcHgpIC8gMTMwMCkpO1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxODAwcHgpIHtcbiAgLnNlbGVjdC1wbGF5ZXJzLXRpdGxlIHtcbiAgICBmb250LXNpemU6IDMycHg7XG4gIH1cbn1cblxuYXBwLWNhcmQtcGxheWVyIHtcbiAgd2lkdGg6IDEyJTtcbiAgbWF4LXdpZHRoOiAyMDBweDtcbiAgbWluLXdpZHRoOiA4MHB4O1xufVxuXG4uZ3JheXNjYWxlIHtcbiAgZmlsdGVyOiBncmF5c2NhbGUoMSk7XG59XG5cbi5pbnB1dE5hbWUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDApO1xuICBib3JkZXI6IG5vbmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDMwcHg7XG4gIHdpZHRoOiAyMDBweDtcbn0iLCJAZnVuY3Rpb24gc3RyaXAtdW5pdCgkdmFsdWUpIHtcbiAgQHJldHVybiAkdmFsdWUgLyAoJHZhbHVlICogMCArIDEpO1xufVxuXG5AbWl4aW4gZmx1aWQtdHlwZSgkbWluLXZ3LCAkbWF4LXZ3LCAkbWluLWZvbnQtc2l6ZSwgJG1heC1mb250LXNpemUpIHtcbiAgJHUxOiB1bml0KCRtaW4tdncpO1xuICAkdTI6IHVuaXQoJG1heC12dyk7XG4gICR1MzogdW5pdCgkbWluLWZvbnQtc2l6ZSk7XG4gICR1NDogdW5pdCgkbWF4LWZvbnQtc2l6ZSk7XG5cbiAgQGlmICR1MSA9PSAkdTIgYW5kICR1MSA9PSAkdTMgYW5kICR1MSA9PSAkdTQge1xuICAgICYge1xuICAgICAgZm9udC1zaXplOiAkbWluLWZvbnQtc2l6ZTtcbiAgICAgIEBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6ICRtaW4tdncpIHtcbiAgICAgICAgZm9udC1zaXplOiBjYWxjKCN7JG1pbi1mb250LXNpemV9ICsgI3tzdHJpcC11bml0KCRtYXgtZm9udC1zaXplIC0gJG1pbi1mb250LXNpemUpfSAqICgoMTAwdncgLSAjeyRtaW4tdnd9KSAvICN7c3RyaXAtdW5pdCgkbWF4LXZ3IC0gJG1pbi12dyl9KSk7XG4gICAgICB9XG4gICAgICBAbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAkbWF4LXZ3KSB7XG4gICAgICAgIGZvbnQtc2l6ZTogJG1heC1mb250LXNpemU7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iXX0= */");

/***/ }),

/***/ "./src/app/select-players/select-players-view/select-players-view.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/select-players/select-players-view/select-players-view.component.ts ***!
  \*************************************************************************************/
/*! exports provided: SelectPlayersViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectPlayersViewComponent", function() { return SelectPlayersViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_robot_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/robot.service */ "./src/app/shared/services/robot.service.ts");
/* harmony import */ var _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/game.service */ "./src/app/shared/services/game.service.ts");
/* harmony import */ var _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/services/audio.service */ "./src/app/shared/services/audio.service.ts");
/* harmony import */ var _shared_components_card_player_card_player_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/components/card-player/card-player.component */ "./src/app/shared/components/card-player/card-player.component.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _main_monitor_main_monitor_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../main-monitor/main-monitor.component */ "./src/app/select-players/main-monitor/main-monitor.component.ts");
/* harmony import */ var animejs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! animejs */ "./node_modules/animejs/lib/anime.es.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};











var SelectPlayersViewComponent = /** @class */ (function () {
    function SelectPlayersViewComponent(router, robotService, game, audioService) {
        this.router = router;
        this.robotService = robotService;
        this.game = game;
        this.audioService = audioService;
        this.colors = ['#ed1941', '#00aeef', '#ffe600', '#0db14b'];
    }
    Object.defineProperty(SelectPlayersViewComponent.prototype, "showInputName", {
        get: function () {
            return this._showInputName;
        },
        set: function (value) {
            var _this = this;
            this._showInputName = value;
            if (!value && this.players.filter(function (e) { return e.name.length > 0; }).length < this.players.length) {
                do {
                    this.currentPlayerIndex++;
                    if (this.currentPlayerIndex >= this.players.length) {
                        this.currentPlayerIndex = 0;
                    }
                } while (this.players[this.currentPlayerIndex].name.length > 0);
            }
            else {
                Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["timer"])(50).subscribe(function () {
                    if (!_this.mainInputName) {
                        Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["timer"])(100).subscribe(function () {
                            _this.mainInputName.nativeElement.focus();
                            _capacitor_core__WEBPACK_IMPORTED_MODULE_8__["Plugins"].StatusBar.hide();
                        });
                    }
                    else {
                        _this.mainInputName.nativeElement.focus();
                        _capacitor_core__WEBPACK_IMPORTED_MODULE_8__["Plugins"].StatusBar.hide();
                    }
                });
            }
            this.updatePlayButton();
            _capacitor_core__WEBPACK_IMPORTED_MODULE_8__["Plugins"].StatusBar.hide();
        },
        enumerable: true,
        configurable: true
    });
    SelectPlayersViewComponent.prototype.ngOnInit = function () {
        this.robotService.generateRobots();
        this.allUnusedRobots = this.robotService.robotsByLevel.get(this.game.level);
        this.players = [];
        this.addPlayer();
        this.addPlayer();
        this.addPlayer();
        this.addPlayer();
        this.currentPlayerIndex = 0;
        this.allowedPlayButton = false;
    };
    SelectPlayersViewComponent.prototype.changeCurrentPlayer = function (index) {
        if (!this.isValidPlayer(this.players[this.currentPlayerIndex])) {
            this.wrongNameAnimation();
        }
        else {
            this.currentPlayerIndex = index;
            this.audioService.playClick();
            this.updatePlayButton();
        }
    };
    SelectPlayersViewComponent.prototype.goToGame = function () {
        var _this = this;
        if (this.allowedPlayButton) {
            this.audioService.playClick();
            var validPlayers = this.players.filter(function (e) { return _this.isValidPlayer(e); });
            this.game.players = validPlayers;
        }
        else {
            // todo play cant click?
        }
        _capacitor_core__WEBPACK_IMPORTED_MODULE_8__["Plugins"].StatusBar.hide();
    };
    SelectPlayersViewComponent.prototype.isValidPlayer = function (player) {
        return player.name.length > 0;
    };
    SelectPlayersViewComponent.prototype.addPlayer = function () {
        this.players.push({
            id: this.players.length + 1,
            robot: { id: undefined, path: '' }, name: '', color: this.colors[this.players.length]
        });
    };
    SelectPlayersViewComponent.prototype.onClickAddPlayer = function () {
        var _this = this;
        this.addPlayer();
        this.audioService.playClick();
        // workaround porque no cargaba la imagen del robot en el main monitor la primera vez
        this.subscription = Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["timer"])(50).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["take"])(1)).subscribe(function (value) {
            _this.currentPlayerIndex = _this.players.length - 1;
            _this.subscription.unsubscribe();
        });
    };
    SelectPlayersViewComponent.prototype.onRemoveCurrentRobot = function () {
        var currentRobot = this.players[this.currentPlayerIndex].robot;
        if (currentRobot.id && this.allUnusedRobots.every(function (e) { return e.id !== currentRobot.id; })) {
            this.allUnusedRobots = [this.players[this.currentPlayerIndex].robot].concat(this.allUnusedRobots);
        }
        this.players[this.currentPlayerIndex].robot = { id: undefined, path: '' };
        this.players[this.currentPlayerIndex].name = '';
        this.currentPlayerIndex = this.players.findIndex(function (e) { return e.robot.id > 0; });
        this.updatePlayButton();
    };
    SelectPlayersViewComponent.prototype.ngOnDestroy = function () {
    };
    SelectPlayersViewComponent.prototype.updatePlayButton = function () {
        var validPlayers = this.players.filter(function (e) { return e.name.length > 0; });
        var invalidPlayers = this.players.filter(function (e) { return e.robot.id && e.name.length === 0; });
        this.canRemovePlayer = validPlayers.length > 2 || (validPlayers.length === 2 && !this.players[this.currentPlayerIndex].robot.id);
        this.allowedPlayButton = validPlayers.length >= 2 && invalidPlayers.length === 0;
    };
    SelectPlayersViewComponent.prototype.onSelectRobot = function () {
        this.updatePlayButton();
    };
    SelectPlayersViewComponent.prototype.onChangeName = function (currentName) {
        if (currentName.length > 0) {
            this.updatePlayButton();
        }
        else {
            this.allowedPlayButton = false;
        }
    };
    SelectPlayersViewComponent.prototype.wrongNameAnimation = function () {
        var _this = this;
        this.audioService.playSfx('cantContinue.mp3');
        var backgroundColor = 'rgba(255,25,206,0.62)';
        Object(animejs__WEBPACK_IMPORTED_MODULE_10__["default"])({
            targets: this.mainMonitor.input.nativeElement,
            direction: 'alternate',
            keyframes: [
                { translateX: 0, translateY: 0, rotate: '0deg', 'backgroundColor': backgroundColor },
                { translateX: -1, translateY: 0, rotate: '0.5deg', 'backgroundColor': backgroundColor },
                { translateX: 2, translateY: 1, rotate: '-0.5deg', 'backgroundColor': backgroundColor },
                { translateX: -3, translateY: 0, rotate: '0.5deg', 'backgroundColor': backgroundColor },
                { translateX: 3, translateY: -1, rotate: '-0.5deg', 'backgroundColor': backgroundColor },
            ],
            complete: function (anim) { _this.mainMonitor.input.nativeElement.style.backgroundColor = 'white'; },
            duration: 300,
            loop: 2
        });
    };
    SelectPlayersViewComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _shared_services_robot_service__WEBPACK_IMPORTED_MODULE_2__["RobotService"] },
        { type: _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"] },
        { type: _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_4__["AudioService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChildren"])(_shared_components_card_player_card_player_component__WEBPACK_IMPORTED_MODULE_5__["CardPlayerComponent"]),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["QueryList"])
    ], SelectPlayersViewComponent.prototype, "cardPlayers", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_main_monitor_main_monitor_component__WEBPACK_IMPORTED_MODULE_9__["MainMonitorComponent"], { static: false }),
        __metadata("design:type", _main_monitor_main_monitor_component__WEBPACK_IMPORTED_MODULE_9__["MainMonitorComponent"])
    ], SelectPlayersViewComponent.prototype, "mainMonitor", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('mainInputName', { static: false }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], SelectPlayersViewComponent.prototype, "mainInputName", void 0);
    SelectPlayersViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-select-players-view',
            template: __importDefault(__webpack_require__(/*! raw-loader!./select-players-view.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/select-players/select-players-view/select-players-view.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./select-players-view.component.scss */ "./src/app/select-players/select-players-view/select-players-view.component.scss")).default]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _shared_services_robot_service__WEBPACK_IMPORTED_MODULE_2__["RobotService"], _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"],
            _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_4__["AudioService"]])
    ], SelectPlayersViewComponent);
    return SelectPlayersViewComponent;
}());



/***/ }),

/***/ "./src/app/select-players/select-players.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/select-players/select-players.module.ts ***!
  \*********************************************************/
/*! exports provided: SelectPlayersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectPlayersModule", function() { return SelectPlayersModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _select_players_view_select_players_view_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./select-players-view/select-players-view.component */ "./src/app/select-players/select-players-view/select-players-view.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _main_monitor_main_monitor_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./main-monitor/main-monitor.component */ "./src/app/select-players/main-monitor/main-monitor.component.ts");
/* harmony import */ var _check_button_check_button_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./check-button/check-button.component */ "./src/app/select-players/check-button/check-button.component.ts");
/* harmony import */ var _tic_button_tic_button_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tic-button/tic-button.component */ "./src/app/select-players/tic-button/tic-button.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};








var SelectPlayersModule = /** @class */ (function () {
    function SelectPlayersModule() {
    }
    SelectPlayersModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_select_players_view_select_players_view_component__WEBPACK_IMPORTED_MODULE_1__["SelectPlayersViewComponent"], _main_monitor_main_monitor_component__WEBPACK_IMPORTED_MODULE_4__["MainMonitorComponent"], _check_button_check_button_component__WEBPACK_IMPORTED_MODULE_5__["CheckButtonComponent"], _tic_button_tic_button_component__WEBPACK_IMPORTED_MODULE_6__["TicButtonComponent"]],
            imports: [
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _select_players_view_select_players_view_component__WEBPACK_IMPORTED_MODULE_1__["SelectPlayersViewComponent"]
                    }
                ]),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"]
            ]
        })
    ], SelectPlayersModule);
    return SelectPlayersModule;
}());



/***/ }),

/***/ "./src/app/select-players/tic-button/tic-button.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/select-players/tic-button/tic-button.component.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host :hover {\n  cursor: pointer;\n}\n\n.st0 {\n  fill: #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VsZWN0LXBsYXllcnMvdGljLWJ1dHRvbi9DOlxcVXNlcnNcXGhhZGFzXFxEZXNrdG9wXFxtZW50ZV9hZGl2aW5hL3NyY1xcYXBwXFxzZWxlY3QtcGxheWVyc1xcdGljLWJ1dHRvblxcdGljLWJ1dHRvbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvc2VsZWN0LXBsYXllcnMvdGljLWJ1dHRvbi90aWMtYnV0dG9uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsZUFBQTtBQ0FKOztBREdBO0VBQ0UsVUFBQTtBQ0FGIiwiZmlsZSI6InNyYy9hcHAvc2VsZWN0LXBsYXllcnMvdGljLWJ1dHRvbi90aWMtYnV0dG9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuICA6aG92ZXIge1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgfVxufVxuLnN0MCB7XG4gIGZpbGw6ICNmZmZcbn1cbiIsIjpob3N0IDpob3ZlciB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLnN0MCB7XG4gIGZpbGw6ICNmZmY7XG59Il19 */");

/***/ }),

/***/ "./src/app/select-players/tic-button/tic-button.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/select-players/tic-button/tic-button.component.ts ***!
  \*******************************************************************/
/*! exports provided: TicButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicButtonComponent", function() { return TicButtonComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

var TicButtonComponent = /** @class */ (function () {
    function TicButtonComponent() {
    }
    TicButtonComponent.prototype.ngOnInit = function () { };
    TicButtonComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tic-button',
            template: __importDefault(__webpack_require__(/*! raw-loader!./tic-button.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/select-players/tic-button/tic-button.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./tic-button.component.scss */ "./src/app/select-players/tic-button/tic-button.component.scss")).default]
        }),
        __metadata("design:paramtypes", [])
    ], TicButtonComponent);
    return TicButtonComponent;
}());



/***/ }),

/***/ "./src/app/shared/models/robot.ts":
/*!****************************************!*\
  !*** ./src/app/shared/models/robot.ts ***!
  \****************************************/
/*! exports provided: Robot */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Robot", function() { return Robot; });
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};
var Robot = /** @class */ (function () {
    function Robot(id, path) {
        this.id = id;
        this.path = path;
    }
    return Robot;
}());



/***/ }),

/***/ "./src/app/shared/services/robot.service.ts":
/*!**************************************************!*\
  !*** ./src/app/shared/services/robot.service.ts ***!
  \**************************************************/
/*! exports provided: RobotService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RobotService", function() { return RobotService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_robot__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/robot */ "./src/app/shared/models/robot.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};


var RobotService = /** @class */ (function () {
    function RobotService() {
    }
    RobotService.prototype.generateRobots = function () {
        this.robotsByLevel = new Map();
        this.robotsByLevel.set(1, [
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](1, 'assets/images/robots/level1/1.svg'),
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](2, 'assets/images/robots/level1/2.svg'),
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](3, 'assets/images/robots/level1/3.svg'),
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](4, 'assets/images/robots/level1/4.svg'),
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](5, 'assets/images/robots/level1/5.svg')
        ]);
        this.robotsByLevel.set(2, [
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](1, 'assets/images/robots/level2/6.svg'),
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](2, 'assets/images/robots/level2/7.svg'),
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](3, 'assets/images/robots/level2/8.svg'),
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](4, 'assets/images/robots/level2/9.svg'),
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](5, 'assets/images/robots/level2/10.svg')
        ]);
        this.robotsByLevel.set(3, [
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](1, 'assets/images/robots/level3/11.svg'),
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](2, 'assets/images/robots/level3/12.svg'),
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](3, 'assets/images/robots/level3/13.svg'),
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](4, 'assets/images/robots/level3/14.svg'),
            new _models_robot__WEBPACK_IMPORTED_MODULE_1__["Robot"](5, 'assets/images/robots/level3/15.svg')
        ]);
    };
    RobotService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], RobotService);
    return RobotService;
}());



/***/ })

}]);
//# sourceMappingURL=select-players-select-players-module.js.map