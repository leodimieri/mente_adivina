(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tutorial-tutorial-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tutorial/tutorial/tutorial.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tutorial/tutorial/tutorial.component.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<video (ended)=\"goBack()\" #videoElement [autoplay]=\"true\" [src]=\"'assets/videos/Tutorial_Comprimido.mp4'\" fxFlex=\"grow\" controls=\"seeking\"\n       controlsList=\"play nofullscreen hidefullscreen nodownload novolume\" looped preload=\"auto\"></video>\n<!--\n<ion-button  (click)=\"goBack()\">Continuar</ion-button>\n-->\n<svg (click)=\"goBack()\" style=\"position: absolute; right: 5px; bottom: 18px; width: 15%; min-width: 100px\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 163.4 77.7\">\n    <path d=\"M146 8.2c4.5-.3 6.4 2.2 6.7 5.7l-.9 40c.3 3.5-1.3 6.6-5.7 6.9L22.4 66.2c-4.5.3-8.3-2.2-8.6-5.7L7.9 18.8c-.3-3.5 3.2-6.6 7.6-6.9L146 8.2z\"\n          fill=\"#741472\"/>\n    <path d=\"M14.5 10c-4.3-.3-6.2 2.2-6.5 5.5l.9 38.8c-.3 3.4 1.3 6.4 5.6 6.7l122.4 5.3c4.3.3 8.1-2.2 8.3-5.5l5.7-40.5c.3-3.4-3.1-6.4-7.4-6.7L14.5 10z\"\n          fill=\"#0092c8\"/>\n    <text transform=\"translate(15.803 43.905)\" font-size=\"18\" font-family=\"FiraSans-Medium\" fill=\"#fff\">SALTEAR VIDEO\n    </text>\n</svg>\n");

/***/ }),

/***/ "./src/app/tutorial/tutorial.module.ts":
/*!*********************************************!*\
  !*** ./src/app/tutorial/tutorial.module.ts ***!
  \*********************************************/
/*! exports provided: TutorialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TutorialModule", function() { return TutorialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _tutorial_tutorial_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tutorial/tutorial.component */ "./src/app/tutorial/tutorial/tutorial.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




var TutorialModule = /** @class */ (function () {
    function TutorialModule() {
    }
    TutorialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_tutorial_tutorial_component__WEBPACK_IMPORTED_MODULE_3__["TutorialComponent"]],
            imports: [
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tutorial_tutorial_component__WEBPACK_IMPORTED_MODULE_3__["TutorialComponent"] }])
            ]
        })
    ], TutorialModule);
    return TutorialModule;
}());



/***/ }),

/***/ "./src/app/tutorial/tutorial/tutorial.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/tutorial/tutorial/tutorial.component.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  background-color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdHV0b3JpYWwvdHV0b3JpYWwvQzpcXFVzZXJzXFxoYWRhc1xcRGVza3RvcFxcbWVudGVfYWRpdmluYS9zcmNcXGFwcFxcdHV0b3JpYWxcXHR1dG9yaWFsXFx0dXRvcmlhbC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvdHV0b3JpYWwvdHV0b3JpYWwvdHV0b3JpYWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx1QkFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvdHV0b3JpYWwvdHV0b3JpYWwvdHV0b3JpYWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xufVxuIiwiOmhvc3Qge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbn0iXX0= */");

/***/ }),

/***/ "./src/app/tutorial/tutorial/tutorial.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/tutorial/tutorial/tutorial.component.ts ***!
  \*********************************************************/
/*! exports provided: TutorialComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TutorialComponent", function() { return TutorialComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/audio.service */ "./src/app/shared/services/audio.service.ts");
/* harmony import */ var _settings_services_settings_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../settings/services/settings.service */ "./src/app/settings/services/settings.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




var TutorialComponent = /** @class */ (function () {
    function TutorialComponent(_location, audio, settings) {
        this._location = _location;
        this.audio = audio;
        this.settings = settings;
    }
    TutorialComponent.prototype.ngOnInit = function () {
        if (this.settings.onChangeMusic.getValue()) {
            this.settings.toggleMusic();
            this.previousMusicEnabled = true;
        }
    };
    TutorialComponent.prototype.goBack = function () {
        this.video.nativeElement.pause();
        if (this.previousMusicEnabled) {
            this.settings.toggleMusic();
        }
        this._location.back();
    };
    TutorialComponent.ctorParameters = function () { return [
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"] },
        { type: _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_2__["AudioService"] },
        { type: _settings_services_settings_service__WEBPACK_IMPORTED_MODULE_3__["SettingsService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('videoElement', { static: false }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], TutorialComponent.prototype, "video", void 0);
    TutorialComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tutorial',
            template: __importDefault(__webpack_require__(/*! raw-loader!./tutorial.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tutorial/tutorial/tutorial.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./tutorial.component.scss */ "./src/app/tutorial/tutorial/tutorial.component.scss")).default]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"], _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_2__["AudioService"],
            _settings_services_settings_service__WEBPACK_IMPORTED_MODULE_3__["SettingsService"]])
    ], TutorialComponent);
    return TutorialComponent;
}());



/***/ })

}]);
//# sourceMappingURL=tutorial-tutorial-module.js.map