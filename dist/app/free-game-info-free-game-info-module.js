(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["free-game-info-free-game-info-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/free-game-info/free-game-info-view/free-game-info-view.component.html":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/free-game-info/free-game-info-view/free-game-info-view.component.html ***!
  \*****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div fxFlex=\"grow\" fxLayout=\"column\" class=\"violet-background\">\n    <app-monitor-container [logo]=\"'categories'\" fxFlex=\"85\">\n        <div fxFill fxLayout=\"column\" fxLayoutAlign=\"start center\">\n            <div fxLayout=\"column\" fxFlexOffset=\"5\" fxLayoutAlign=\"center center\" fxLayoutGap=\"10px\">\n                <div class=\"violet-text\">\n                    MODALIDAD LIBRE\n                </div>\n                <div class=\"gray-text\">\n                    <div>Elijan los desafíos o usen el modo aleatorio.</div>\n                    <div>¡Se puede adaptar al uso que quieran!</div>\n                </div>\n            </div>\n            <div style=\"width: 15%\" fxFlexOffset=\"10\" (click)=\"onClickOk()\">\n                <svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"100 100 83.5 83.5\">\n                    <circle cx=\"141.8\" cy=\"142.9\" r=\"37.5\" opacity=\".24\" fill=\"#a7a9ac\"></circle>\n                    <circle cx=\"141.8\" cy=\"139.9\" r=\"37.5\" fill=\"#bcbec0\"></circle>\n                    <circle cx=\"141.8\" cy=\"139.9\" r=\"31.8\" fill=\"#f7941d\"></circle>\n                    <path d=\"M128.1 120l34.5 18.3c1.9 1 1.9 2.7 0 3.7l-34.5 18.3\" fill=\"#fff\"></path>\n                </svg>\n            </div>\n        </div>\n    </app-monitor-container>\n    <div class=\"full-width\" fxFlex=\"8\">\n        <app-back-button [backUrl]=\"'cover'\"></app-back-button>\n    </div>\n</div>\n");

/***/ }),

/***/ "./src/app/free-game-info/free-game-info-view/free-game-info-view.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/free-game-info/free-game-info-view/free-game-info-view.component.scss ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".gray-text {\n  color: #6d6e71;\n  font-size: 26px;\n  text-align: center;\n}\n\n.violet-text {\n  color: #92278F;\n  font-size: 34px;\n  font-weight: 500;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZnJlZS1nYW1lLWluZm8vZnJlZS1nYW1lLWluZm8tdmlldy9DOlxcVXNlcnNcXGhhZGFzXFxEZXNrdG9wXFxtZW50ZV9hZGl2aW5hL3NyY1xcYXBwXFxmcmVlLWdhbWUtaW5mb1xcZnJlZS1nYW1lLWluZm8tdmlld1xcZnJlZS1nYW1lLWluZm8tdmlldy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZnJlZS1nYW1lLWluZm8vZnJlZS1nYW1lLWluZm8tdmlldy9DOlxcVXNlcnNcXGhhZGFzXFxEZXNrdG9wXFxtZW50ZV9hZGl2aW5hL3NyY1xcdmFyaWFibGVzLnNjc3MiLCJzcmMvYXBwL2ZyZWUtZ2FtZS1pbmZvL2ZyZWUtZ2FtZS1pbmZvLXZpZXcvZnJlZS1nYW1lLWluZm8tdmlldy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNFLGNDRVc7RUREWCxlQUFBO0VBQ0Esa0JBQUE7QUVBRjs7QUZFQTtFQUNFLGNDSmE7RURLYixlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBRUNGIiwiZmlsZSI6InNyYy9hcHAvZnJlZS1nYW1lLWluZm8vZnJlZS1nYW1lLWluZm8tdmlldy9mcmVlLWdhbWUtaW5mby12aWV3LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uL3ZhcmlhYmxlc1wiO1xuLmdyYXktdGV4dCB7XG4gIGNvbG9yOiAkZ3JheS1jb2xvcjtcbiAgZm9udC1zaXplOiAyNnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4udmlvbGV0LXRleHQge1xuICBjb2xvcjogJHZpb2xldC1jb2xvcjtcbiAgZm9udC1zaXplOiAzNHB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4iLCIkZ3JlZW4tY29sb3I6ICNDRERDMjkgIWRlZmF1bHQ7XG4kb3JhbmdlLWNvbG9yOiAjRkVCRTEwICFkZWZhdWx0O1xuJGJsYWNrLWNvbG9yOiBibGFjayAhZGVmYXVsdDtcbiR2aW9sZXQtY29sb3I6ICM5MjI3OEYgIWRlZmF1bHQ7XG4kZ3JheS1jb2xvcjogIzZkNmU3MSAhZGVmYXVsdDtcblxuJGdyZWVuLWNhdGVnb3J5OiAjMDBCM0FBO1xuJGJsdWUtY2F0ZWdvcnk6ICMwMEFFRUY7XG4kZnVjaHNpYS1jYXRlZ29yeTogI0VDMDA4QztcbiRvcmFuZ2UtY2F0ZWdvcnk6ICNGQUE2MUE7XG5cbiRyZWQtc2VsZWN0LXBsYXllcjogI2VkMTk0MTtcbiRsaWdodC1ibHVlLXNlbGVjdC1wbGF5ZXI6ICMwMGFlZWY7XG4keWVsbG93LXNlbGVjdC1wbGF5ZXI6ICNmZmU2MDA7XG4kZ3JlZW4tc2VsZWN0LXBsYXllcjogIzBkYjE0YjtcbiIsIi5ncmF5LXRleHQge1xuICBjb2xvcjogIzZkNmU3MTtcbiAgZm9udC1zaXplOiAyNnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi52aW9sZXQtdGV4dCB7XG4gIGNvbG9yOiAjOTIyNzhGO1xuICBmb250LXNpemU6IDM0cHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/free-game-info/free-game-info-view/free-game-info-view.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/free-game-info/free-game-info-view/free-game-info-view.component.ts ***!
  \*************************************************************************************/
/*! exports provided: FreeGameInfoViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FreeGameInfoViewComponent", function() { return FreeGameInfoViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/services/audio.service */ "./src/app/shared/services/audio.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/game.service */ "./src/app/shared/services/game.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




var FreeGameInfoViewComponent = /** @class */ (function () {
    function FreeGameInfoViewComponent(audio, router, game) {
        this.audio = audio;
        this.router = router;
        this.game = game;
    }
    FreeGameInfoViewComponent.prototype.ngOnInit = function () {
    };
    FreeGameInfoViewComponent.prototype.onClickOk = function () {
        this.audio.playClick();
        this.game.setMode('free');
    };
    FreeGameInfoViewComponent.ctorParameters = function () { return [
        { type: _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_1__["AudioService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"] }
    ]; };
    FreeGameInfoViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-free-game-info-view',
            template: __importDefault(__webpack_require__(/*! raw-loader!./free-game-info-view.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/free-game-info/free-game-info-view/free-game-info-view.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./free-game-info-view.component.scss */ "./src/app/free-game-info/free-game-info-view/free-game-info-view.component.scss")).default]
        }),
        __metadata("design:paramtypes", [_shared_services_audio_service__WEBPACK_IMPORTED_MODULE_1__["AudioService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"]])
    ], FreeGameInfoViewComponent);
    return FreeGameInfoViewComponent;
}());



/***/ }),

/***/ "./src/app/free-game-info/free-game-info.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/free-game-info/free-game-info.module.ts ***!
  \*********************************************************/
/*! exports provided: FreeGameInfoModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FreeGameInfoModule", function() { return FreeGameInfoModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _free_game_info_view_free_game_info_view_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./free-game-info-view/free-game-info-view.component */ "./src/app/free-game-info/free-game-info-view/free-game-info-view.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




var FreeGameInfoModule = /** @class */ (function () {
    function FreeGameInfoModule() {
    }
    FreeGameInfoModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_free_game_info_view_free_game_info_view_component__WEBPACK_IMPORTED_MODULE_1__["FreeGameInfoViewComponent"]],
            imports: [
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_2__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _free_game_info_view_free_game_info_view_component__WEBPACK_IMPORTED_MODULE_1__["FreeGameInfoViewComponent"]
                    }
                ])
            ]
        })
    ], FreeGameInfoModule);
    return FreeGameInfoModule;
}());



/***/ })

}]);
//# sourceMappingURL=free-game-info-free-game-info-module.js.map