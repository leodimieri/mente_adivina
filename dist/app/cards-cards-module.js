(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cards-cards-module"],{

/***/ "./node_modules/angular-fittext/fesm5/angular-fittext.js":
/*!***************************************************************!*\
  !*** ./node_modules/angular-fittext/fesm5/angular-fittext.js ***!
  \***************************************************************/
/*! exports provided: AngularFittextModule, AngularFittextDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AngularFittextModule", function() { return AngularFittextModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AngularFittextDirective", function() { return AngularFittextDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var AngularFittextDirective = /** @class */ (function () {
    function AngularFittextDirective(el, renderer) {
        var _this = this;
        this.el = el;
        this.renderer = renderer;
        this.fittext = true;
        this.compression = 1;
        this.activateOnResize = true;
        this.minFontSize = 0;
        this.maxFontSize = Number.POSITIVE_INFINITY;
        this.delay = 100;
        this.fontUnit = 'px';
        this.calcSize = 10;
        this.onWindowResize = function () {
            if (_this.activateOnResize) {
                _this.setFontSize();
            }
        };
        this.setFontSize = function () {
            _this.resizeTimeout = setTimeout((function () {
                if (_this.fittextElement.offsetHeight * _this.fittextElement.offsetWidth !== 0) {
                    // reset to default
                    // reset to default
                    _this.setStyles(_this.calcSize, 1, 'inline-block');
                    // set new
                    // set new
                    _this.setStyles(_this.calculateNewFontSize(), _this.lineHeight, _this.display);
                }
            }).bind(_this), _this.delay);
        };
        this.calculateNewFontSize = function () {
            var /** @type {?} */ ratio = (_this.calcSize * _this.newlines) / _this.fittextElement.offsetWidth / _this.newlines;
            return Math.max(Math.min((_this.fittextParent.offsetWidth -
                (parseFloat(getComputedStyle(_this.fittextParent).paddingLeft) +
                    parseFloat(getComputedStyle(_this.fittextParent).paddingRight)) -
                6) *
                ratio *
                _this.compression, _this.fittextMaxFontSize), _this.fittextMinFontSize);
        };
        this.setStyles = function (fontSize, lineHeight, display) {
            _this.renderer.setStyle(_this.fittextElement, 'fontSize', fontSize.toString() + _this.fontUnit);
            _this.renderer.setStyle(_this.fittextElement, 'lineHeight', lineHeight.toString());
            _this.renderer.setStyle(_this.fittextElement, 'display', display);
        };
        this.fittextElement = el.nativeElement;
        this.fittextParent = this.fittextElement.parentElement;
        this.computed = window.getComputedStyle(this.fittextElement);
        this.newlines = this.fittextElement.childElementCount > 0 ? this.fittextElement.childElementCount : 1;
        this.lineHeight = this.computed['line-height'];
        this.display = this.computed['display'];
    }
    /**
     * @return {?}
     */
    AngularFittextDirective.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.fittextMinFontSize = this.minFontSize === 'inherit' ? this.computed['font-size'] : this.minFontSize;
        this.fittextMaxFontSize = this.maxFontSize === 'inherit' ? this.computed['font-size'] : this.maxFontSize;
    };
    /**
     * @return {?}
     */
    AngularFittextDirective.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        this.setFontSize();
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    AngularFittextDirective.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes['compression'] && !changes['compression'].firstChange) {
            this.setFontSize();
        }
        if (changes['ngModel']) {
            this.fittextElement.innerHTML = this.ngModel;
            this.setFontSize();
        }
    };
    AngularFittextDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[fittext]'
                },] },
    ];
    /** @nocollapse */
    AngularFittextDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], },
    ]; };
    AngularFittextDirective.propDecorators = {
        "fittext": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "compression": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "activateOnResize": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "minFontSize": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "maxFontSize": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "delay": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "ngModel": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "fontUnit": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "onWindowResize": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['window:resize',] },],
    };
    return AngularFittextDirective;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var AngularFittextModule = /** @class */ (function () {
    function AngularFittextModule() {
    }
    AngularFittextModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    declarations: [
                        AngularFittextDirective
                    ],
                    exports: [
                        AngularFittextDirective
                    ]
                },] },
    ];
    return AngularFittextModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */



//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1maXR0ZXh0LmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9hbmd1bGFyLWZpdHRleHQvc3JjL2FuZ3VsYXItZml0dGV4dC5kaXJlY3RpdmUudHMiLCJuZzovL2FuZ3VsYXItZml0dGV4dC9zcmMvYW5ndWxhci1maXR0ZXh0Lm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0FmdGVyVmlld0luaXQsIERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgSG9zdExpc3RlbmVyLCBJbnB1dCwgT25DaGFuZ2VzLCBPbkluaXQsIFJlbmRlcmVyMiwgU2ltcGxlQ2hhbmdlc30gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1tmaXR0ZXh0XSdcbn0pXG5leHBvcnQgY2xhc3MgQW5ndWxhckZpdHRleHREaXJlY3RpdmUgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0LCBPbkluaXQsIE9uQ2hhbmdlcyB7XG5cbiAgQElucHV0KCkgZml0dGV4dD8gPSB0cnVlO1xuICBASW5wdXQoKSBjb21wcmVzc2lvbj8gPSAxO1xuICBASW5wdXQoKSBhY3RpdmF0ZU9uUmVzaXplPyA9IHRydWU7XG4gIEBJbnB1dCgpIG1pbkZvbnRTaXplPzogbnVtYmVyIHwgJ2luaGVyaXQnID0gMDtcbiAgQElucHV0KCkgbWF4Rm9udFNpemU/OiBudW1iZXIgfCAnaW5oZXJpdCcgPSBOdW1iZXIuUE9TSVRJVkVfSU5GSU5JVFk7XG4gIEBJbnB1dCgpIGRlbGF5PyA9IDEwMDtcbiAgQElucHV0KCkgbmdNb2RlbDtcbiAgQElucHV0KCkgZm9udFVuaXQ/OiAncHgnIHwgJ2VtJyB8IHN0cmluZyA9ICdweCc7XG5cbiAgcHJpdmF0ZSBmaXR0ZXh0UGFyZW50OiBIVE1MRWxlbWVudDtcbiAgcHJpdmF0ZSBmaXR0ZXh0RWxlbWVudDogSFRNTEVsZW1lbnQ7XG4gIHByaXZhdGUgZml0dGV4dE1pbkZvbnRTaXplOiBudW1iZXI7XG4gIHByaXZhdGUgZml0dGV4dE1heEZvbnRTaXplOiBudW1iZXI7XG4gIHByaXZhdGUgY29tcHV0ZWQ6IENTU1N0eWxlRGVjbGFyYXRpb247XG4gIHByaXZhdGUgbmV3bGluZXM6IG51bWJlcjtcbiAgcHJpdmF0ZSBsaW5lSGVpZ2h0OiBzdHJpbmc7XG4gIHByaXZhdGUgZGlzcGxheTogc3RyaW5nO1xuICBwcml2YXRlIGNhbGNTaXplID0gMTA7XG4gIHByaXZhdGUgcmVzaXplVGltZW91dDogbnVtYmVyO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgZWw6IEVsZW1lbnRSZWYsXG4gICAgcHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyXG4gICkge1xuICAgIHRoaXMuZml0dGV4dEVsZW1lbnQgPSBlbC5uYXRpdmVFbGVtZW50O1xuICAgIHRoaXMuZml0dGV4dFBhcmVudCA9IHRoaXMuZml0dGV4dEVsZW1lbnQucGFyZW50RWxlbWVudDtcbiAgICB0aGlzLmNvbXB1dGVkID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUodGhpcy5maXR0ZXh0RWxlbWVudCk7XG4gICAgdGhpcy5uZXdsaW5lcyA9IHRoaXMuZml0dGV4dEVsZW1lbnQuY2hpbGRFbGVtZW50Q291bnQgPiAwID8gdGhpcy5maXR0ZXh0RWxlbWVudC5jaGlsZEVsZW1lbnRDb3VudCA6IDE7XG4gICAgdGhpcy5saW5lSGVpZ2h0ID0gdGhpcy5jb21wdXRlZFsnbGluZS1oZWlnaHQnXTtcbiAgICB0aGlzLmRpc3BsYXkgPSB0aGlzLmNvbXB1dGVkWydkaXNwbGF5J107XG4gIH1cblxuICBASG9zdExpc3RlbmVyKCd3aW5kb3c6cmVzaXplJylcbiAgcHVibGljIG9uV2luZG93UmVzaXplID0gKCk6IHZvaWQgPT4ge1xuICAgIGlmICh0aGlzLmFjdGl2YXRlT25SZXNpemUpIHtcbiAgICAgIHRoaXMuc2V0Rm9udFNpemUoKTtcbiAgICB9XG4gIH07XG5cbiAgcHVibGljIG5nT25Jbml0KCkge1xuICAgIHRoaXMuZml0dGV4dE1pbkZvbnRTaXplID0gdGhpcy5taW5Gb250U2l6ZSA9PT0gJ2luaGVyaXQnID8gdGhpcy5jb21wdXRlZFsnZm9udC1zaXplJ10gOiB0aGlzLm1pbkZvbnRTaXplO1xuICAgIHRoaXMuZml0dGV4dE1heEZvbnRTaXplID0gdGhpcy5tYXhGb250U2l6ZSA9PT0gJ2luaGVyaXQnID8gdGhpcy5jb21wdXRlZFsnZm9udC1zaXplJ10gOiB0aGlzLm1heEZvbnRTaXplO1xuICB9XG5cbiAgcHVibGljIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcbiAgICB0aGlzLnNldEZvbnRTaXplKCk7XG4gIH1cblxuICBwdWJsaWMgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xuICAgIGlmIChjaGFuZ2VzWydjb21wcmVzc2lvbiddICYmICFjaGFuZ2VzWydjb21wcmVzc2lvbiddLmZpcnN0Q2hhbmdlKSB7XG4gICAgICB0aGlzLnNldEZvbnRTaXplKCk7XG4gICAgfVxuICAgIGlmIChjaGFuZ2VzWyduZ01vZGVsJ10pIHtcbiAgICAgIHRoaXMuZml0dGV4dEVsZW1lbnQuaW5uZXJIVE1MID0gdGhpcy5uZ01vZGVsO1xuICAgICAgdGhpcy5zZXRGb250U2l6ZSgpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgc2V0Rm9udFNpemUgPSAoKTogdm9pZCA9PiB7XG4gICAgdGhpcy5yZXNpemVUaW1lb3V0ID0gc2V0VGltZW91dChcbiAgICAgICgoKSA9PiB7XG4gICAgICAgIGlmICh0aGlzLmZpdHRleHRFbGVtZW50Lm9mZnNldEhlaWdodCAqIHRoaXMuZml0dGV4dEVsZW1lbnQub2Zmc2V0V2lkdGggIT09IDApIHtcbiAgICAgICAgICAvLyByZXNldCB0byBkZWZhdWx0XG4gICAgICAgICAgdGhpcy5zZXRTdHlsZXModGhpcy5jYWxjU2l6ZSwgMSwgJ2lubGluZS1ibG9jaycpO1xuICAgICAgICAgIC8vIHNldCBuZXdcbiAgICAgICAgICB0aGlzLnNldFN0eWxlcyh0aGlzLmNhbGN1bGF0ZU5ld0ZvbnRTaXplKCksIHRoaXMubGluZUhlaWdodCwgdGhpcy5kaXNwbGF5KTtcbiAgICAgICAgfVxuICAgICAgfSkuYmluZCh0aGlzKSxcbiAgICAgIHRoaXMuZGVsYXlcbiAgICApO1xuICB9O1xuXG4gIHByaXZhdGUgY2FsY3VsYXRlTmV3Rm9udFNpemUgPSAoKTogbnVtYmVyID0+IHtcbiAgICBjb25zdCByYXRpbyA9ICh0aGlzLmNhbGNTaXplICogdGhpcy5uZXdsaW5lcykgLyB0aGlzLmZpdHRleHRFbGVtZW50Lm9mZnNldFdpZHRoIC8gdGhpcy5uZXdsaW5lcztcblxuICAgIHJldHVybiBNYXRoLm1heChcbiAgICAgIE1hdGgubWluKFxuICAgICAgICAodGhpcy5maXR0ZXh0UGFyZW50Lm9mZnNldFdpZHRoIC1cbiAgICAgICAgICAocGFyc2VGbG9hdChnZXRDb21wdXRlZFN0eWxlKHRoaXMuZml0dGV4dFBhcmVudCkucGFkZGluZ0xlZnQpICtcbiAgICAgICAgICAgIHBhcnNlRmxvYXQoZ2V0Q29tcHV0ZWRTdHlsZSh0aGlzLmZpdHRleHRQYXJlbnQpLnBhZGRpbmdSaWdodCkpIC1cbiAgICAgICAgICA2KSAqXG4gICAgICAgIHJhdGlvICpcbiAgICAgICAgdGhpcy5jb21wcmVzc2lvbixcbiAgICAgICAgdGhpcy5maXR0ZXh0TWF4Rm9udFNpemVcbiAgICAgICksXG4gICAgICB0aGlzLmZpdHRleHRNaW5Gb250U2l6ZVxuICAgICk7XG4gIH07XG5cbiAgcHJpdmF0ZSBzZXRTdHlsZXMgPSAoZm9udFNpemU6IG51bWJlciwgbGluZUhlaWdodDogbnVtYmVyIHwgc3RyaW5nLCBkaXNwbGF5OiBzdHJpbmcpOiB2b2lkID0+IHtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuZml0dGV4dEVsZW1lbnQsICdmb250U2l6ZScsIGZvbnRTaXplLnRvU3RyaW5nKCkgKyB0aGlzLmZvbnRVbml0KTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuZml0dGV4dEVsZW1lbnQsICdsaW5lSGVpZ2h0JywgbGluZUhlaWdodC50b1N0cmluZygpKTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuZml0dGV4dEVsZW1lbnQsICdkaXNwbGF5JywgZGlzcGxheSk7XG4gIH07XG59XG4iLCJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QW5ndWxhckZpdHRleHREaXJlY3RpdmV9IGZyb20gJy4vYW5ndWxhci1maXR0ZXh0LmRpcmVjdGl2ZSc7XG5cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgQW5ndWxhckZpdHRleHREaXJlY3RpdmVcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIEFuZ3VsYXJGaXR0ZXh0RGlyZWN0aXZlXG4gIF1cbn0pXG5cbmV4cG9ydCBjbGFzcyBBbmd1bGFyRml0dGV4dE1vZHVsZSB7XG59XG4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUE7SUEyQkUsaUNBQ1UsSUFDQTtRQUZWLGlCQVVDO1FBVFMsT0FBRSxHQUFGLEVBQUU7UUFDRixhQUFRLEdBQVIsUUFBUTt1QkF0QkUsSUFBSTsyQkFDQSxDQUFDO2dDQUNJLElBQUk7MkJBQ1csQ0FBQzsyQkFDRCxNQUFNLENBQUMsaUJBQWlCO3FCQUNsRCxHQUFHO3dCQUVzQixJQUFJO3dCQVU1QixFQUFFOzhCQWdCRztZQUN0QixJQUFJLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDekIsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQ3BCO1NBQ0Y7MkJBcUJxQjtZQUNwQixLQUFJLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FDN0IsQ0FBQztnQkFDQyxJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxLQUFLLENBQUMsRUFBRTs7O29CQUU1RSxLQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxFQUFFLGNBQWMsQ0FBQyxDQUFDOzs7b0JBRWpELEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLG9CQUFvQixFQUFFLEVBQUUsS0FBSSxDQUFDLFVBQVUsRUFBRSxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQzVFO2FBQ0YsRUFBRSxJQUFJLENBQUMsS0FBSSxDQUFDLEVBQ2IsS0FBSSxDQUFDLEtBQUssQ0FDWCxDQUFDO1NBQ0g7b0NBRThCO1lBQzdCLHFCQUFNLEtBQUssR0FBRyxDQUFDLEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLFFBQVEsSUFBSSxLQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsUUFBUSxDQUFDO1lBRWhHLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FDYixJQUFJLENBQUMsR0FBRyxDQUNOLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXO2lCQUM1QixVQUFVLENBQUMsZ0JBQWdCLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQztvQkFDM0QsVUFBVSxDQUFDLGdCQUFnQixDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDaEUsQ0FBQztnQkFDSCxLQUFLO2dCQUNMLEtBQUksQ0FBQyxXQUFXLEVBQ2hCLEtBQUksQ0FBQyxrQkFBa0IsQ0FDeEIsRUFDRCxLQUFJLENBQUMsa0JBQWtCLENBQ3hCLENBQUM7U0FDSDt5QkFFbUIsVUFBQyxRQUFnQixFQUFFLFVBQTJCLEVBQUUsT0FBZTtZQUNqRixLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsY0FBYyxFQUFFLFVBQVUsRUFBRSxRQUFRLENBQUMsUUFBUSxFQUFFLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzdGLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxjQUFjLEVBQUUsWUFBWSxFQUFFLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQ2pGLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxjQUFjLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1NBQ2pFO1FBckVDLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDLGFBQWEsQ0FBQztRQUN2QyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDO1FBQ3ZELElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxDQUFDO1FBQ3RHLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7S0FDekM7Ozs7SUFTTSwwQ0FBUTs7OztRQUNiLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsV0FBVyxLQUFLLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDekcsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxXQUFXLEtBQUssU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQzs7Ozs7SUFHcEcsaURBQWU7Ozs7UUFDcEIsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDOzs7Ozs7SUFHZCw2Q0FBVzs7OztjQUFDLE9BQXNCO1FBQ3ZDLElBQUksT0FBTyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsRUFBRTtZQUNqRSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDcEI7UUFDRCxJQUFJLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUN0QixJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQzdDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNwQjs7O2dCQTVESixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFdBQVc7aUJBQ3RCOzs7O2dCQUppQyxVQUFVO2dCQUEwQyxTQUFTOzs7NEJBTzVGLEtBQUs7Z0NBQ0wsS0FBSztxQ0FDTCxLQUFLO2dDQUNMLEtBQUs7Z0NBQ0wsS0FBSzswQkFDTCxLQUFLOzRCQUNMLEtBQUs7NkJBQ0wsS0FBSzttQ0F5QkwsWUFBWSxTQUFDLGVBQWU7O2tDQXZDL0I7Ozs7Ozs7QUNBQTs7OztnQkFJQyxRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFO3dCQUNaLHVCQUF1QjtxQkFDeEI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLHVCQUF1QjtxQkFDeEI7aUJBQ0Y7OytCQVhEOzs7Ozs7Ozs7Ozs7Ozs7In0=

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/answer-button/answer-button.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/answer-button/answer-button.component.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div fxLayout=\"column\" fxFlex=\"grow\" style=\"margin: 10px 5px\" fxLayoutGap=\"4px\">\n  <div fxFlex=\"grow\" [style.background-color]=\"cardColor\" style=\"border-radius: 10px; box-shadow: lightgray -3px 3px\">\n    <div  class=\"full-width answer-text-height\" style=\"position: relative\">\n      <div [ngStyle]=\"{'opacity': show ? '1' : '0'}\" style=\"position: absolute\" class=\"answer-text\"  [minFontSize]=\"18\" [maxFontSize]=\"30\" fittext>\n        {{ answer}}\n      </div>\n      <div  fxFlex=\"grow\" fxLayoutAlign=\"column\" [ngStyle]=\"{'opacity': show ? '0' : '1'}\" class=\"answer-text\" style=\"position: absolute\" [minFontSize]=\"22\" [maxFontSize]=\"40\" fittext>\n        RESPUESTA\n      </div>\n\n    </div>\n  </div>\n  <span style=\"font-size: 16px; color: gray\">Solo mira la respuesta el que lee</span>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/card-header/card-header.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/card-header/card-header.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div fxFlex=\"grow\" fxLayout=\"row\" fxLayoutAlign=\"start center\" style=\"border: 2px indianred dot-dash\">\n  <mat-icon>edit</mat-icon> <h3>Adivinanzas</h3>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/card-view/card-view.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/card-view/card-view.component.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div fxFlex=\"grow\" xmlns:xhtml=\"http://www.w3.org/1999/xhtml\" [ngClass]=\" 'background-level-' + (gameService.level)\">\n  <app-settings-button style=\"position: absolute; left: 0; top: 0;\"></app-settings-button>\n\n  <svg preserveAspectRatio=\"xMidYMin\" fxFlexOffset=\"15\" fxFlex=\"70\" style=\"max-width: 100%; max-height: 100%\"  xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n         viewBox=\"0 0 698 498.3\">\n    <g>\n      <path d=\"M692.1.2v459c0 18.6-15.2 33.8-33.8 33.8h-618c-18.6 0-33.8-15.2-33.8-33.8V.2h685.6z\" fill=\"#b1b3b6\"></path>\n      <path d=\"M668.3.2v465.3c0 9.8-8 17.8-17.9 17.8H45.8c-9.9 0-17.9-8-17.9-17.8V.2h640.4z\" fill=\"#9d9fa2\"></path>\n      <path d=\"M667.4.2v462.5c0 9.8-8 17.8-17.8 17.8H49c-9.8 0-17.8-8-17.8-17.8V.2h636.2z\" fill=\"#fff\"></path>\n      <circle class=\"st3\" cx=\"18.5\" cy=\"187.3\" r=\"5.9\"></circle>\n      <defs><ellipse id=\"ellipse-card-container1\"  cx=\"19.1\" cy=\"186.7\" rx=\"6\" ry=\"6\"></ellipse></defs>\n      <use xlink:href=\"#ellipse-card-container1\" overflow=\"visible\" fill=\"#dedfe0\"></use>\n      <clipPath id=\"ellipse-card-container2\"><use xlink:href=\"#ellipse-card-container1\" overflow=\"visible\"></use></clipPath>\n      <path clip-path=\"url(#ellipse-card-container2)\" fill=\"none\" stroke=\"#8a8c8e\" stroke-width=\"2.001\" stroke-miterlimit=\"10\"\n            d=\"M14.4 192.3l9.8-11.3\"></path>\n      <g><ellipse  class=\"st3\" cx=\"18.7\" cy=\"73.5\" rx=\"5.9\" ry=\"5.9\"></ellipse>\n        <defs><ellipse id=\"ellipse-card-container3\"  cx=\"19\" cy=\"74.4\" rx=\"6\" ry=\"6\"></ellipse></defs>\n        <use xlink:href=\"#ellipse-card-container3\" overflow=\"visible\" fill=\"#dedfe0\"></use>\n        <clipPath id=\"ellipse-card-container4\"><use xlink:href=\"#ellipse-card-container3\" overflow=\"visible\"></use></clipPath>\n        <path clip-path=\"url(#ellipse-card-container4)\" fill=\"none\" stroke=\"#8a8c8e\" stroke-width=\"2.001\" stroke-miterlimit=\"10\"\n              d=\"M16.1 67.5l5.6 14\"></path></g>\n      <g><ellipse transform=\"rotate(-32.839 18.992 299.385)\" class=\"st3\" cx=\"19\" cy=\"299.4\" rx=\"5.9\" ry=\"5.9\"></ellipse>\n        <defs><ellipse id=\"ellipse-card-container5\" transform=\"rotate(-38.157 18.802 300.355)\" cx=\"18.8\" cy=\"300.3\" rx=\"6\" ry=\"6\"></ellipse></defs>\n        <use xlink:href=\"#ellipse-card-container5\" overflow=\"visible\" fill=\"#dedfe0\"></use>\n        <clipPath id=\"ellipse-card-container6\"><use xlink:href=\"#ellipse-card-container5\" overflow=\"visible\"></use></clipPath>\n        <path clip-path=\"url(#ellipse-card-container6)\" fill=\"none\" stroke=\"#8a8c8e\" stroke-width=\"2.001\" stroke-miterlimit=\"10\"\n              d=\"M19.7 293l-2.1 14.9\"></path></g>\n      <g><ellipse transform=\"rotate(-32.839 18.992 412.477)\" class=\"st3\" cx=\"19\" cy=\"412.5\" rx=\"5.9\" ry=\"5.9\"></ellipse>\n        <defs><ellipse id=\"ellipse-card-container7\" transform=\"rotate(-38.157 18.803 413.465)\" cx=\"18.8\" cy=\"413.4\" rx=\"6\" ry=\"6\"></ellipse></defs>\n        <use xlink:href=\"#ellipse-card-container7\" overflow=\"visible\" fill=\"#dedfe0\"></use>\n        <clipPath id=\"ellipse-card-container8\"><use xlink:href=\"#ellipse-card-container7\" overflow=\"visible\"></use></clipPath>\n        <path clip-path=\"url(#ellipse-card-container8)\" fill=\"none\" stroke=\"#8a8c8e\" stroke-width=\"2.001\" stroke-miterlimit=\"10\"\n              d=\"M19.7 406.1L17.6 421\"></path></g>\n      <g><circle class=\"st3\" cx=\"680.9\" cy=\"187.3\" r=\"5.9\"></circle>\n        <defs><ellipse id=\"ellipse-card-container9\" transform=\"rotate(-5.321 681.536 186.652) scale(1.00001)\" cx=\"681.5\" cy=\"186.7\" rx=\"6\" ry=\"6\"></ellipse></defs>\n        <use xlink:href=\"#ellipse-card-container9\" overflow=\"visible\" fill=\"#dedfe0\"></use>\n        <clipPath id=\"ellipse-card-container10\"><use xlink:href=\"#ellipse-card-container9\" overflow=\"visible\"></use></clipPath>\n        <path clip-path=\"url(#ellipse-card-container10)\" fill=\"none\" stroke=\"#8a8c8e\" stroke-width=\"2.001\" stroke-miterlimit=\"10\"\n              d=\"M676.8 192.3l9.8-11.3\"></path>\n        <g><ellipse transform=\"rotate(-62.626 681.076 73.466)\" class=\"st3\" cx=\"681.1\" cy=\"73.5\" rx=\"5.9\" ry=\"5.9\"></ellipse>\n          <defs><ellipse id=\"ellipse-card-container11\" transform=\"rotate(-67.95 681.367 74.371)\" cx=\"681.3\" cy=\"74.4\" rx=\"6\" ry=\"6\"></ellipse></defs>\n          <use xlink:href=\"#ellipse-card-container11\" overflow=\"visible\" fill=\"#dedfe0\"></use>\n          <clipPath id=\"ellipse-card-container12\"><use xlink:href=\"#ellipse-card-container11\" overflow=\"visible\"></use></clipPath>\n          <path clip-path=\"url(#ellipse-card-container12)\" fill=\"none\" stroke=\"#8a8c8e\" stroke-width=\"2.001\" stroke-miterlimit=\"10\"\n                d=\"M678.5 67.5l5.5 14\"></path></g>\n        <g><ellipse transform=\"rotate(-32.839 681.32 299.387)\" class=\"st3\" cx=\"681.4\" cy=\"299.4\" rx=\"5.9\" ry=\"5.9\"></ellipse>\n          <defs><ellipse id=\"ellipse-card-container13\" transform=\"rotate(-38.157 681.236 300.344)\" cx=\"681.2\" cy=\"300.3\" rx=\"6\" ry=\"6\"></ellipse></defs>\n          <use xlink:href=\"#ellipse-card-container13\" overflow=\"visible\" fill=\"#dedfe0\"></use>\n          <clipPath id=\"ellipse-card-container14\"><use xlink:href=\"#ellipse-card-container13\" overflow=\"visible\"></use></clipPath>\n          <path clip-path=\"url(#ellipse-card-container14)\" fill=\"none\" stroke=\"#8a8c8e\" stroke-width=\"2.001\" stroke-miterlimit=\"10\"\n                d=\"M682.1 293l-2.1 14.9\"></path></g>\n        <g><ellipse transform=\"rotate(-32.839 681.32 412.48)\" class=\"st3\" cx=\"681.4\" cy=\"412.5\" rx=\"5.9\" ry=\"5.9\"></ellipse>\n          <defs><ellipse id=\"ellipse-card-container15\" transform=\"rotate(-38.157 681.238 413.455)\" cx=\"681.2\" cy=\"413.4\" rx=\"6\" ry=\"6\"></ellipse></defs>\n          <use xlink:href=\"#ellipse-card-container15\" overflow=\"visible\" fill=\"#dedfe0\"></use>\n          <clipPath id=\"ellipse-card-container16\"><use xlink:href=\"#ellipse-card-container15\" overflow=\"visible\"></use></clipPath>\n          <path clip-path=\"url(#ellipse-card-container16)\" fill=\"none\" stroke=\"#8a8c8e\" stroke-width=\"2.001\" stroke-miterlimit=\"10\"\n                d=\"M682.1 406.1L680 421\"></path></g></g>\n    </g>\n    <foreignObject style=\"width: 100%; height: 100%\">\n\n      <xhtml:div fxLayout=\"column\"  style=\"padding-left: 5%; padding-right: 5%; padding-bottom: 2%; height: 498px\">\n        <router-outlet ></router-outlet>\n      </xhtml:div>\n    </foreignObject>\n\n    <svg *ngIf=\"showAllStars\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0\" y=\"0\" viewBox=\"0 0 1061 599\" class=\"stars\"\n    ><style>.st0{fill:#ed1c24}.st1{fill:#92278f}.st2{fill:#00aeef}.st3{fill:#8dc63f}.st4{fill:#ffef00}.st5{fill:#ee2a7b}</style>\n      <path class=\"st0\"\n            d=\"M92.1 251.4l-31.5-20.6-34.9 14.3 9.8-36.4-24.4-28.8 37.7-1.8L68.6 146 82 181.2l36.7 8.9-29.4 23.7zM401.3 139.4l-21.6-30.9-37.7.2 22.7-30-11.8-35.8 35.6 12.3 30.4-22.3-.7 37.7 30.6 21.9-36.1 11z\"></path>\n      <path class=\"st1\" d=\"M223 165.5l-23.3-15.2-25.8 10.6 7.3-26.9-18-21.2 27.8-1.4 14.6-23.7 9.9 26 27 6.6-21.6 17.5z\"></path>\n      <path class=\"st2\" d=\"M314.3 207.2l-10.2-6.6-11.2 4.6 3.2-11.7-7.8-9.2 12.1-.6 6.3-10.3 4.3 11.3 11.8 2.9-9.4 7.6z\"></path>\n      <path class=\"st3\"\n            d=\"M225.3 294.7l-5.7-20.9-20.5-7.1 18.1-11.9.5-21.6 16.9 13.5 20.7-6.2-7.7 20.2 12.4 17.8-21.6-1z\"></path>\n      <path class=\"st1\"\n            d=\"M254.5 442.7l-37.7 1.8-19.9 32.1-13.3-35.3-36.7-9 29.5-23.6-2.8-37.6 31.5 20.7 35-14.2-10 36.3z\"></path>\n      <path class=\"st2\"\n            d=\"M497.5 222.1l-24.1-8.4-20.6 15.1.5-25.5-20.7-14.9 24.5-7.4 7.7-24.3 14.6 20.9 25.6-.1-15.5 20.4z\"></path>\n      <path class=\"st4\"\n            d=\"M319.6 358.2l-27.8 1.4-14.7 23.6-9.8-26-27-6.6 21.7-17.4-2.1-27.8 23.3 15.3 25.7-10.5-7.3 26.8z\"></path>\n      <path class=\"st5\" d=\"M494.7 323.9l-12.1.5-6.4 10.3-4.3-11.3-11.8-2.9 9.5-7.6-.9-12.1 10.1 6.7 11.3-4.6-3.2 11.7z\"></path>\n      <path class=\"st4\" d=\"M117.1 330.5l-12.1.5-6.4 10.3-4.3-11.3-11.8-2.9 9.5-7.6-.9-12.1 10.1 6.7 11.2-4.6-3.1 11.7z\"></path>\n      <path class=\"st5\"\n            d=\"M449.1 470.4l-16.9-13.6-20.7 6.2 7.7-20.2-12.3-17.8 21.6 1.1 13.1-17.3 5.7 20.9 20.4 7.2-18.1 11.8z\"></path>\n      <path class=\"st2\"\n            d=\"M941.5 181.2l36.7 8.5 27.8-25.4 3.3 37.5 32.8 18.7-34.7 14.7-7.6 36.9-24.7-28.4-37.5 4.2 19.5-32.3z\"></path>\n      <path class=\"st4\"\n            d=\"M540.2 482.2l31 21.6 35.3-13.2-11 36.1 23.5 29.5-37.8.6-20.8 31.5-12.3-35.7-36.3-10 30.1-22.8z\"></path>\n      <path class=\"st0\"\n            d=\"M831.9 352.4l27.1 6.3 20.6-18.8 2.4 27.7 24.2 13.7-25.6 10.9-5.6 27.3-18.2-21-27.7 3.1 14.4-23.9z\"></path>\n      <path class=\"st5\" d=\"M748.1 299.2l11.8 2.7 8.9-8.2 1.1 12.1 10.5 6-11.1 4.7-2.5 11.9-7.9-9.2-12 1.4 6.2-10.4z\"></path>\n      <path fill=\"#f281b3\"\n            d=\"M831.8 170.5l12.5 17.7 21.6-.4-12.8 17.4 7 20.5-20.6-6.9-17.3 13 .3-21.6-17.8-12.5 20.7-6.5z\"></path>\n      <path class=\"st1\" d=\"M663.9 47.7L688 37.5l5.3-25.6 17.2 19.8 26-2.9L723 51.2l10.8 23.9-25.5-6-19.4 17.7-2.2-26.1z\"></path>\n      <path class=\"st3\"\n            d=\"M604.8 343.8l25.5-.4 14.2-21.3 8.2 24.2 24.6 6.8-20.4 15.4L658 394l-20.9-14.7-24 8.9 7.5-24.4z\"></path>\n      <path fill=\"#00a79d\"\n            d=\"M576.8 179.1l25.7-10.8 5.6-27.3 18.2 21.1 27.6-3.1-14.3 23.8 11.4 25.4-27.1-6.4-20.5 18.8-2.4-27.7z\"></path>\n      <path class=\"st5\" d=\"M538.6 251.8l11.1-4.7 2.5-11.9 7.9 9.2 12-1.3-6.2 10.3 5 11.1-11.8-2.8-9 8.2-1-12.1z\"></path>\n      <path fill=\"#00a651\" d=\"M890.8 115.6l11.2-4.7 2.4-11.9 8 9.2 12-1.3-6.3 10.3 5 11.1-11.8-2.8-8.9 8.2-1.1-12.1z\"></path>\n      <path class=\"st4\" d=\"M529.8 76l20.5 6.9 17.3-13-.2 21.7 17.7 12.5-20.7 6.4-6.4 20.7-12.5-17.7-21.7.3 13-17.3z\"></path>\n    </svg>\n    <svg *ngIf=\"showHalfStars\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0\" y=\"0\" viewBox=\"0 0 1061 599\" class=\"stars\"\n    ><style>.st0{fill:#ed1c24}.st1{fill:#92278f}.st2{fill:#00aeef}.st3{fill:#8dc63f}.st4{fill:#ffef00}.st5{fill:#ee2a7b}</style>\n      <path class=\"st0\"\n            d=\"M92.1 251.4l-31.5-20.6-34.9 14.3 9.8-36.4-24.4-28.8 37.7-1.8L68.6 146 82 181.2l36.7 8.9-29.4 23.7zM401.3 139.4l-21.6-30.9-37.7.2 22.7-30-11.8-35.8 35.6 12.3 30.4-22.3-.7 37.7 30.6 21.9-36.1 11z\"></path>\n      <path class=\"st1\" d=\"M223 165.5l-23.3-15.2-25.8 10.6 7.3-26.9-18-21.2 27.8-1.4 14.6-23.7 9.9 26 27 6.6-21.6 17.5z\"></path>\n      <path class=\"st2\" d=\"M314.3 207.2l-10.2-6.6-11.2 4.6 3.2-11.7-7.8-9.2 12.1-.6 6.3-10.3 4.3 11.3 11.8 2.9-9.4 7.6z\"></path>\n\n      <path class=\"st4\"\n            d=\"M319.6 358.2l-27.8 1.4-14.7 23.6-9.8-26-27-6.6 21.7-17.4-2.1-27.8 23.3 15.3 25.7-10.5-7.3 26.8z\"></path>\n      <path class=\"st5\" d=\"M494.7 323.9l-12.1.5-6.4 10.3-4.3-11.3-11.8-2.9 9.5-7.6-.9-12.1 10.1 6.7 11.3-4.6-3.2 11.7z\"></path>\n      <path class=\"st5\"\n            d=\"M449.1 470.4l-16.9-13.6-20.7 6.2 7.7-20.2-12.3-17.8 21.6 1.1 13.1-17.3 5.7 20.9 20.4 7.2-18.1 11.8z\"></path>\n      <path class=\"st2\"\n            d=\"M941.5 181.2l36.7 8.5 27.8-25.4 3.3 37.5 32.8 18.7-34.7 14.7-7.6 36.9-24.7-28.4-37.5 4.2 19.5-32.3z\"></path>\n      <path class=\"st4\"\n            d=\"M540.2 482.2l31 21.6 35.3-13.2-11 36.1 23.5 29.5-37.8.6-20.8 31.5-12.3-35.7-36.3-10 30.1-22.8z\"></path>\n\n      <path class=\"st5\" d=\"M538.6 251.8l11.1-4.7 2.5-11.9 7.9 9.2 12-1.3-6.2 10.3 5 11.1-11.8-2.8-9 8.2-1-12.1z\"></path>\n      <path fill=\"#00a651\" d=\"M890.8 115.6l11.2-4.7 2.4-11.9 8 9.2 12-1.3-6.3 10.3 5 11.1-11.8-2.8-8.9 8.2-1.1-12.1z\"></path>\n      <path class=\"st4\" d=\"M529.8 76l20.5 6.9 17.3-13-.2 21.7 17.7 12.5-20.7 6.4-6.4 20.7-12.5-17.7-21.7.3 13-17.3z\"></path>\n    </svg>\n  </svg>\n  <div *ngIf=\"gameService.gameMode !== 'free' && (showHalfStars || showAllStars)\" style=\"position: absolute; bottom: 5%;\" class=\"full-width\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\n    <span style=\"font-size: 30px; background-color: rgba(128,128,128,0.9); color: white; padding: 2%; border-radius: 10px\">\n      {{showHalfStars ? '¡Avanza 1 lugar!' : '¡Avanza 2 lugares!'}}\n    </span>\n  </div>\n    <div *ngIf=\"gameService.gameMode === 'complete'\" fxLayout=\"column\" fxLayoutAlign=\"start\" fxFlex=\"15\">\n      <app-character-action fxFlex=\"30\" class=\"full-width\" [action]=\"'Responde'\" [player]=\"gameService.players[gameService.currentTurn.answer]\"></app-character-action>\n      <app-character-action fxFlexOffset=\"10\" fxFlex=\"30\" class=\"full-width\" [action]=\"'Lee'\" [player]=\"gameService.players[gameService.currentTurn.read]\"></app-character-action>\n    </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/character-action/character-action.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/character-action/character-action.component.html ***!
  \*************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<svg fxFlex=\"grow\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" viewBox=\"0 0 126.4 149\" >\n    <path [attr.fill]=\"player.color\"  d=\"M113.8 65.7h7.4v18.6h-7.4zM4.6 68.2h4.7v11.5H4.6z\"></path>\n    <path [attr.fill]=\"player.color\"  d=\"M8.3 65.7h7.4v18.6H8.3z\"></path>\n    <path [attr.fill]=\"player.color\"\n          d=\"M21 7c-5.3-.2-9.8 3.9-10 9l3.1 120.5c-.1 5.2 4.1 9.6 9.5 9.8h82.6c5.3.2 9.8-5.5 10-10.7l1.3-120.5c.1-5.2-4.1-9.6-9.5-9.8L21 7z\"></path>\n    <path fill=\"white\"\n          d=\"M26.2 32.3c-4.9-.2-8.9 2.9-9 6.7l1.6 67.5c-.1 3.9 3.8 7.2 8.6 7.3l75.6-.6c4.9.2 8.9-2.9 9-6.7V39.8c.1-3.9-3.8-7.2-8.6-7.3l-77.2-.2zM103.9 140.8H27.2c-4.7 0-8.5-3.8-8.5-8.5v-4.7c0-4.7 3.8-8.5 8.5-8.5h76.7c4.7 0 8.5 3.8 8.5 8.5v4.7c0 4.7-3.8 8.5-8.5 8.5z\"></path>\n    <g>\n        <clipPath id=\"clip-path-read-answer\">\n            <path  d=\"M26.2 32.3c-4.9-.2-8.9 2.9-9 6.7l1.6 67.5c-.1 3.9 3.8 7.2 8.6 7.3l75.6-.6c4.9.2 8.9-2.9 9-6.7V39.8c.1-3.9-3.8-7.2-8.6-7.3l-77.2-.2z\"></path></clipPath>\n        <g clip-path=\"url(#clip-path-read-answer)\">\n            <!--<use xlink:href=\"assets/images/robots/robot_1.svg\"/>-->\n            <use id=\"card-robot\" transform=\"translate(3, 20)\"  [attr.xlink:href]=\"'assets/images/robots/robots_level' + game.level + '.svg#robot' + player.robot.id\"></use>\n        </g>\n    </g>\n    <g>\n        <text font-size='16' text-anchor=\"middle\" x=\"50%\" fill=\"white\" y=\"18%\">{{action}}</text>\n        <text font-size='16' text-anchor=\"middle\" x=\"50%\" fill=\"black\" y=\"90%\">{{player.name}}</text>\n    </g>\n</svg>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/correct-button/correct-button.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/correct-button/correct-button.component.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div fxFill fxLayout=\"row\" fxLayoutAlign=\"center center\">\n  <svg class=\"full-width\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"10 10 75 75\">\n    <path d=\"M62.6 81.8H30.1c-9.4 0-17-7.7-17-17V32.3c0-9.4 7.7-17 17-17h32.5c9.4 0 17 7.7 17 17v32.5c0 9.4-7.6 17-17 17z\"\n          fill=\"#d1d3d4\"></path>\n    <path d=\"M64.5 79H31.9c-9.4 0-17-7.7-17-17V29.5c0-9.4 7.7-17 17-17h32.5c9.4 0 17 7.7 17 17V62c.1 9.4-7.6 17-16.9 17z\"\n          fill=\"#ed145b\"></path>\n    <path d=\"M38.1 40.4l7.1 17.4 12-26.8c.8-1.9 1.5-3.3 2.3-4.1.7-.8 1.9-1.2 3.4-1.2 1.4 0 2.6.4 3.6 1.3s1.5 1.9 1.5 3c0 .4-.1 1-.3 1.5-.2.6-.4 1.1-.6 1.7-.2.5-.5 1.1-.8 1.8l-13 26.9c-.3.7-.7 1.6-1.2 2.6-.5 1.1-1 2-1.6 2.7-.6.7-1.3 1.3-2.2 1.8-.9.4-1.9.6-3.1.6-1.6 0-2.9-.3-3.8-1s-1.6-1.4-2.1-2.1c-.5-.8-1.2-2.3-2.3-4.6l-8.7-18.2c-.2-.6-.5-1.2-.8-1.8-.3-.6-.5-1.2-.7-1.8-.2-.6-.3-1.1-.3-1.6 0-.7.2-1.4.7-2 .5-.7 1.1-1.2 1.9-1.7.8-.4 1.7-.6 2.7-.6 1.9 0 3.1.5 3.8 1.4.9.9 1.6 2.5 2.5 4.8z\"\n          fill=\"#b80545\"></path>\n    <path d=\"M39 38.5l7.1 17.4 12-26.8c.8-1.9 1.5-3.3 2.3-4.1.7-.8 1.9-1.2 3.4-1.2 1.4 0 2.6.4 3.6 1.3s1.5 1.9 1.5 3c0 .4-.1 1-.3 1.5-.2.6-.4 1.1-.6 1.7-.2.5-.5 1.1-.8 1.8L54.2 60c-.3.7-.7 1.6-1.2 2.6-.5 1.1-1 2-1.6 2.7-.6.7-1.3 1.3-2.2 1.8-.9.4-1.9.6-3.1.6-1.6 0-2.9-.3-3.8-1-.9-.7-1.6-1.4-2.1-2.1-.5-.8-1.2-2.3-2.3-4.6l-8.7-18.2c-.2-.6-.5-1.2-.8-1.8-.3-.6-.5-1.2-.7-1.8-.2-.6-.3-1.1-.3-1.6 0-.7.2-1.4.7-2 .5-.7 1.1-1.2 1.9-1.7.8-.4 1.7-.6 2.7-.6 1.9 0 3.1.5 3.8 1.4.9.9 1.7 2.5 2.5 4.8z\"\n          fill=\"#fff\"></path>\n  </svg>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/guessword-card/guessword-card.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/guessword-card/guessword-card.component.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div fxFlex=\"grow\" fxLayout=\"column\" >\n  <div fxLayout=\"column\" fxLayoutAlign=\"space-between\" fxFlex=\"75\" class=\"full-width\"  >\n    <div fxFlex=\"20\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\n      <svg width=\"10%\" height=\"80%\">\n        <path class=\"st0\"\n              d=\"M41.4 32.1c-1.8 0-3.5-.3-5.1-1-3.2-1.4-5.8-3.9-7.1-7.2-1.3-3.3-1.3-6.8.1-10.1 2.1-4.9 6.8-8.1 12.1-8.1 1.8 0 3.5.3 5.1 1 6.7 2.8 9.9 10.6 7 17.3-2 4.9-6.8 8.1-12.1 8.1zm0-24.2c-4.4 0-8.4 2.6-10.1 6.7-1.1 2.7-1.2 5.7 0 8.4 1.1 2.7 3.2 4.8 5.9 6 1.4.6 2.8.9 4.3.9 4.4 0 8.4-2.6 10.1-6.7 2.4-5.6-.3-12-5.9-14.4-1.4-.6-2.9-.9-4.3-.9z\"></path>\n        <path class=\"st0\"\n              d=\"M31.1 23.3s-26.2 8.8-23 15.6C11.3 45.8 32.7 26 32.7 26M36.6 27.1c-.2 0-.4-.1-.6-.2-2.7-2.8-3.9-5.7-3.7-8.7.4-4.8 4.5-7.9 4.6-8 .4-.3.8-.2 1.1.2.3.4.2.8-.2 1.1 0 0-3.7 2.8-4 6.9-.2 2.5.9 5 3.3 7.4.3.3.3.8 0 1.1-.1.2-.3.2-.5.2z\"></path>\n\n      </svg>\n      <div class=\"title nunito\"  [minFontSize]=\"22\" [maxFontSize]=\"45\" fittext>\n        {{card.cardName | uppercase}}\n      </div>\n    </div>\n\n    <div fxFlex=\"80\" fxLayout=\"column\" fxLayoutAlign=\"start\" fxLayoutGap=\"3%\" class=\"app-card-texts\" [ngClass]=\"'text-level-' + card.level\" >\n\n      <div fxLayout=\"column\" fxLayoutGap=\"1%\">\n        <span class=\"guessword-title\" *ngIf=\"card.level < 3\">Pistas:</span>\n        <span class=\"guessword-title\" *ngIf=\"card.level === 3\">¿Cuál es el objeto que se relaciona con estas palabras?</span>\n\n        <span class=\"phrase-text\" *ngFor=\"let line of card.inititalTexts, let i = index\"> {{i + 1}} - {{line}}</span>\n      </div>\n      <div class=\"full-width\" *ngIf=\"card.level < 3 && cardService.shoeExtraTexts.value\" fxLayout=\"column\" fxLayoutGap=\"1%\" fxLayoutAlign=\"start start\" >\n        <span class=\"guessword-title\">Más pistas:</span>\n        <span *ngFor=\"let extra of card.extraTexts, let i = index\" class=\"option-text\" >{{i + 1 + card.inititalTexts.length}} -  {{extra}}</span>\n\n      </div>\n      <div class=\"full-width\" *ngIf=\"card.level === 3 && cardService.shoeExtraTexts.value\" fxLayout=\"column\" fxLayoutGap=\"1%\" fxLayoutAlign=\"start start\" >\n        <span *ngIf=\"card.extraTexts.length > 1\" class=\"guessword-title\">Opciones:</span>\n        <span *ngIf=\"card.extraTexts.length === 1\" class=\"guessword-title\">Ayuda:</span>\n        <div class=\"full-width\" fxLayout=\"row\" fxLayoutAlign=\"space-evenly center\">\n          <span *ngFor=\"let extra of card.extraTexts, let i = index\" class=\"option-text\" >{{extra}}</span>\n\n        </div>\n\n      </div>\n    </div>\n  </div>\n  <div fxLayout=\"row\" fxFlex=\"25\" fxLayoutAlign=\"start\" [fxLayoutGap]=\"card.level === 3 ? '2.5%' : '5%'\">\n    <app-answer-button [cardColor]=\"'#ec008c'\" [answer]=\"cardService.currentCard.answer\" [fxFlex]=\"card.level === 3 ? '60' : '50'\"></app-answer-button>\n    <ng-container *ngIf=\"cardService.currentAnswerIsViewed\">\n      <app-correct-button (click)=\"correctAnswer()\" fxFlex=\"10\"></app-correct-button>\n      <app-options-button (click)=\"onClickOptions()\" *ngIf=\"!cardService.shoeExtraTexts.value &&\n                cardService.currentCard.extraTexts && cardService.currentCard.extraTexts.length > 0\" fxFlex=\"25\"></app-options-button>\n      <app-wrong-button (click)=\"wrongAnswer()\" *ngIf=\"cardService.shoeExtraTexts.value || !cardService.currentCard.extraTexts\n    || cardService.currentCard.extraTexts.length === 0 \" fxFlex=\"10\"></app-wrong-button>\n    </ng-container>\n\n  </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/options-button/options-button.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/options-button/options-button.component.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<svg fxFlex=\"grow\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 176.4 69.35\">\n    <text transform=\"translate(77.39 29.47)\" font-size=\"18\" fill=\"#92278f\" font-family=\"FiraSans-Bold,Fira Sans\"\n          font-weight=\"700\">¿N\n        <tspan x=\"20.59\" y=\"0\" letter-spacing=\"-.0139974em\">E</tspan>\n        <tspan x=\"29.81\" y=\"0\">C</tspan>\n        <tspan x=\"40.16\" y=\"0\" letter-spacing=\"-.00998264em\">E</tspan>\n        <tspan x=\"49.45\" y=\"0\">SI</tspan>\n        <tspan x=\"65.02\" y=\"0\" letter-spacing=\"-.0500217em\">T</tspan>\n        <tspan x=\"73.83\" y=\"0\">AS</tspan>\n        <tspan letter-spacing=\"-.05598958em\">\n            <tspan x=\"0\" y=\"20.49\">A</tspan>\n            <tspan x=\"9.61\" y=\"20.49\" letter-spacing=\"-.00005425em\">YU</tspan>\n            <tspan x=\"32.02\" y=\"20.49\" letter-spacing=\"-.00998264em\">D</tspan>\n            <tspan x=\"43.56\" y=\"20.49\" letter-spacing=\"-.03000217em\">A</tspan>\n            <tspan x=\"53.64\" y=\"20.49\" letter-spacing=\"-.00005425em\">?</tspan>\n        </tspan>\n    </text>\n    <rect y=\"2.79\" width=\"66.56\" height=\"66.56\" rx=\"17.01\" ry=\"17.01\" fill=\"#d1d3d4\"></rect>\n    <rect x=\"1.86\" width=\"66.56\" height=\"66.56\" rx=\"17.01\" ry=\"17.01\" fill=\"#92278f\"></rect>\n    <path d=\"M17.65 38.11a3.05 3.05 0 0 1 0-6.1H52.4a3.05 3.05 0 0 1 0 6.1z\" fill=\"#6d0e6b\"></path>\n    <path d=\"M52.4 32.87a2.19 2.19 0 1 1 0 4.38H17.65a2.19 2.19 0 1 1 0-4.38H52.4m0-1.72H17.65a3.91 3.91 0 1 0 0 7.81H52.4a3.91 3.91 0 1 0 0-7.81z\"\n          fill=\"#741472\"></path>\n    <path d=\"M35.02 55.45a3.05 3.05 0 0 1-3-3V17.68a3.05 3.05 0 1 1 6.1 0v34.77a3.05 3.05 0 0 1-3.1 3z\" fill=\"#6d0e6b\"></path>\n    <path d=\"M35.02 15.45a2.19 2.19 0 0 1 2.2 2.23v34.77a2.19 2.19 0 0 1-4.38 0V17.68a2.19 2.19 0 0 1 2.18-2.23m0-1.72a3.92 3.92 0 0 0-3.91 3.91v34.81a3.91 3.91 0 0 0 7.81 0V17.68a3.92 3.92 0 0 0-3.91-3.91z\"\n          fill=\"#741472\"></path>\n    <rect x=\"47.46\" y=\"26.77\" width=\"7.81\" height=\"42.56\" rx=\"3.91\" ry=\"3.91\" transform=\"rotate(90 51.245 33.385)\"\n          fill=\"#fff\" stroke=\"#fff\" stroke-miterlimit=\"10\" stroke-width=\".85884477\"></rect>\n    <rect x=\"32.68\" y=\"12.22\" width=\"7.81\" height=\"42.56\" rx=\"3.91\" ry=\"3.91\" fill=\"#fff\" stroke=\"#fff\"\n          stroke-miterlimit=\"10\" stroke-width=\".85884477\"></rect>\n</svg>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/phrase-card/phrase-card.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/phrase-card/phrase-card.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<div fxFlex=\"grow\" fxLayout=\"column\" >\n  <div  fxLayout=\"column\" fxLayoutAlign=\"space-between\" fxLayoutGap=\"2%\" fxFlex=\"75\" class=\"full-width\"  >\n\n    <!--<svg  preserveAspectRatio=\"xMidYMid\" fxFlex=\"20\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 340.7 54\">\n      <text transform=\"matrix(.9 0 0 1 62.693 37.365)\" [attr.font-size]=\"card.level === 1 ? 28 : 35\"  fill=\"#00b6b5\">\n        {{card.cardName | uppercase}}\n      </text></svg>-->\n    <div fxFlex=\"20\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\n      <svg width=\"10%\" height=\"80%\">\n        <path class=\"st0\"\n              d=\"M54.1 48.2l-19.6-9.8-18.7-.2c-1.7 0-3.2-.7-4.4-1.9-1.2-1.2-1.8-2.8-1.8-4.5L9.8 14c0-3.4 2.8-6.2 6.3-6.2l34.1.3c3.5 0 6.3 2.9 6.2 6.3l-.2 17.8c0 3.4-2.8 6.2-6.3 6.2H48l6.1 9.8zM16.7 11c-2 0-3.6 1.6-3.6 3.6l-.2 17.1c0 1 .4 1.9 1 2.6.7.7 1.6 1.1 2.5 1.1l18.6.1 11.6 5.7-3.6-5.7 6.2.1c2 0 3.6-1.6 3.6-3.6l.1-17c0-2-1.6-3.6-3.6-3.6L16.7 11z\"></path>\n        <path class=\"st0\" d=\"M16.2 31.1c-.5 0-1-.4-1-.9V14.4c0-.5.4-.9 1-.9.5 0 1 .4 1 .9v15.8c0 .5-.4.9-1 .9z\"></path>\n\n      </svg>\n      <div class=\"title nunito\"  [minFontSize]=\"22\" [maxFontSize]=\"40\" fittext>\n        {{card.cardName | uppercase}}\n      </div>\n    </div>\n    <div fxFlex=\"80\" fxLayout=\"column\" fxLayoutAlign=\"start\" fxLayoutGap=\"5%\" class=\"app-card-texts\" [ngClass]=\"'text-level-' + card.level\">\n\n      <div fxLayout=\"column\" fxLayoutGap=\"1%\">\n        <span class=\"phrase-title\" *ngIf=\"card.level !== 1\">¿Qué signfica?</span>\n\n        <span class=\"phrase-text\" *ngFor=\"let line of card.inititalTexts, let i = index\">{{line}}</span>\n      </div>\n      <div fxFlex=\"grow\" class=\"full-width\" *ngIf=\"cardService.shoeExtraTexts.value\" fxLayout=\"column\" fxLayoutGap=\"1%\" fxLayoutAlign=\"start start\" >\n        <span *ngIf=\"card.extraTexts.length > 1\" class=\"option-text-title\" style=\"font-weight: 600; text-decoration: underline; font-size: 25px;\">Opciones:</span>\n        <span *ngIf=\"card.extraTexts.length === 1\" class=\"option-text-title\" style=\"font-weight: 600; text-decoration: underline; font-size: 25px;\">Ayuda:</span>\n        <div class=\"full-width\" fxLayout=\"column\" fxLayoutGap=\"1%\" fxLayoutAlign=\"start start\">\n          <span *ngFor=\"let extra of card.extraTexts\" class=\"option-text\" >- {{extra}}</span>\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n  <div fxLayout=\"row\" fxFlex=\"25\" fxLayoutAlign=\"start\" [fxLayoutGap]=\"card.level === 3 ? '2.5%' : '5%'\">\n    <app-answer-button [cardColor]=\"'#86dfd8'\" [answer]=\"cardService.currentCard.answer\" [fxFlex]=\"card.level === 3 ? '60' : '50'\"></app-answer-button>\n    <ng-container *ngIf=\"cardService.currentAnswerIsViewed\">\n      <app-correct-button (click)=\"correctAnswer()\" fxFlex=\"10\"></app-correct-button>\n      <app-options-button (click)=\"onClickOptions()\" *ngIf=\"!cardService.shoeExtraTexts.value &&\n                cardService.currentCard.extraTexts && cardService.currentCard.extraTexts.length > 0\" fxFlex=\"25\"></app-options-button>\n      <app-wrong-button (click)=\"wrongAnswer()\" *ngIf=\"cardService.shoeExtraTexts.value\" fxFlex=\"10\"></app-wrong-button>\n    </ng-container>\n\n  </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/riddle-card/riddle-card.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/riddle-card/riddle-card.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div fxFlex=\"grow\" fxLayout=\"column\" xmlns:xhtml=\"http://www.w3.org/1999/xhtml\">\n  <div  fxLayout=\"column\" fxLayoutAlign=\"space-between\" fxFlex=\"75\" class=\"full-width\"  >\n      <div fxFlex=\"20\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\n          <svg width=\"10%\" height=\"80%\">\n              <path class=\"st3\"\n                    d=\"M33 16.6c0-1.5.6-3 1.6-4.5s2.4-2.7 4.3-3.7 4-1.4 6.5-1.3c2.3.1 4.3.5 6 1.4 1.7.9 3 2.1 4 3.6.9 1.5 1.3 3.1 1.3 4.8 0 1.3-.3 2.5-.9 3.5-.6 1-1.2 1.8-2 2.6-.8.7-2.1 1.9-4.1 3.6-.5.5-1 .9-1.3 1.3s-.6.7-.7 1c-.2.3-.3.6-.4.9-.1.3-.2.8-.4 1.6-.3 1.6-1.3 2.4-2.9 2.4-.8 0-1.5-.3-2-.9-.5-.6-.8-1.4-.8-2.4 0-1.3.3-2.5.7-3.4.4-1 1-1.8 1.7-2.5s1.6-1.6 2.8-2.5c1-.9 1.8-1.5 2.2-1.9.5-.4.9-.9 1.2-1.5.3-.5.5-1.1.5-1.8 0-1.3-.4-2.3-1.3-3.2-.9-.9-2.1-1.4-3.6-1.4-1.7 0-3 .4-3.9 1.2-.8.9-1.6 2.1-2.2 3.8-.6 1.8-1.6 2.6-3.1 2.6-.9 0-1.6-.4-2.2-1-.8-.9-1.1-1.6-1-2.3zm10.8 26.2c-1 0-1.8-.4-2.5-1s-1-1.5-1-2.6c0-1 .4-1.8 1.1-2.5.7-.7 1.6-1 2.6-.9 1 0 1.8.4 2.5 1.1.7.7 1 1.5.9 2.5 0 1.1-.4 2-1.1 2.6-.8.6-1.6.8-2.5.8zM33.4 40.8c-.2 1.5-.9 2.9-2 4.3-1.2 1.4-2.7 2.5-4.7 3.2-2 .8-4.2 1-6.6.7-2.3-.3-4.2-1-5.8-2-1.6-1.1-2.8-2.4-3.6-3.9-.8-1.6-1-3.2-.8-4.9.2-1.3.6-2.5 1.3-3.4.7-.9 1.4-1.7 2.3-2.3.8-.6 2.3-1.7 4.5-3.1.6-.4 1.1-.8 1.4-1.1.4-.3.6-.6.8-.9.2-.3.4-.6.5-.9.1-.3.3-.8.6-1.6.5-1.6 1.5-2.3 3.1-2.1.8.1 1.5.5 1.9 1.1.5.6.7 1.4.5 2.5-.2 1.3-.5 2.4-1.1 3.3-.5.9-1.2 1.7-1.9 2.3-.8.6-1.8 1.4-3.1 2.2-1.1.7-1.9 1.3-2.4 1.7-.5.4-.9.8-1.3 1.3s-.6 1.1-.7 1.7c-.2 1.2.2 2.4 1 3.3.8 1 2 1.6 3.4 1.8 1.7.2 3 0 4-.8.9-.8 1.8-1.9 2.6-3.6.8-1.7 1.9-2.4 3.4-2.3.9.1 1.6.5 2.1 1.2.5.9.7 1.6.6 2.3zm-8-27.2c1 .1 1.7.5 2.4 1.3.6.7.9 1.6.7 2.7-.1 1-.6 1.8-1.4 2.3-.8.6-1.7.8-2.7.7-1-.1-1.8-.6-2.4-1.3-.6-.8-.8-1.6-.7-2.6.1-1.1.6-1.9 1.4-2.4.9-.6 1.7-.8 2.7-.7z\"></path>\n          </svg>\n          <div class=\"title nunito\"  [minFontSize]=\"22\" [maxFontSize]=\"45\" fittext>\n              {{card.cardName | uppercase}}\n          </div>\n      </div>\n   <!-- <svg fxFlex=\"20\" preserveAspectRatio=\"xMidYMid\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"9 0 292 57.8\">\n      <text transform=\"matrix(.95 0 0 1 62.627 45.365)\" font-size=\"36\"  fill=\"#f7941d\">\n      </text>\n    </svg>-->\n\n    <div fxFlexOffset=\"2\" fxFlex=\"80\" fxLayout=\"column\" fxLayoutAlign=\"start\" fxLayoutGap=\"1%\" class=\"app-card-texts\">\n        <i [ngStyle]=\"{'font-size': card.level === 3 ? '24px' : '26px'}\" *ngIf=\"card.hint && card.hint.length > 0\" class=\"riddle-hint\">\n            {{card.hint }}\n        </i>\n      <span [ngStyle]=\"{'font-size': card.level === 3 ? (line.length > 30 ? '22px' : '24px') : '26px'}\" *ngFor=\"let line of card.inititalTexts, let i = index\" class=\"riddle-text\">\n              {{line}}\n            </span>\n      <div  fxFlex=\"grow\" *ngIf=\"cardService.shoeExtraTexts.value\" class=\"full-width\"  fxLayout=\"column\" fxLayoutAlign=\"center\" fxLayoutGap=\"1%\" >\n          <span *ngIf=\"card.extraTexts.length > 1\" class=\"option-text\" style=\"font-weight: 600; text-decoration: underline; font-size: 25px;\">Opciones:</span>\n          <span *ngIf=\"card.extraTexts.length === 1\" class=\"option-text\" style=\"font-weight: 600; text-decoration: underline; font-size: 25px;\">Ayuda:</span>\n          <div class=\"full-width\" fxLayoutAlign=\"space-evenly start\" fxLayout=\"row\">\n              <span *ngFor=\"let extra of card.extraTexts\" class=\"option-text\" [ngStyle]=\"{'font-size': card.level === 3 && extra.length > 12 ? (extra.length > 20 ? '16px' : '18px') : '22px'}\">{{extra}}</span>\n          </div>\n\n      </div>\n    </div>\n  </div>\n  <div fxLayout=\"row\" fxFlex=\"25\" fxLayoutAlign=\"start\" [fxLayoutGap]=\"card.level === 3 ? '2.5%' : '5%'\">\n    <app-answer-button [cardColor]=\"'#f7941d'\" [answer]=\"cardService.currentCard.answer\" [fxFlex]=\"card.level === 3 ? '60' : '50'\"></app-answer-button>\n      <ng-container *ngIf=\"cardService.currentAnswerIsViewed\">\n          <app-correct-button (click)=\"correctAnswer()\" fxFlex=\"10\"></app-correct-button>\n          <app-options-button (click)=\"onClickOptions()\" *ngIf=\"!cardService.shoeExtraTexts.value &&\n                cardService.currentCard.extraTexts && cardService.currentCard.extraTexts.length > 0\" fxFlex=\"25\"></app-options-button>\n          <app-wrong-button (click)=\"wrongAnswer()\" *ngIf=\"cardService.shoeExtraTexts.value\" fxFlex=\"10\"></app-wrong-button>\n      </ng-container>\n\n  </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/spelling-card/spelling-card.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/spelling-card/spelling-card.component.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div fxFlex=\"grow\" fxLayout=\"column\" >\n  <div  fxLayout=\"column\" fxLayoutAlign=\"space-between\" fxFlex=\"75\" class=\"full-width\"  >\n\n    <!--<svg preserveAspectRatio=\"xMidYMid\" fxFlex=\"20\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 260 57.8\">\n      <text transform=\"matrix(.95 0 0 1 63.662 39.999)\" font-size=\"36\" fill=\"#00aeef\">\n        {{card.cardName | uppercase}}\n      </text>\n      <path class=\"st3\"\n            d=\"M27.8 28.8l-1-2.8h-8.2l-1 2.8c-.4 1.1-.7 1.9-1 2.2-.3.4-.7.6-1.3.6-.5 0-1-.2-1.4-.6-.4-.4-.6-.9-.6-1.4 0-.3 0-.6.1-.9.1-.3.2-.8.5-1.3L19 13c.1-.4.3-.9.5-1.5s.4-1.1.7-1.4c.2-.4.5-.7.9-.9.4-.2.8-.4 1.4-.4.6 0 1 .1 1.4.4.4.2.7.5.9.9l.6 1.2c.2.4.4 1 .6 1.7l5.3 14.3c.4 1.1.6 1.9.6 2.4s-.2 1-.6 1.4c-.4.4-.9.6-1.4.6-.3 0-.6-.1-.8-.2-.2-.1-.4-.3-.6-.5-.2-.2-.3-.5-.5-1 .1-.4 0-.8-.2-1.2zm-8-6.1h6l-3-9.1-3 9.1zM54.3 41.2v-.5c-.4.5-.8 1-1.2 1.4-.4.4-.9.6-1.4.8-.5.2-1.1.3-1.8.3-.8 0-1.5-.2-2.1-.5-.6-.4-1.1-.9-1.5-1.5-.4-.8-.6-1.9-.6-3.3v-7.1c0-.7.1-1.3.4-1.6.3-.4.7-.5 1.2-.5s.9.2 1.2.5c.3.4.5.9.5 1.6v5.8c0 .8.1 1.5.2 2.1.1.6.4 1 .7 1.3.3.3.8.5 1.4.5.6 0 1.1-.2 1.6-.5.5-.4.8-.8 1.1-1.4.2-.5.3-1.6.3-3.4v-4.4c0-.7.2-1.2.5-1.6.3-.4.7-.5 1.2-.5s.9.2 1.2.5c.3.4.4.9.4 1.6v10.4c0 .7-.1 1.2-.4 1.5-.3.3-.7.5-1.1.5-.4 0-.8-.2-1.1-.5-.5-.4-.7-.9-.7-1.5z\"></path>\n      <path class=\"st3\"\n            d=\"M42.8 24.6L35.9 33h7.3c.6 0 1 .2 1.3.5.3.3.5.7.5 1.2s-.2.8-.4 1.1c-.3.3-.7.4-1.4.4H33.1c-.7 0-1.2-.2-1.6-.5-.4-.3-.5-.8-.5-1.4 0-.4.1-.7.4-1.1.2-.4.8-1 1.5-2 .8-1 1.6-1.9 2.2-2.7.7-.8 1.3-1.6 1.9-2.3.6-.7 1.1-1.3 1.4-1.8.4-.5.7-.9.9-1.3h-5.6c-.8 0-1.4-.1-1.7-.2-.4-.2-.6-.5-.6-1.2 0-.5.1-.8.4-1.1.3-.3.7-.4 1.3-.4h8.6c.8 0 1.4.1 1.8.4.4.3.6.7.6 1.4 0 .2 0 .4-.1.7l-.3.6c-.1.2-.2.3-.4.6-.1.2-.3.4-.5.7z\"></path>\n      <path class=\"st3\"\n            d=\"M50 17.2h-7.3c0 .9.2 1.8.5 2.5s.8 1.3 1.3 1.6c.6.4 1.2.5 1.8.5.4 0 .9-.1 1.2-.2.4-.1.7-.3 1.1-.5.3-.2.7-.5 1-.8l1.1-1.1c.2-.2.5-.3.8-.3.4 0 .7.1.9.3.2.2.4.6.4 1s-.1.8-.4 1.3-.7 1-1.2 1.4c-.5.5-1.2.8-2 1.1-.8.3-1.7.5-2.8.5-2.4 0-4.3-.8-5.6-2.3-1.3-1.5-2-3.6-2-6.1 0-1.2.2-2.3.5-3.4.3-1 .8-1.9 1.4-2.7.6-.7 1.4-1.3 2.3-1.7 1-.3 2-.5 3.2-.5 1.5 0 2.7.3 3.8 1 1 .7 1.8 1.6 2.4 2.6.5 1.1.8 2.2.8 3.3 0 1-.3 1.7-.8 2-.7.3-1.4.5-2.4.5zm-7.3-2.4h6.8c-.1-1.4-.4-2.5-1-3.2-.6-.7-1.4-1-2.4-1-.9 0-1.7.4-2.3 1.1-.6.7-1 1.7-1.1 3.1zM22.3 42v4.6c0 .7-.2 1.3-.5 1.6-.3.4-.8.5-1.3.5s-.9-.2-1.3-.5c-.3-.4-.5-.9-.5-1.6v-5.5c0-.9 0-1.5-.1-2s-.2-.9-.5-1.2c-.3-.3-.7-.5-1.2-.5-1.1 0-1.9.4-2.2 1.2-.4.8-.5 1.9-.5 3.3v4.6c0 .7-.2 1.2-.5 1.6-.3.4-.7.5-1.3.5-.5 0-.9-.2-1.3-.5-.3-.4-.5-.9-.5-1.6v-9.8c0-.6.1-1.1.4-1.5.3-.3.7-.5 1.2-.5s.9.2 1.2.5c.3.3.5.7.5 1.3v.3c.6-.7 1.2-1.2 1.9-1.6.7-.3 1.4-.5 2.3-.5.9 0 1.6.2 2.2.5.6.3 1.1.9 1.5 1.6.6-.7 1.2-1.2 1.9-1.6.7-.3 1.4-.5 2.2-.5.9 0 1.7.2 2.4.6.7.4 1.2.9 1.5 1.6.3.6.4 1.6.4 2.9v6.7c0 .7-.2 1.3-.5 1.6-.3.4-.8.5-1.3.5s-.9-.2-1.3-.5c-.3-.4-.5-.9-.5-1.6v-5.8c0-.7 0-1.3-.1-1.8-.1-.4-.2-.8-.5-1.1-.3-.3-.7-.5-1.3-.5-.5 0-.9.1-1.3.4-.4.3-.7.6-.9 1.1-.1.7-.2 1.7-.2 3.2z\"></path>\n    </svg>-->\n    <div fxFlex=\"20\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\n      <svg width=\"10%\" height=\"80%\">\n        <path class=\"st3\"\n              d=\"M27.8 28.8l-1-2.8h-8.2l-1 2.8c-.4 1.1-.7 1.9-1 2.2-.3.4-.7.6-1.3.6-.5 0-1-.2-1.4-.6-.4-.4-.6-.9-.6-1.4 0-.3 0-.6.1-.9.1-.3.2-.8.5-1.3L19 13c.1-.4.3-.9.5-1.5s.4-1.1.7-1.4c.2-.4.5-.7.9-.9.4-.2.8-.4 1.4-.4.6 0 1 .1 1.4.4.4.2.7.5.9.9l.6 1.2c.2.4.4 1 .6 1.7l5.3 14.3c.4 1.1.6 1.9.6 2.4s-.2 1-.6 1.4c-.4.4-.9.6-1.4.6-.3 0-.6-.1-.8-.2-.2-.1-.4-.3-.6-.5-.2-.2-.3-.5-.5-1 .1-.4 0-.8-.2-1.2zm-8-6.1h6l-3-9.1-3 9.1zM54.3 41.2v-.5c-.4.5-.8 1-1.2 1.4-.4.4-.9.6-1.4.8-.5.2-1.1.3-1.8.3-.8 0-1.5-.2-2.1-.5-.6-.4-1.1-.9-1.5-1.5-.4-.8-.6-1.9-.6-3.3v-7.1c0-.7.1-1.3.4-1.6.3-.4.7-.5 1.2-.5s.9.2 1.2.5c.3.4.5.9.5 1.6v5.8c0 .8.1 1.5.2 2.1.1.6.4 1 .7 1.3.3.3.8.5 1.4.5.6 0 1.1-.2 1.6-.5.5-.4.8-.8 1.1-1.4.2-.5.3-1.6.3-3.4v-4.4c0-.7.2-1.2.5-1.6.3-.4.7-.5 1.2-.5s.9.2 1.2.5c.3.4.4.9.4 1.6v10.4c0 .7-.1 1.2-.4 1.5-.3.3-.7.5-1.1.5-.4 0-.8-.2-1.1-.5-.5-.4-.7-.9-.7-1.5z\"></path>\n        <path class=\"st3\"\n              d=\"M42.8 24.6L35.9 33h7.3c.6 0 1 .2 1.3.5.3.3.5.7.5 1.2s-.2.8-.4 1.1c-.3.3-.7.4-1.4.4H33.1c-.7 0-1.2-.2-1.6-.5-.4-.3-.5-.8-.5-1.4 0-.4.1-.7.4-1.1.2-.4.8-1 1.5-2 .8-1 1.6-1.9 2.2-2.7.7-.8 1.3-1.6 1.9-2.3.6-.7 1.1-1.3 1.4-1.8.4-.5.7-.9.9-1.3h-5.6c-.8 0-1.4-.1-1.7-.2-.4-.2-.6-.5-.6-1.2 0-.5.1-.8.4-1.1.3-.3.7-.4 1.3-.4h8.6c.8 0 1.4.1 1.8.4.4.3.6.7.6 1.4 0 .2 0 .4-.1.7l-.3.6c-.1.2-.2.3-.4.6-.1.2-.3.4-.5.7z\"></path>\n        <path class=\"st3\"\n              d=\"M50 17.2h-7.3c0 .9.2 1.8.5 2.5s.8 1.3 1.3 1.6c.6.4 1.2.5 1.8.5.4 0 .9-.1 1.2-.2.4-.1.7-.3 1.1-.5.3-.2.7-.5 1-.8l1.1-1.1c.2-.2.5-.3.8-.3.4 0 .7.1.9.3.2.2.4.6.4 1s-.1.8-.4 1.3-.7 1-1.2 1.4c-.5.5-1.2.8-2 1.1-.8.3-1.7.5-2.8.5-2.4 0-4.3-.8-5.6-2.3-1.3-1.5-2-3.6-2-6.1 0-1.2.2-2.3.5-3.4.3-1 .8-1.9 1.4-2.7.6-.7 1.4-1.3 2.3-1.7 1-.3 2-.5 3.2-.5 1.5 0 2.7.3 3.8 1 1 .7 1.8 1.6 2.4 2.6.5 1.1.8 2.2.8 3.3 0 1-.3 1.7-.8 2-.7.3-1.4.5-2.4.5zm-7.3-2.4h6.8c-.1-1.4-.4-2.5-1-3.2-.6-.7-1.4-1-2.4-1-.9 0-1.7.4-2.3 1.1-.6.7-1 1.7-1.1 3.1zM22.3 42v4.6c0 .7-.2 1.3-.5 1.6-.3.4-.8.5-1.3.5s-.9-.2-1.3-.5c-.3-.4-.5-.9-.5-1.6v-5.5c0-.9 0-1.5-.1-2s-.2-.9-.5-1.2c-.3-.3-.7-.5-1.2-.5-1.1 0-1.9.4-2.2 1.2-.4.8-.5 1.9-.5 3.3v4.6c0 .7-.2 1.2-.5 1.6-.3.4-.7.5-1.3.5-.5 0-.9-.2-1.3-.5-.3-.4-.5-.9-.5-1.6v-9.8c0-.6.1-1.1.4-1.5.3-.3.7-.5 1.2-.5s.9.2 1.2.5c.3.3.5.7.5 1.3v.3c.6-.7 1.2-1.2 1.9-1.6.7-.3 1.4-.5 2.3-.5.9 0 1.6.2 2.2.5.6.3 1.1.9 1.5 1.6.6-.7 1.2-1.2 1.9-1.6.7-.3 1.4-.5 2.2-.5.9 0 1.7.2 2.4.6.7.4 1.2.9 1.5 1.6.3.6.4 1.6.4 2.9v6.7c0 .7-.2 1.3-.5 1.6-.3.4-.8.5-1.3.5s-.9-.2-1.3-.5c-.3-.4-.5-.9-.5-1.6v-5.8c0-.7 0-1.3-.1-1.8-.1-.4-.2-.8-.5-1.1-.3-.3-.7-.5-1.3-.5-.5 0-.9.1-1.3.4-.4.3-.7.6-.9 1.1-.1.7-.2 1.7-.2 3.2z\"></path>\n\n      </svg>\n      <div class=\"title nunito\"  [minFontSize]=\"22\" [maxFontSize]=\"45\" fittext>\n        {{card.cardName | uppercase}}\n      </div>\n    </div>\n\n\n    <div fxFlexOffset=\"5\" fxFlex=\"80\" fxLayout=\"column\" fxLayoutAlign=\"start\" fxLayoutGap=\"5%\" class=\"app-card-texts\">\n      <span class=\"statement\">Deletrear la siguiente palabra:</span>\n      <span class=\"spelling-text\">{{card.inititalTexts[currentWordIndex]}}</span>\n    </div>\n  </div>\n  <div fxLayout=\"row\" fxFlex=\"25\" fxLayoutAlign=\"start\" fxLayoutGap=\"5%\">\n    <app-correct-button fxFlexOffset=\"65\" (click)=\"correctAnswer()\" fxFlex=\"10\"></app-correct-button>\n    <app-wrong-button (click)=\"wrongAnswer()\" fxFlex=\"10\"></app-wrong-button>\n  </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/wrong-button/wrong-button.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/wrong-button/wrong-button.component.html ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div fxFill fxLayout=\"row\" fxLayoutAlign=\"center center\">\n\n    <svg class=\"full-width\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"10 10 75 75\">\n        <style>.st2 {\n            fill: #005798\n        }</style>\n        <path d=\"M62.6 81.8H30.1c-9.4 0-17-7.7-17-17V32.3c0-9.4 7.7-17 17-17h32.5c9.4 0 17 7.7 17 17v32.5c0 9.4-7.6 17-17 17z\"\n              fill=\"#d1d3d4\"></path>\n        <path d=\"M64.5 79H31.9c-9.4 0-17-7.7-17-17V29.5c0-9.4 7.7-17 17-17h32.5c9.4 0 17 7.7 17 17V62c.1 9.4-7.6 17-16.9 17z\"\n              fill=\"#006ab6\"></path>\n        <path class=\"st2\"\n              d=\"M58.8 68.2c-.8 0-1.4-.1-2-.4-.5-.3-1-.6-1.3-1.1-.4-.5-1.1-1.4-2.1-2.8L45 52.1l-8.9 12.1c-.7.9-1.1 1.6-1.4 1.9-.3.3-.6.7-.9 1-.3.3-.8.6-1.2.7-.5.2-1 .3-1.7.3-1 0-1.8-.3-2.4-.8-.6-.5-.9-1.3-.9-2.3 0-1.2.7-2.8 2.1-4.7L40 46.7l-8.7-12.1c-.8-1.1-1.4-2.1-1.8-2.9-.4-.7-.6-1.5-.6-2.1 0-.6.3-1.2 1-1.7.7-.6 1.5-.8 2.5-.8 1.1 0 2 .3 2.6.9.7.6 1.7 1.9 2.9 3.7l7.2 10.4L53 31.7l1.5-2.1c.4-.6.8-1 1.2-1.4.3-.3.7-.6 1.1-.8.4-.2.9-.3 1.5-.3 1 0 1.9.3 2.5.8.6.5.9 1.1.9 1.8 0 1.1-.8 2.7-2.3 4.7l-9.1 12.3 9.8 13.6c.8 1.1 1.5 2.1 1.8 2.8.4.7.5 1.3.5 1.9 0 .6-.1 1-.4 1.5-.3.5-.7.8-1.3 1.1-.5.4-1.1.6-1.9.6z\"></path>\n        <path class=\"st2\"\n              d=\"M58.3 27.7c.9 0 1.6.2 2.2.7.5.4.7.9.7 1.4 0 .7-.4 2-2.2 4.4l-8.9 12-.4.6.4.6 9.6 13.3c.8 1.1 1.4 2 1.8 2.8.3.6.5 1.2.5 1.7s-.1.9-.4 1.2c-.3.4-.6.7-1.1.9-.5.2-1.1.4-1.7.4-.7 0-1.3-.1-1.7-.4-.5-.3-.9-.6-1.1-.9-.4-.5-1.1-1.4-2-2.8l-8-11.2-.8-1.1-.8 1.1L35.9 64c-.7.9-1.1 1.6-1.4 1.9-.2.3-.5.6-.9.9-.3.3-.7.5-1.1.6-.4.2-.9.2-1.5.2-.9 0-1.6-.2-2.1-.7-.5-.4-.7-1-.7-1.9 0-1.1.7-2.6 2-4.4l10.1-13.3.4-.6-.4-.6-8.5-11.8c-.8-1.1-1.3-2-1.7-2.8-.3-.7-.5-1.3-.5-1.9 0-.2 0-.7.8-1.3.6-.5 1.3-.7 2.2-.7 1 0 1.8.2 2.3.7.7.6 1.6 1.8 2.8 3.6l6.8 9.9.8 1.2.8-1.1 7.3-9.9c.6-.8 1.1-1.5 1.5-2.1.4-.5.8-1 1.1-1.3.3-.3.6-.5 1-.7.4-.2.8-.2 1.3-.2m0-1c-.6 0-1.2.1-1.7.3-.5.2-.9.5-1.3.9s-.8.9-1.2 1.5c-.4.6-.9 1.3-1.5 2.1l-7.2 9.9-6.8-9.9c-1.3-1.8-2.2-3.1-2.9-3.8-.7-.7-1.7-1-3-1-1.1 0-2.1.3-2.8.9-.8.6-1.2 1.3-1.2 2.1s.2 1.5.6 2.4c.4.8 1 1.8 1.8 3l8.5 11.8-10.2 13.2c-1.5 2-2.3 3.6-2.3 5 0 1.2.4 2 1.1 2.7.7.6 1.7.9 2.8.9.7 0 1.3-.1 1.9-.3.5-.2 1-.5 1.4-.8.4-.4.7-.7 1-1.1.3-.4.7-1 1.4-1.9L45.2 53l8 11.2c1 1.4 1.7 2.3 2.1 2.8.4.5.9.9 1.4 1.2.6.3 1.3.5 2.2.5.8 0 1.5-.2 2.2-.5.6-.3 1.1-.7 1.5-1.3.3-.5.5-1.1.5-1.8s-.2-1.4-.6-2.2c-.4-.8-1-1.7-1.9-2.9L51 46.7l8.9-12c1.6-2.1 2.4-3.8 2.4-5 0-.8-.4-1.6-1.1-2.2-.8-.5-1.7-.8-2.9-.8z\"></path>\n        <g>\n            <path d=\"M32.4 60.1l10.1-13.3L34 35c-.8-1.1-1.4-2.1-1.8-3-.4-.8-.6-1.6-.6-2.4 0-.8.4-1.5 1.2-2.1.8-.6 1.7-.9 2.8-.9 1.3 0 2.3.3 3 1 .7.7 1.7 1.9 2.9 3.8l6.8 9.9 7.2-9.9c.6-.8 1.1-1.5 1.5-2.1.4-.6.8-1.1 1.2-1.5.4-.4.8-.7 1.3-.9.5-.2 1-.3 1.7-.3 1.2 0 2.1.3 2.8.9.7.6 1.1 1.4 1.1 2.2 0 1.2-.8 2.9-2.4 5l-8.9 12L63.4 60c.9 1.2 1.5 2.1 1.9 2.9.4.8.6 1.5.6 2.2 0 .6-.2 1.2-.5 1.8-.4.5-.8 1-1.5 1.3-.6.3-1.4.5-2.2.5-.9 0-1.6-.2-2.2-.5-.6-.3-1.1-.7-1.4-1.2-.4-.5-1.1-1.4-2.1-2.8L48 53l-8.5 11.6c-.7.9-1.1 1.6-1.4 1.9-.3.4-.6.7-1 1.1-.4.4-.9.6-1.4.8-.5.2-1.2.3-1.9.3-1.1 0-2-.3-2.8-.9-.7-.6-1.1-1.5-1.1-2.7.3-1.4 1-3 2.5-5z\"\n                  fill=\"#fff\" stroke=\"#fff\" stroke-width=\"1.692\" stroke-miterlimit=\"10\"></path>\n        </g>\n    </svg>\n</div>\n");

/***/ }),

/***/ "./src/app/cards/cards.module.ts":
/*!***************************************!*\
  !*** ./src/app/cards/cards.module.ts ***!
  \***************************************/
/*! exports provided: CardsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardsModule", function() { return CardsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _components_card_header_card_header_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/card-header/card-header.component */ "./src/app/cards/components/card-header/card-header.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _components_character_action_character_action_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/character-action/character-action.component */ "./src/app/cards/components/character-action/character-action.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_spelling_card_spelling_card_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/spelling-card/spelling-card.component */ "./src/app/cards/components/spelling-card/spelling-card.component.ts");
/* harmony import */ var _components_riddle_card_riddle_card_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/riddle-card/riddle-card.component */ "./src/app/cards/components/riddle-card/riddle-card.component.ts");
/* harmony import */ var _components_guessword_card_guessword_card_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/guessword-card/guessword-card.component */ "./src/app/cards/components/guessword-card/guessword-card.component.ts");
/* harmony import */ var _components_card_view_card_view_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/card-view/card-view.component */ "./src/app/cards/components/card-view/card-view.component.ts");
/* harmony import */ var _components_wrong_button_wrong_button_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/wrong-button/wrong-button.component */ "./src/app/cards/components/wrong-button/wrong-button.component.ts");
/* harmony import */ var _components_correct_button_correct_button_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/correct-button/correct-button.component */ "./src/app/cards/components/correct-button/correct-button.component.ts");
/* harmony import */ var _components_answer_button_answer_button_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/answer-button/answer-button.component */ "./src/app/cards/components/answer-button/answer-button.component.ts");
/* harmony import */ var _components_options_button_options_button_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/options-button/options-button.component */ "./src/app/cards/components/options-button/options-button.component.ts");
/* harmony import */ var _components_phrase_card_phrase_card_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/phrase-card/phrase-card.component */ "./src/app/cards/components/phrase-card/phrase-card.component.ts");
/* harmony import */ var _directives_card_directive__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./directives/card.directive */ "./src/app/cards/directives/card.directive.ts");
/* harmony import */ var angular_fittext__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! angular-fittext */ "./node_modules/angular-fittext/fesm5/angular-fittext.js");
/* harmony import */ var _guards_exit_from_card_guard__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../guards/exit-from-card.guard */ "./src/app/guards/exit-from-card.guard.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

















var CardsModule = /** @class */ (function () {
    function CardsModule() {
    }
    CardsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_components_card_header_card_header_component__WEBPACK_IMPORTED_MODULE_1__["CardHeaderComponent"], _components_character_action_character_action_component__WEBPACK_IMPORTED_MODULE_3__["CharacterActionComponent"], _components_spelling_card_spelling_card_component__WEBPACK_IMPORTED_MODULE_5__["SpellingCardComponent"], _components_riddle_card_riddle_card_component__WEBPACK_IMPORTED_MODULE_6__["RiddleCardComponent"],
                _components_guessword_card_guessword_card_component__WEBPACK_IMPORTED_MODULE_7__["GuesswordCardComponent"], _components_card_view_card_view_component__WEBPACK_IMPORTED_MODULE_8__["CardViewComponent"], _components_wrong_button_wrong_button_component__WEBPACK_IMPORTED_MODULE_9__["WrongButtonComponent"], _components_correct_button_correct_button_component__WEBPACK_IMPORTED_MODULE_10__["CorrectButtonComponent"], _components_answer_button_answer_button_component__WEBPACK_IMPORTED_MODULE_11__["AnswerButtonComponent"], _components_options_button_options_button_component__WEBPACK_IMPORTED_MODULE_12__["OptionsButtonComponent"], _components_phrase_card_phrase_card_component__WEBPACK_IMPORTED_MODULE_13__["PhraseCardComponent"], _directives_card_directive__WEBPACK_IMPORTED_MODULE_14__["CardDirective"]],
            imports: [
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_2__["SharedModule"],
                angular_fittext__WEBPACK_IMPORTED_MODULE_15__["AngularFittextModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _components_card_view_card_view_component__WEBPACK_IMPORTED_MODULE_8__["CardViewComponent"],
                        canDeactivate: [_guards_exit_from_card_guard__WEBPACK_IMPORTED_MODULE_16__["ExitFromCardGuard"]],
                        children: [
                            {
                                path: 'riddle',
                                component: _components_riddle_card_riddle_card_component__WEBPACK_IMPORTED_MODULE_6__["RiddleCardComponent"]
                            },
                            {
                                path: 'spelling',
                                component: _components_spelling_card_spelling_card_component__WEBPACK_IMPORTED_MODULE_5__["SpellingCardComponent"]
                            },
                            {
                                path: 'phrase',
                                component: _components_phrase_card_phrase_card_component__WEBPACK_IMPORTED_MODULE_13__["PhraseCardComponent"]
                            },
                            {
                                path: 'guessword',
                                component: _components_guessword_card_guessword_card_component__WEBPACK_IMPORTED_MODULE_7__["GuesswordCardComponent"]
                            }
                        ]
                    }
                ])
            ]
        })
    ], CardsModule);
    return CardsModule;
}());



/***/ }),

/***/ "./src/app/cards/components/answer-button/answer-button.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/cards/components/answer-button/answer-button.component.scss ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".answer-text-height {\n  min-height: 100%;\n  vertical-align: middle;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.answer-text {\n  color: white;\n  font-weight: 500;\n  text-align: center;\n  padding-left: 2%;\n  padding-right: 2%;\n}\n\n.st0 {\n  fill: #6d6e71;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FyZHMvY29tcG9uZW50cy9hbnN3ZXItYnV0dG9uL0M6XFxVc2Vyc1xcaGFkYXNcXERlc2t0b3BcXG1lbnRlX2FkaXZpbmEvc3JjXFxhcHBcXGNhcmRzXFxjb21wb25lbnRzXFxhbnN3ZXItYnV0dG9uXFxhbnN3ZXItYnV0dG9uLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL2Fuc3dlci1idXR0b24vYW5zd2VyLWJ1dHRvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ0NGOztBREVBO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FDQ0Y7O0FEQ0E7RUFBSyxhQUFBO0FDR0wiLCJmaWxlIjoic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL2Fuc3dlci1idXR0b24vYW5zd2VyLWJ1dHRvbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hbnN3ZXItdGV4dC1oZWlnaHQge1xuICBtaW4taGVpZ2h0OiAxMDAlO1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcblxufVxuLmFuc3dlci10ZXh0IHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDogNTAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmctbGVmdDogMiU7XG4gIHBhZGRpbmctcmlnaHQ6IDIlO1xufVxuLnN0MHtmaWxsOiM2ZDZlNzF9XG4iLCIuYW5zd2VyLXRleHQtaGVpZ2h0IHtcbiAgbWluLWhlaWdodDogMTAwJTtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5hbnN3ZXItdGV4dCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDIlO1xuICBwYWRkaW5nLXJpZ2h0OiAyJTtcbn1cblxuLnN0MCB7XG4gIGZpbGw6ICM2ZDZlNzE7XG59Il19 */");

/***/ }),

/***/ "./src/app/cards/components/answer-button/answer-button.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/cards/components/answer-button/answer-button.component.ts ***!
  \***************************************************************************/
/*! exports provided: AnswerButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnswerButtonComponent", function() { return AnswerButtonComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/audio.service */ "./src/app/shared/services/audio.service.ts");
/* harmony import */ var _shared_services_card_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/services/card.service */ "./src/app/shared/services/card.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};



var AnswerButtonComponent = /** @class */ (function () {
    function AnswerButtonComponent(audio, card) {
        this.audio = audio;
        this.card = card;
    }
    AnswerButtonComponent.prototype.onClick = function (btn) {
        this.audio.playClick();
        this.show = !this.show;
        this.card.currentAnswerIsViewed = true;
    };
    AnswerButtonComponent.prototype.ngOnInit = function () {
        this.show = false;
    };
    AnswerButtonComponent.ctorParameters = function () { return [
        { type: _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_1__["AudioService"] },
        { type: _shared_services_card_service__WEBPACK_IMPORTED_MODULE_2__["CardService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], AnswerButtonComponent.prototype, "answer", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], AnswerButtonComponent.prototype, "cardColor", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('click', ['$event.target']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AnswerButtonComponent.prototype, "onClick", null);
    AnswerButtonComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-answer-button',
            template: __importDefault(__webpack_require__(/*! raw-loader!./answer-button.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/answer-button/answer-button.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./answer-button.component.scss */ "./src/app/cards/components/answer-button/answer-button.component.scss")).default]
        }),
        __metadata("design:paramtypes", [_shared_services_audio_service__WEBPACK_IMPORTED_MODULE_1__["AudioService"], _shared_services_card_service__WEBPACK_IMPORTED_MODULE_2__["CardService"]])
    ], AnswerButtonComponent);
    return AnswerButtonComponent;
}());



/***/ }),

/***/ "./src/app/cards/components/card-header/card-header.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/cards/components/card-header/card-header.component.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NhcmRzL2NvbXBvbmVudHMvY2FyZC1oZWFkZXIvY2FyZC1oZWFkZXIuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/cards/components/card-header/card-header.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/cards/components/card-header/card-header.component.ts ***!
  \***********************************************************************/
/*! exports provided: CardHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardHeaderComponent", function() { return CardHeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

var CardHeaderComponent = /** @class */ (function () {
    function CardHeaderComponent() {
    }
    CardHeaderComponent.prototype.ngOnInit = function () {
    };
    CardHeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-card-header',
            template: __importDefault(__webpack_require__(/*! raw-loader!./card-header.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/card-header/card-header.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./card-header.component.scss */ "./src/app/cards/components/card-header/card-header.component.scss")).default]
        }),
        __metadata("design:paramtypes", [])
    ], CardHeaderComponent);
    return CardHeaderComponent;
}());



/***/ }),

/***/ "./src/app/cards/components/card-view/card-view.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/cards/components/card-view/card-view.component.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".card-container {\n  background: url(\"/assets/images/cards/card-container.svg\") bottom center no-repeat;\n  background-size: contain;\n}\n\n.st3 {\n  fill: #6d6e71;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FyZHMvY29tcG9uZW50cy9jYXJkLXZpZXcvQzpcXFVzZXJzXFxoYWRhc1xcRGVza3RvcFxcbWVudGVfYWRpdmluYS9zcmNcXGFwcFxcY2FyZHNcXGNvbXBvbmVudHNcXGNhcmQtdmlld1xcY2FyZC12aWV3LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL2NhcmQtdmlldy9jYXJkLXZpZXcuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrRkFBQTtFQUNBLHdCQUFBO0FDQ0Y7O0FEQ0E7RUFBSyxhQUFBO0FDR0wiLCJmaWxlIjoic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL2NhcmQtdmlldy9jYXJkLXZpZXcuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZC1jb250YWluZXIge1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9jYXJkcy9jYXJkLWNvbnRhaW5lci5zdmdcIikgYm90dG9tIGNlbnRlciBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbn1cbi5zdDN7ZmlsbDojNmQ2ZTcxfVxuIiwiLmNhcmQtY29udGFpbmVyIHtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvY2FyZHMvY2FyZC1jb250YWluZXIuc3ZnXCIpIGJvdHRvbSBjZW50ZXIgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG59XG5cbi5zdDMge1xuICBmaWxsOiAjNmQ2ZTcxO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/cards/components/card-view/card-view.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/cards/components/card-view/card-view.component.ts ***!
  \*******************************************************************/
/*! exports provided: CardViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardViewComponent", function() { return CardViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_game_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/game.service */ "./src/app/shared/services/game.service.ts");
/* harmony import */ var _shared_services_card_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/services/card.service */ "./src/app/shared/services/card.service.ts");
/* harmony import */ var _directives_card_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../directives/card.directive */ "./src/app/cards/directives/card.directive.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var animejs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! animejs */ "./node_modules/animejs/lib/anime.es.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};







var CardViewComponent = /** @class */ (function () {
    function CardViewComponent(gameService, cardService) {
        this.gameService = gameService;
        this.cardService = cardService;
    }
    CardViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.starSubscription = this.cardService.playStarAnimation.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["take"])(1)).subscribe(function (value) {
            if (value === 1) {
                _this.showHalfStars = true;
            }
            else {
                _this.showAllStars = true;
            }
            Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["timer"])(100).subscribe(function (value1) {
                Object(animejs__WEBPACK_IMPORTED_MODULE_5__["default"])({
                    targets: '.stars path',
                    easing: 'linear',
                    duration: 500,
                    direction: 'alternate',
                    rotate: [0, function (el, i, l) {
                            return (Math.random() < 0.5 ? 1 : -1) * (Math.random() * 2);
                        }],
                    scale: [1, .95],
                    autoplay: true,
                    loop: 5
                });
            });
        });
    };
    CardViewComponent.prototype.canDeactivate = function () {
        return this.cardService.currentCardIsAnswered;
    };
    CardViewComponent.prototype.ngOnDestroy = function () {
        if (this.starSubscription) {
            this.starSubscription.unsubscribe();
        }
    };
    CardViewComponent.ctorParameters = function () { return [
        { type: _shared_services_game_service__WEBPACK_IMPORTED_MODULE_1__["GameService"] },
        { type: _shared_services_card_service__WEBPACK_IMPORTED_MODULE_2__["CardService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_directives_card_directive__WEBPACK_IMPORTED_MODULE_3__["CardDirective"], { static: true }),
        __metadata("design:type", _directives_card_directive__WEBPACK_IMPORTED_MODULE_3__["CardDirective"])
    ], CardViewComponent.prototype, "card", void 0);
    CardViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-card-view',
            template: __importDefault(__webpack_require__(/*! raw-loader!./card-view.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/card-view/card-view.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./card-view.component.scss */ "./src/app/cards/components/card-view/card-view.component.scss")).default]
        }),
        __metadata("design:paramtypes", [_shared_services_game_service__WEBPACK_IMPORTED_MODULE_1__["GameService"], _shared_services_card_service__WEBPACK_IMPORTED_MODULE_2__["CardService"]])
    ], CardViewComponent);
    return CardViewComponent;
}());



/***/ }),

/***/ "./src/app/cards/components/character-action/character-action.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/cards/components/character-action/character-action.component.scss ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NhcmRzL2NvbXBvbmVudHMvY2hhcmFjdGVyLWFjdGlvbi9jaGFyYWN0ZXItYWN0aW9uLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/cards/components/character-action/character-action.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/cards/components/character-action/character-action.component.ts ***!
  \*********************************************************************************/
/*! exports provided: CharacterActionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CharacterActionComponent", function() { return CharacterActionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_models_player__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/models/player */ "./src/app/shared/models/player.ts");
/* harmony import */ var _shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/services/game.service */ "./src/app/shared/services/game.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};



var CharacterActionComponent = /** @class */ (function () {
    function CharacterActionComponent(game) {
        this.game = game;
    }
    CharacterActionComponent.prototype.ngOnInit = function () {
    };
    CharacterActionComponent.ctorParameters = function () { return [
        { type: _shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _shared_models_player__WEBPACK_IMPORTED_MODULE_1__["Player"])
    ], CharacterActionComponent.prototype, "player", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], CharacterActionComponent.prototype, "color", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], CharacterActionComponent.prototype, "action", void 0);
    CharacterActionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-character-action',
            template: __importDefault(__webpack_require__(/*! raw-loader!./character-action.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/character-action/character-action.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./character-action.component.scss */ "./src/app/cards/components/character-action/character-action.component.scss")).default]
        }),
        __metadata("design:paramtypes", [_shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"]])
    ], CharacterActionComponent);
    return CharacterActionComponent;
}());



/***/ }),

/***/ "./src/app/cards/components/correct-button/correct-button.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/cards/components/correct-button/correct-button.component.scss ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NhcmRzL2NvbXBvbmVudHMvY29ycmVjdC1idXR0b24vY29ycmVjdC1idXR0b24uY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/cards/components/correct-button/correct-button.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/cards/components/correct-button/correct-button.component.ts ***!
  \*****************************************************************************/
/*! exports provided: CorrectButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CorrectButtonComponent", function() { return CorrectButtonComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

var CorrectButtonComponent = /** @class */ (function () {
    function CorrectButtonComponent() {
    }
    CorrectButtonComponent.prototype.ngOnInit = function () {
    };
    CorrectButtonComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-correct-button',
            template: __importDefault(__webpack_require__(/*! raw-loader!./correct-button.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/correct-button/correct-button.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./correct-button.component.scss */ "./src/app/cards/components/correct-button/correct-button.component.scss")).default]
        }),
        __metadata("design:paramtypes", [])
    ], CorrectButtonComponent);
    return CorrectButtonComponent;
}());



/***/ }),

/***/ "./src/app/cards/components/guessword-card/guessword-card.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/cards/components/guessword-card/guessword-card.component.scss ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  display: flex;\n  flex: 1 1 100%;\n  height: 100%;\n  margin: 0;\n  min-height: 100%;\n  min-width: 100%;\n  width: 100%;\n}\n\n.st0 {\n  fill: #f49ac1;\n}\n\n.phrase-text {\n  font-weight: 500;\n  color: #6d6e71 !important;\n}\n\n.option-text {\n  font-weight: 600;\n  color: #6d6e71 !important;\n}\n\n.app-card-texts {\n  overflow: auto;\n  max-height: 500px;\n  font-size: 23px;\n  padding-left: 3%;\n  padding-right: 3%;\n  overflow-y: hidden;\n}\n\n.title {\n  color: #EC008C;\n}\n\n.text-level-1 {\n  font-size: 28px;\n}\n\n.text-level-2 {\n  font-size: 23px;\n}\n\n.guessword-title {\n  font-weight: 600;\n  color: #6d6e71 !important;\n  text-decoration: underline;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FyZHMvY29tcG9uZW50cy9ndWVzc3dvcmQtY2FyZC9DOlxcVXNlcnNcXGhhZGFzXFxEZXNrdG9wXFxtZW50ZV9hZGl2aW5hL3NyY1xcYXBwXFxjYXJkc1xcY29tcG9uZW50c1xcZ3Vlc3N3b3JkLWNhcmRcXGd1ZXNzd29yZC1jYXJkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL2d1ZXNzd29yZC1jYXJkL2d1ZXNzd29yZC1jYXJkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL2d1ZXNzd29yZC1jYXJkL0M6XFxVc2Vyc1xcaGFkYXNcXERlc2t0b3BcXG1lbnRlX2FkaXZpbmEvc3JjXFx2YXJpYWJsZXMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNFLGFBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUFjLFNBQUE7RUFBVyxnQkFBQTtFQUFrQixlQUFBO0VBQWlCLFdBQUE7QUNJOUQ7O0FEREE7RUFBSyxhQUFBO0FDS0w7O0FESkE7RUFDRSxnQkFBQTtFQUNBLHlCQUFBO0FDT0Y7O0FETEE7RUFDRSxnQkFBQTtFQUNBLHlCQUFBO0FDUUY7O0FETEE7RUFDRSxjQUFBO0VBQWdCLGlCQUFBO0VBQW1CLGVBQUE7RUFDbkMsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDVUY7O0FEUkE7RUFDRSxjRWhCaUI7QUQyQm5COztBRFBBO0VBQ0UsZUFBQTtBQ1VGOztBRFJBO0VBQ0UsZUFBQTtBQ1dGOztBRFRBO0VBQ0UsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLDBCQUFBO0FDWUYiLCJmaWxlIjoic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL2d1ZXNzd29yZC1jYXJkL2d1ZXNzd29yZC1jYXJkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uLy4uL3ZhcmlhYmxlc1wiO1xuOmhvc3Qge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4OiAxIDEgMTAwJTtcbiAgaGVpZ2h0OiAxMDAlOyBtYXJnaW46IDA7IG1pbi1oZWlnaHQ6IDEwMCU7IG1pbi13aWR0aDogMTAwJTsgd2lkdGg6IDEwMCU7XG59XG5cbi5zdDB7ZmlsbDojZjQ5YWMxfVxuLnBocmFzZS10ZXh0IHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgY29sb3I6ICRncmF5LWNvbG9yICFpbXBvcnRhbnQ7XG59XG4ub3B0aW9uLXRleHQge1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogJGdyYXktY29sb3IgIWltcG9ydGFudDtcbn1cblxuLmFwcC1jYXJkLXRleHRzIHtcbiAgb3ZlcmZsb3c6IGF1dG87IG1heC1oZWlnaHQ6IDUwMHB4OyBmb250LXNpemU6IDIzcHg7XG4gIHBhZGRpbmctbGVmdDogMyU7XG4gIHBhZGRpbmctcmlnaHQ6IDMlO1xuICBvdmVyZmxvdy15OiBoaWRkZW47XG59XG4udGl0bGUge1xuICBjb2xvcjogJGZ1Y2hzaWEtY2F0ZWdvcnk7XG59XG5cblxuLnRleHQtbGV2ZWwtMSB7XG4gIGZvbnQtc2l6ZTogMjhweDtcbn1cbi50ZXh0LWxldmVsLTIge1xuICBmb250LXNpemU6IDIzcHg7XG59XG4uZ3Vlc3N3b3JkLXRpdGxlIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6ICRncmF5LWNvbG9yICFpbXBvcnRhbnQ7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xufVxuIiwiOmhvc3Qge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4OiAxIDEgMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBtYXJnaW46IDA7XG4gIG1pbi1oZWlnaHQ6IDEwMCU7XG4gIG1pbi13aWR0aDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5zdDAge1xuICBmaWxsOiAjZjQ5YWMxO1xufVxuXG4ucGhyYXNlLXRleHQge1xuICBmb250LXdlaWdodDogNTAwO1xuICBjb2xvcjogIzZkNmU3MSAhaW1wb3J0YW50O1xufVxuXG4ub3B0aW9uLXRleHQge1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzZkNmU3MSAhaW1wb3J0YW50O1xufVxuXG4uYXBwLWNhcmQtdGV4dHMge1xuICBvdmVyZmxvdzogYXV0bztcbiAgbWF4LWhlaWdodDogNTAwcHg7XG4gIGZvbnQtc2l6ZTogMjNweDtcbiAgcGFkZGluZy1sZWZ0OiAzJTtcbiAgcGFkZGluZy1yaWdodDogMyU7XG4gIG92ZXJmbG93LXk6IGhpZGRlbjtcbn1cblxuLnRpdGxlIHtcbiAgY29sb3I6ICNFQzAwOEM7XG59XG5cbi50ZXh0LWxldmVsLTEge1xuICBmb250LXNpemU6IDI4cHg7XG59XG5cbi50ZXh0LWxldmVsLTIge1xuICBmb250LXNpemU6IDIzcHg7XG59XG5cbi5ndWVzc3dvcmQtdGl0bGUge1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzZkNmU3MSAhaW1wb3J0YW50O1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbn0iLCIkZ3JlZW4tY29sb3I6ICNDRERDMjkgIWRlZmF1bHQ7XG4kb3JhbmdlLWNvbG9yOiAjRkVCRTEwICFkZWZhdWx0O1xuJGJsYWNrLWNvbG9yOiBibGFjayAhZGVmYXVsdDtcbiR2aW9sZXQtY29sb3I6ICM5MjI3OEYgIWRlZmF1bHQ7XG4kZ3JheS1jb2xvcjogIzZkNmU3MSAhZGVmYXVsdDtcblxuJGdyZWVuLWNhdGVnb3J5OiAjMDBCM0FBO1xuJGJsdWUtY2F0ZWdvcnk6ICMwMEFFRUY7XG4kZnVjaHNpYS1jYXRlZ29yeTogI0VDMDA4QztcbiRvcmFuZ2UtY2F0ZWdvcnk6ICNGQUE2MUE7XG5cbiRyZWQtc2VsZWN0LXBsYXllcjogI2VkMTk0MTtcbiRsaWdodC1ibHVlLXNlbGVjdC1wbGF5ZXI6ICMwMGFlZWY7XG4keWVsbG93LXNlbGVjdC1wbGF5ZXI6ICNmZmU2MDA7XG4kZ3JlZW4tc2VsZWN0LXBsYXllcjogIzBkYjE0YjtcbiJdfQ== */");

/***/ }),

/***/ "./src/app/cards/components/guessword-card/guessword-card.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/cards/components/guessword-card/guessword-card.component.ts ***!
  \*****************************************************************************/
/*! exports provided: GuesswordCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuesswordCardComponent", function() { return GuesswordCardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/card.service */ "./src/app/shared/services/card.service.ts");
/* harmony import */ var _shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/services/game.service */ "./src/app/shared/services/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _directives_card_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../directives/card.directive */ "./src/app/cards/directives/card.directive.ts");
/* harmony import */ var _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shared/services/audio.service */ "./src/app/shared/services/audio.service.ts");
/* harmony import */ var _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/firebase-analytics/ngx */ "./node_modules/@ionic-native/firebase-analytics/ngx/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};







var GuesswordCardComponent = /** @class */ (function (_super) {
    __extends(GuesswordCardComponent, _super);
    function GuesswordCardComponent(cardService, gameService, router, audio, firebaseAnalytics) {
        var _this = _super.call(this, cardService, gameService, router, audio, firebaseAnalytics) || this;
        _this.cardService = cardService;
        _this.gameService = gameService;
        _this.router = router;
        _this.audio = audio;
        return _this;
    }
    GuesswordCardComponent.prototype.ngOnInit = function () {
        _super.prototype.ngOnInit.call(this);
    };
    GuesswordCardComponent.prototype.correctAnswer = function () {
        if (this.answered) {
            return;
        }
        this.answered = true;
        this.addPoints();
        _super.prototype.correctPointsAnimationAndReturnToGame.call(this);
    };
    GuesswordCardComponent.prototype.wrongAnswer = function () {
        if (this.answered) {
            return;
        }
        this.answered = true;
        _super.prototype.wrongAnimation.call(this);
        _super.prototype.returnToGame.call(this);
    };
    GuesswordCardComponent.prototype.onClickOptions = function () {
        _super.prototype.onClickOptions.call(this);
    };
    GuesswordCardComponent.ctorParameters = function () { return [
        { type: _shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__["CardService"] },
        { type: _shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_5__["AudioService"] },
        { type: _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_6__["FirebaseAnalytics"] }
    ]; };
    GuesswordCardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-guessword-card',
            template: __importDefault(__webpack_require__(/*! raw-loader!./guessword-card.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/guessword-card/guessword-card.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./guessword-card.component.scss */ "./src/app/cards/components/guessword-card/guessword-card.component.scss")).default]
        }),
        __metadata("design:paramtypes", [_shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__["CardService"], _shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_5__["AudioService"],
            _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_6__["FirebaseAnalytics"]])
    ], GuesswordCardComponent);
    return GuesswordCardComponent;
}(_directives_card_directive__WEBPACK_IMPORTED_MODULE_4__["CardDirective"]));



/***/ }),

/***/ "./src/app/cards/components/options-button/options-button.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/cards/components/options-button/options-button.component.scss ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".st1 {\n  fill: #92278f;\n}\n\n.st2 {\n  fill: #6d0e6b;\n}\n\n.st3 {\n  fill: #741472;\n}\n\n.st4 {\n  fill: #fff;\n  stroke: #fff;\n  stroke-width: 0.8588;\n  stroke-miterlimit: 10;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FyZHMvY29tcG9uZW50cy9vcHRpb25zLWJ1dHRvbi9DOlxcVXNlcnNcXGhhZGFzXFxEZXNrdG9wXFxtZW50ZV9hZGl2aW5hL3NyY1xcYXBwXFxjYXJkc1xcY29tcG9uZW50c1xcb3B0aW9ucy1idXR0b25cXG9wdGlvbnMtYnV0dG9uLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL29wdGlvbnMtYnV0dG9uL29wdGlvbnMtYnV0dG9uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtBQ0NGOztBREVBO0VBQ0UsYUFBQTtBQ0NGOztBREVBO0VBQ0UsYUFBQTtBQ0NGOztBREVBO0VBQ0UsVUFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtFQUNBLHFCQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL29wdGlvbnMtYnV0dG9uL29wdGlvbnMtYnV0dG9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnN0MSB7XG4gIGZpbGw6ICM5MjI3OGZcbn1cblxuLnN0MiB7XG4gIGZpbGw6ICM2ZDBlNmJcbn1cblxuLnN0MyB7XG4gIGZpbGw6ICM3NDE0NzJcbn1cblxuLnN0NCB7XG4gIGZpbGw6ICNmZmY7XG4gIHN0cm9rZTogI2ZmZjtcbiAgc3Ryb2tlLXdpZHRoOiAuODU4ODtcbiAgc3Ryb2tlLW1pdGVybGltaXQ6IDEwXG59XG4iLCIuc3QxIHtcbiAgZmlsbDogIzkyMjc4Zjtcbn1cblxuLnN0MiB7XG4gIGZpbGw6ICM2ZDBlNmI7XG59XG5cbi5zdDMge1xuICBmaWxsOiAjNzQxNDcyO1xufVxuXG4uc3Q0IHtcbiAgZmlsbDogI2ZmZjtcbiAgc3Ryb2tlOiAjZmZmO1xuICBzdHJva2Utd2lkdGg6IDAuODU4ODtcbiAgc3Ryb2tlLW1pdGVybGltaXQ6IDEwO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/cards/components/options-button/options-button.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/cards/components/options-button/options-button.component.ts ***!
  \*****************************************************************************/
/*! exports provided: OptionsButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OptionsButtonComponent", function() { return OptionsButtonComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/card.service */ "./src/app/shared/services/card.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};


var OptionsButtonComponent = /** @class */ (function () {
    function OptionsButtonComponent(cardService) {
        this.cardService = cardService;
    }
    OptionsButtonComponent.prototype.onClick = function (btn) {
        this.cardService.shoeExtraTexts.next(true);
    };
    OptionsButtonComponent.prototype.ngOnInit = function () {
    };
    OptionsButtonComponent.ctorParameters = function () { return [
        { type: _shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__["CardService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('click', ['$event.target']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], OptionsButtonComponent.prototype, "onClick", null);
    OptionsButtonComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-options-button',
            template: __importDefault(__webpack_require__(/*! raw-loader!./options-button.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/options-button/options-button.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./options-button.component.scss */ "./src/app/cards/components/options-button/options-button.component.scss")).default]
        }),
        __metadata("design:paramtypes", [_shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__["CardService"]])
    ], OptionsButtonComponent);
    return OptionsButtonComponent;
}());



/***/ }),

/***/ "./src/app/cards/components/phrase-card/phrase-card.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/cards/components/phrase-card/phrase-card.component.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  display: flex;\n  flex: 1 1 100%;\n  height: 100%;\n  margin: 0;\n  min-height: 100%;\n  min-width: 100%;\n  width: 100%;\n}\n\n.st0 {\n  fill: #86dfd8;\n}\n\n.phrase-title {\n  font-weight: 600;\n  color: #6d6e71 !important;\n  text-decoration: underline;\n}\n\n.phrase-text {\n  font-weight: 500;\n  color: #6d6e71 !important;\n}\n\n.option-text {\n  font-weight: 600;\n  color: #6d6e71 !important;\n}\n\n.option-text-title {\n  font-weight: 600;\n  color: #6d6e71;\n}\n\n.app-card-texts {\n  overflow: auto;\n  max-height: 500px;\n  font-size: 21px;\n  padding-left: 3%;\n  padding-right: 3%;\n  overflow-y: hidden;\n}\n\n.title {\n  color: #00B3AA;\n}\n\n.text-level-1 {\n  font-size: 26px;\n}\n\n.text-level-2 {\n  font-size: 23px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FyZHMvY29tcG9uZW50cy9waHJhc2UtY2FyZC9DOlxcVXNlcnNcXGhhZGFzXFxEZXNrdG9wXFxtZW50ZV9hZGl2aW5hL3NyY1xcYXBwXFxjYXJkc1xcY29tcG9uZW50c1xccGhyYXNlLWNhcmRcXHBocmFzZS1jYXJkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL3BocmFzZS1jYXJkL3BocmFzZS1jYXJkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL3BocmFzZS1jYXJkL0M6XFxVc2Vyc1xcaGFkYXNcXERlc2t0b3BcXG1lbnRlX2FkaXZpbmEvc3JjXFx2YXJpYWJsZXMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLGFBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUFjLFNBQUE7RUFBVyxnQkFBQTtFQUFrQixlQUFBO0VBQWlCLFdBQUE7QUNHOUQ7O0FEREE7RUFBSyxhQUFBO0FDS0w7O0FESkE7RUFDRSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EsMEJBQUE7QUNPRjs7QURMQTtFQUNFLGdCQUFBO0VBQ0EseUJBQUE7QUNRRjs7QUROQTtFQUNFLGdCQUFBO0VBQ0EseUJBQUE7QUNTRjs7QURQQTtFQUNFLGdCQUFBO0VBQ0EsY0VuQlc7QUQ2QmI7O0FEUEE7RUFDRSxjQUFBO0VBQWdCLGlCQUFBO0VBQW1CLGVBQUE7RUFDbkMsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDWUY7O0FEVEE7RUFDRSxjRTVCZTtBRHdDakI7O0FEVkE7RUFDRSxlQUFBO0FDYUY7O0FEWEE7RUFDRSxlQUFBO0FDY0YiLCJmaWxlIjoic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL3BocmFzZS1jYXJkL3BocmFzZS1jYXJkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uLy4uL3ZhcmlhYmxlc1wiO1xuXG46aG9zdCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXg6IDEgMSAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7IG1hcmdpbjogMDsgbWluLWhlaWdodDogMTAwJTsgbWluLXdpZHRoOiAxMDAlOyB3aWR0aDogMTAwJTtcbn1cbi5zdDB7ZmlsbDojODZkZmQ4fVxuLnBocmFzZS10aXRsZSB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAkZ3JheS1jb2xvciAhaW1wb3J0YW50O1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbn1cbi5waHJhc2UtdGV4dCB7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGNvbG9yOiAkZ3JheS1jb2xvciAhaW1wb3J0YW50O1xufVxuLm9wdGlvbi10ZXh0IHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6ICRncmF5LWNvbG9yICFpbXBvcnRhbnQ7XG59XG4ub3B0aW9uLXRleHQtdGl0bGUge1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogJGdyYXktY29sb3I7XG59XG5cbi5hcHAtY2FyZC10ZXh0cyB7XG4gIG92ZXJmbG93OiBhdXRvOyBtYXgtaGVpZ2h0OiA1MDBweDsgZm9udC1zaXplOiAyMXB4O1xuICBwYWRkaW5nLWxlZnQ6IDMlO1xuICBwYWRkaW5nLXJpZ2h0OiAzJTtcbiAgb3ZlcmZsb3cteTogaGlkZGVuO1xufVxuQGltcG9ydCBcIi4uLy4uLy4uLy4uL3ZhcmlhYmxlc1wiO1xuLnRpdGxlIHtcbiAgY29sb3I6ICRncmVlbi1jYXRlZ29yeTtcbn1cbi50ZXh0LWxldmVsLTEge1xuICBmb250LXNpemU6IDI2cHg7XG59XG4udGV4dC1sZXZlbC0yIHtcbiAgZm9udC1zaXplOiAyM3B4O1xufVxuIiwiOmhvc3Qge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4OiAxIDEgMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBtYXJnaW46IDA7XG4gIG1pbi1oZWlnaHQ6IDEwMCU7XG4gIG1pbi13aWR0aDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5zdDAge1xuICBmaWxsOiAjODZkZmQ4O1xufVxuXG4ucGhyYXNlLXRpdGxlIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6ICM2ZDZlNzEgIWltcG9ydGFudDtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59XG5cbi5waHJhc2UtdGV4dCB7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGNvbG9yOiAjNmQ2ZTcxICFpbXBvcnRhbnQ7XG59XG5cbi5vcHRpb24tdGV4dCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjNmQ2ZTcxICFpbXBvcnRhbnQ7XG59XG5cbi5vcHRpb24tdGV4dC10aXRsZSB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjNmQ2ZTcxO1xufVxuXG4uYXBwLWNhcmQtdGV4dHMge1xuICBvdmVyZmxvdzogYXV0bztcbiAgbWF4LWhlaWdodDogNTAwcHg7XG4gIGZvbnQtc2l6ZTogMjFweDtcbiAgcGFkZGluZy1sZWZ0OiAzJTtcbiAgcGFkZGluZy1yaWdodDogMyU7XG4gIG92ZXJmbG93LXk6IGhpZGRlbjtcbn1cblxuLnRpdGxlIHtcbiAgY29sb3I6ICMwMEIzQUE7XG59XG5cbi50ZXh0LWxldmVsLTEge1xuICBmb250LXNpemU6IDI2cHg7XG59XG5cbi50ZXh0LWxldmVsLTIge1xuICBmb250LXNpemU6IDIzcHg7XG59IiwiJGdyZWVuLWNvbG9yOiAjQ0REQzI5ICFkZWZhdWx0O1xuJG9yYW5nZS1jb2xvcjogI0ZFQkUxMCAhZGVmYXVsdDtcbiRibGFjay1jb2xvcjogYmxhY2sgIWRlZmF1bHQ7XG4kdmlvbGV0LWNvbG9yOiAjOTIyNzhGICFkZWZhdWx0O1xuJGdyYXktY29sb3I6ICM2ZDZlNzEgIWRlZmF1bHQ7XG5cbiRncmVlbi1jYXRlZ29yeTogIzAwQjNBQTtcbiRibHVlLWNhdGVnb3J5OiAjMDBBRUVGO1xuJGZ1Y2hzaWEtY2F0ZWdvcnk6ICNFQzAwOEM7XG4kb3JhbmdlLWNhdGVnb3J5OiAjRkFBNjFBO1xuXG4kcmVkLXNlbGVjdC1wbGF5ZXI6ICNlZDE5NDE7XG4kbGlnaHQtYmx1ZS1zZWxlY3QtcGxheWVyOiAjMDBhZWVmO1xuJHllbGxvdy1zZWxlY3QtcGxheWVyOiAjZmZlNjAwO1xuJGdyZWVuLXNlbGVjdC1wbGF5ZXI6ICMwZGIxNGI7XG4iXX0= */");

/***/ }),

/***/ "./src/app/cards/components/phrase-card/phrase-card.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/cards/components/phrase-card/phrase-card.component.ts ***!
  \***********************************************************************/
/*! exports provided: PhraseCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhraseCardComponent", function() { return PhraseCardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/card.service */ "./src/app/shared/services/card.service.ts");
/* harmony import */ var _directives_card_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../directives/card.directive */ "./src/app/cards/directives/card.directive.ts");
/* harmony import */ var _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shared/services/game.service */ "./src/app/shared/services/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shared/services/audio.service */ "./src/app/shared/services/audio.service.ts");
/* harmony import */ var _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/firebase-analytics/ngx */ "./node_modules/@ionic-native/firebase-analytics/ngx/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};







var PhraseCardComponent = /** @class */ (function (_super) {
    __extends(PhraseCardComponent, _super);
    function PhraseCardComponent(cardService, router, gameService, audio, firebaseAnalytics) {
        var _this = _super.call(this, cardService, gameService, router, audio, firebaseAnalytics) || this;
        _this.cardService = cardService;
        _this.router = router;
        _this.gameService = gameService;
        _this.audio = audio;
        return _this;
    }
    PhraseCardComponent.prototype.ngOnInit = function () {
        _super.prototype.ngOnInit.call(this);
    };
    PhraseCardComponent.prototype.correctAnswer = function () {
        if (this.answered) {
            return;
        }
        this.answered = true;
        this.addPoints();
        _super.prototype.correctPointsAnimationAndReturnToGame.call(this);
    };
    PhraseCardComponent.prototype.wrongAnswer = function () {
        if (this.answered) {
            return;
        }
        this.answered = true;
        _super.prototype.wrongAnimation.call(this);
        _super.prototype.returnToGame.call(this);
    };
    PhraseCardComponent.prototype.onClickOptions = function () {
        _super.prototype.onClickOptions.call(this);
    };
    PhraseCardComponent.ctorParameters = function () { return [
        { type: _shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__["CardService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"] },
        { type: _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_5__["AudioService"] },
        { type: _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_6__["FirebaseAnalytics"] }
    ]; };
    PhraseCardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-phrase-card',
            template: __importDefault(__webpack_require__(/*! raw-loader!./phrase-card.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/phrase-card/phrase-card.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./phrase-card.component.scss */ "./src/app/cards/components/phrase-card/phrase-card.component.scss")).default]
        }),
        __metadata("design:paramtypes", [_shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__["CardService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"], _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_5__["AudioService"],
            _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_6__["FirebaseAnalytics"]])
    ], PhraseCardComponent);
    return PhraseCardComponent;
}(_directives_card_directive__WEBPACK_IMPORTED_MODULE_2__["CardDirective"]));



/***/ }),

/***/ "./src/app/cards/components/riddle-card/riddle-card.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/cards/components/riddle-card/riddle-card.component.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  display: flex;\n  flex: 1 1 100%;\n  height: 100%;\n  margin: 0;\n  min-height: 100%;\n  min-width: 100%;\n  width: 100%;\n}\n\n.riddle-text {\n  font-weight: 500;\n  color: #6d6e71 !important;\n}\n\n.riddle-hint {\n  font-weight: 600;\n  color: #6d6e71 !important;\n}\n\n.option-text {\n  font-weight: 600;\n  color: #6d6e71 !important;\n  font-size: 80%;\n}\n\n.st3 {\n  fill: #fdc485;\n}\n\n.title {\n  color: #FEBE10;\n}\n\n.app-card-texts {\n  overflow: auto;\n  max-height: 500px;\n  font-size: 21px;\n  padding-left: 3%;\n  padding-right: 3%;\n  overflow-y: hidden;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FyZHMvY29tcG9uZW50cy9yaWRkbGUtY2FyZC9DOlxcVXNlcnNcXGhhZGFzXFxEZXNrdG9wXFxtZW50ZV9hZGl2aW5hL3NyY1xcYXBwXFxjYXJkc1xcY29tcG9uZW50c1xccmlkZGxlLWNhcmRcXHJpZGRsZS1jYXJkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL3JpZGRsZS1jYXJkL3JpZGRsZS1jYXJkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL3JpZGRsZS1jYXJkL0M6XFxVc2Vyc1xcaGFkYXNcXERlc2t0b3BcXG1lbnRlX2FkaXZpbmEvc3JjXFx2YXJpYWJsZXMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNFLGFBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUFjLFNBQUE7RUFBVyxnQkFBQTtFQUFrQixlQUFBO0VBQWlCLFdBQUE7QUNJOUQ7O0FERkE7RUFDRSxnQkFBQTtFQUNBLHlCQUFBO0FDS0Y7O0FESEE7RUFDRSxnQkFBQTtFQUNBLHlCQUFBO0FDTUY7O0FESkE7RUFDRSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtBQ09GOztBRExBO0VBQUssYUFBQTtBQ1NMOztBRFJBO0VBQ0UsY0VwQmE7QUQrQmY7O0FEUkE7RUFDRSxjQUFBO0VBQWdCLGlCQUFBO0VBQW1CLGVBQUE7RUFDbkMsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDYUYiLCJmaWxlIjoic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL3JpZGRsZS1jYXJkL3JpZGRsZS1jYXJkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uLy4uL3ZhcmlhYmxlc1wiO1xuOmhvc3Qge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4OiAxIDEgMTAwJTtcbiAgaGVpZ2h0OiAxMDAlOyBtYXJnaW46IDA7IG1pbi1oZWlnaHQ6IDEwMCU7IG1pbi13aWR0aDogMTAwJTsgd2lkdGg6IDEwMCU7XG59XG4ucmlkZGxlLXRleHQge1xuICBmb250LXdlaWdodDogNTAwO1xuICBjb2xvcjogJGdyYXktY29sb3IgIWltcG9ydGFudDtcbn1cbi5yaWRkbGUtaGludCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAkZ3JheS1jb2xvciAhaW1wb3J0YW50O1xufVxuLm9wdGlvbi10ZXh0IHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6ICRncmF5LWNvbG9yICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogODAlO1xufVxuLnN0M3tmaWxsOiNmZGM0ODV9XG4udGl0bGUge1xuICBjb2xvcjogJG9yYW5nZS1jb2xvcjtcbn1cblxuLmFwcC1jYXJkLXRleHRzIHtcbiAgb3ZlcmZsb3c6IGF1dG87IG1heC1oZWlnaHQ6IDUwMHB4OyBmb250LXNpemU6IDIxcHg7XG4gIHBhZGRpbmctbGVmdDogMyU7XG4gIHBhZGRpbmctcmlnaHQ6IDMlO1xuICBvdmVyZmxvdy15OiBoaWRkZW47XG59XG5cbiIsIjpob3N0IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleDogMSAxIDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgbWFyZ2luOiAwO1xuICBtaW4taGVpZ2h0OiAxMDAlO1xuICBtaW4td2lkdGg6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ucmlkZGxlLXRleHQge1xuICBmb250LXdlaWdodDogNTAwO1xuICBjb2xvcjogIzZkNmU3MSAhaW1wb3J0YW50O1xufVxuXG4ucmlkZGxlLWhpbnQge1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzZkNmU3MSAhaW1wb3J0YW50O1xufVxuXG4ub3B0aW9uLXRleHQge1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzZkNmU3MSAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDgwJTtcbn1cblxuLnN0MyB7XG4gIGZpbGw6ICNmZGM0ODU7XG59XG5cbi50aXRsZSB7XG4gIGNvbG9yOiAjRkVCRTEwO1xufVxuXG4uYXBwLWNhcmQtdGV4dHMge1xuICBvdmVyZmxvdzogYXV0bztcbiAgbWF4LWhlaWdodDogNTAwcHg7XG4gIGZvbnQtc2l6ZTogMjFweDtcbiAgcGFkZGluZy1sZWZ0OiAzJTtcbiAgcGFkZGluZy1yaWdodDogMyU7XG4gIG92ZXJmbG93LXk6IGhpZGRlbjtcbn0iLCIkZ3JlZW4tY29sb3I6ICNDRERDMjkgIWRlZmF1bHQ7XG4kb3JhbmdlLWNvbG9yOiAjRkVCRTEwICFkZWZhdWx0O1xuJGJsYWNrLWNvbG9yOiBibGFjayAhZGVmYXVsdDtcbiR2aW9sZXQtY29sb3I6ICM5MjI3OEYgIWRlZmF1bHQ7XG4kZ3JheS1jb2xvcjogIzZkNmU3MSAhZGVmYXVsdDtcblxuJGdyZWVuLWNhdGVnb3J5OiAjMDBCM0FBO1xuJGJsdWUtY2F0ZWdvcnk6ICMwMEFFRUY7XG4kZnVjaHNpYS1jYXRlZ29yeTogI0VDMDA4QztcbiRvcmFuZ2UtY2F0ZWdvcnk6ICNGQUE2MUE7XG5cbiRyZWQtc2VsZWN0LXBsYXllcjogI2VkMTk0MTtcbiRsaWdodC1ibHVlLXNlbGVjdC1wbGF5ZXI6ICMwMGFlZWY7XG4keWVsbG93LXNlbGVjdC1wbGF5ZXI6ICNmZmU2MDA7XG4kZ3JlZW4tc2VsZWN0LXBsYXllcjogIzBkYjE0YjtcbiJdfQ== */");

/***/ }),

/***/ "./src/app/cards/components/riddle-card/riddle-card.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/cards/components/riddle-card/riddle-card.component.ts ***!
  \***********************************************************************/
/*! exports provided: RiddleCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RiddleCardComponent", function() { return RiddleCardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_game_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/game.service */ "./src/app/shared/services/game.service.ts");
/* harmony import */ var _shared_services_card_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/services/card.service */ "./src/app/shared/services/card.service.ts");
/* harmony import */ var _directives_card_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../directives/card.directive */ "./src/app/cards/directives/card.directive.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shared/services/audio.service */ "./src/app/shared/services/audio.service.ts");
/* harmony import */ var _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/firebase-analytics/ngx */ "./node_modules/@ionic-native/firebase-analytics/ngx/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};







var RiddleCardComponent = /** @class */ (function (_super) {
    __extends(RiddleCardComponent, _super);
    function RiddleCardComponent(cardService, router, gameService, audio, firebaseAnalytics) {
        var _this = _super.call(this, cardService, gameService, router, audio, firebaseAnalytics) || this;
        _this.cardService = cardService;
        _this.router = router;
        _this.gameService = gameService;
        _this.audio = audio;
        return _this;
    }
    RiddleCardComponent.prototype.ngOnInit = function () {
        _super.prototype.ngOnInit.call(this);
    };
    RiddleCardComponent.prototype.correctAnswer = function () {
        if (this.answered) {
            return;
        }
        this.answered = true;
        _super.prototype.addPoints.call(this);
        _super.prototype.correctPointsAnimationAndReturnToGame.call(this);
    };
    RiddleCardComponent.prototype.wrongAnswer = function () {
        if (this.answered) {
            return;
        }
        this.answered = true;
        _super.prototype.wrongAnimation.call(this);
        _super.prototype.returnToGame.call(this);
    };
    RiddleCardComponent.prototype.onClickOptions = function () {
        _super.prototype.onClickOptions.call(this);
    };
    RiddleCardComponent.ctorParameters = function () { return [
        { type: _shared_services_card_service__WEBPACK_IMPORTED_MODULE_2__["CardService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _shared_services_game_service__WEBPACK_IMPORTED_MODULE_1__["GameService"] },
        { type: _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_5__["AudioService"] },
        { type: _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_6__["FirebaseAnalytics"] }
    ]; };
    RiddleCardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-riddle-card',
            template: __importDefault(__webpack_require__(/*! raw-loader!./riddle-card.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/riddle-card/riddle-card.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./riddle-card.component.scss */ "./src/app/cards/components/riddle-card/riddle-card.component.scss")).default]
        }),
        __metadata("design:paramtypes", [_shared_services_card_service__WEBPACK_IMPORTED_MODULE_2__["CardService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _shared_services_game_service__WEBPACK_IMPORTED_MODULE_1__["GameService"], _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_5__["AudioService"],
            _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_6__["FirebaseAnalytics"]])
    ], RiddleCardComponent);
    return RiddleCardComponent;
}(_directives_card_directive__WEBPACK_IMPORTED_MODULE_3__["CardDirective"]));



/***/ }),

/***/ "./src/app/cards/components/spelling-card/spelling-card.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/cards/components/spelling-card/spelling-card.component.scss ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  display: flex;\n  flex: 1 1 100%;\n  height: 100%;\n  margin: 0;\n  min-height: 100%;\n  min-width: 100%;\n  width: 100%;\n}\n\n.st3 {\n  fill: #6dcff6;\n}\n\n.spelling-text {\n  font-weight: 500;\n  color: #6d6e71 !important;\n}\n\n.app-card-texts {\n  overflow: auto;\n  max-height: 500px;\n  font-size: 36px;\n  padding-left: 3%;\n  padding-right: 3%;\n  overflow-y: hidden;\n}\n\n.title {\n  color: #00AEEF;\n}\n\n.statement {\n  font-weight: 500;\n  color: #6d6e71 !important;\n  text-decoration: underline;\n  font-size: 30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FyZHMvY29tcG9uZW50cy9zcGVsbGluZy1jYXJkL0M6XFxVc2Vyc1xcaGFkYXNcXERlc2t0b3BcXG1lbnRlX2FkaXZpbmEvc3JjXFxhcHBcXGNhcmRzXFxjb21wb25lbnRzXFxzcGVsbGluZy1jYXJkXFxzcGVsbGluZy1jYXJkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jYXJkcy9jb21wb25lbnRzL3NwZWxsaW5nLWNhcmQvc3BlbGxpbmctY2FyZC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY2FyZHMvY29tcG9uZW50cy9zcGVsbGluZy1jYXJkL0M6XFxVc2Vyc1xcaGFkYXNcXERlc2t0b3BcXG1lbnRlX2FkaXZpbmEvc3JjXFx2YXJpYWJsZXMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLGFBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUFjLFNBQUE7RUFBVyxnQkFBQTtFQUFrQixlQUFBO0VBQWlCLFdBQUE7QUNHOUQ7O0FEREE7RUFBSyxhQUFBO0FDS0w7O0FESkE7RUFDRSxnQkFBQTtFQUNBLHlCQUFBO0FDT0Y7O0FESkE7RUFDRSxjQUFBO0VBQWdCLGlCQUFBO0VBQW1CLGVBQUE7RUFDbkMsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDU0Y7O0FEUEE7RUFDRSxjRWJjO0FEdUJoQjs7QURSQTtFQUNFLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSwwQkFBQTtFQUNBLGVBQUE7QUNXRiIsImZpbGUiOiJzcmMvYXBwL2NhcmRzL2NvbXBvbmVudHMvc3BlbGxpbmctY2FyZC9zcGVsbGluZy1jYXJkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uLy4uL3ZhcmlhYmxlc1wiO1xuXG46aG9zdCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXg6IDEgMSAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7IG1hcmdpbjogMDsgbWluLWhlaWdodDogMTAwJTsgbWluLXdpZHRoOiAxMDAlOyB3aWR0aDogMTAwJTtcbn1cbi5zdDN7ZmlsbDojNmRjZmY2fVxuLnNwZWxsaW5nLXRleHQge1xuICBmb250LXdlaWdodDogNTAwO1xuICBjb2xvcjogJGdyYXktY29sb3IgIWltcG9ydGFudDtcbn1cblxuLmFwcC1jYXJkLXRleHRzIHtcbiAgb3ZlcmZsb3c6IGF1dG87IG1heC1oZWlnaHQ6IDUwMHB4OyBmb250LXNpemU6IDM2cHg7XG4gIHBhZGRpbmctbGVmdDogMyU7XG4gIHBhZGRpbmctcmlnaHQ6IDMlO1xuICBvdmVyZmxvdy15OiBoaWRkZW47XG59XG4udGl0bGUge1xuICBjb2xvcjogJGJsdWUtY2F0ZWdvcnk7XG59XG4uc3RhdGVtZW50IHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgY29sb3I6ICRncmF5LWNvbG9yICFpbXBvcnRhbnQ7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuICBmb250LXNpemU6IDMwcHg7XG59XG4iLCI6aG9zdCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXg6IDEgMSAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIG1hcmdpbjogMDtcbiAgbWluLWhlaWdodDogMTAwJTtcbiAgbWluLXdpZHRoOiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLnN0MyB7XG4gIGZpbGw6ICM2ZGNmZjY7XG59XG5cbi5zcGVsbGluZy10ZXh0IHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgY29sb3I6ICM2ZDZlNzEgIWltcG9ydGFudDtcbn1cblxuLmFwcC1jYXJkLXRleHRzIHtcbiAgb3ZlcmZsb3c6IGF1dG87XG4gIG1heC1oZWlnaHQ6IDUwMHB4O1xuICBmb250LXNpemU6IDM2cHg7XG4gIHBhZGRpbmctbGVmdDogMyU7XG4gIHBhZGRpbmctcmlnaHQ6IDMlO1xuICBvdmVyZmxvdy15OiBoaWRkZW47XG59XG5cbi50aXRsZSB7XG4gIGNvbG9yOiAjMDBBRUVGO1xufVxuXG4uc3RhdGVtZW50IHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgY29sb3I6ICM2ZDZlNzEgIWltcG9ydGFudDtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG4gIGZvbnQtc2l6ZTogMzBweDtcbn0iLCIkZ3JlZW4tY29sb3I6ICNDRERDMjkgIWRlZmF1bHQ7XG4kb3JhbmdlLWNvbG9yOiAjRkVCRTEwICFkZWZhdWx0O1xuJGJsYWNrLWNvbG9yOiBibGFjayAhZGVmYXVsdDtcbiR2aW9sZXQtY29sb3I6ICM5MjI3OEYgIWRlZmF1bHQ7XG4kZ3JheS1jb2xvcjogIzZkNmU3MSAhZGVmYXVsdDtcblxuJGdyZWVuLWNhdGVnb3J5OiAjMDBCM0FBO1xuJGJsdWUtY2F0ZWdvcnk6ICMwMEFFRUY7XG4kZnVjaHNpYS1jYXRlZ29yeTogI0VDMDA4QztcbiRvcmFuZ2UtY2F0ZWdvcnk6ICNGQUE2MUE7XG5cbiRyZWQtc2VsZWN0LXBsYXllcjogI2VkMTk0MTtcbiRsaWdodC1ibHVlLXNlbGVjdC1wbGF5ZXI6ICMwMGFlZWY7XG4keWVsbG93LXNlbGVjdC1wbGF5ZXI6ICNmZmU2MDA7XG4kZ3JlZW4tc2VsZWN0LXBsYXllcjogIzBkYjE0YjtcbiJdfQ== */");

/***/ }),

/***/ "./src/app/cards/components/spelling-card/spelling-card.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/cards/components/spelling-card/spelling-card.component.ts ***!
  \***************************************************************************/
/*! exports provided: SpellingCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpellingCardComponent", function() { return SpellingCardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/card.service */ "./src/app/shared/services/card.service.ts");
/* harmony import */ var _directives_card_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../directives/card.directive */ "./src/app/cards/directives/card.directive.ts");
/* harmony import */ var _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shared/services/game.service */ "./src/app/shared/services/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shared/services/audio.service */ "./src/app/shared/services/audio.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/firebase-analytics/ngx */ "./node_modules/@ionic-native/firebase-analytics/ngx/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};









var SpellingCardComponent = /** @class */ (function (_super) {
    __extends(SpellingCardComponent, _super);
    function SpellingCardComponent(cardService, router, gameService, audio, firebaseAnalytics) {
        var _this = _super.call(this, cardService, gameService, router, audio, firebaseAnalytics) || this;
        _this.cardService = cardService;
        _this.router = router;
        _this.gameService = gameService;
        _this.audio = audio;
        return _this;
    }
    SpellingCardComponent.prototype.ngOnInit = function () {
        _super.prototype.ngOnInit.call(this);
        // la primera siempre es lvl 2
        this.currentWordIndex = 1;
    };
    SpellingCardComponent.prototype.correctAnswer = function () {
        var _this = this;
        if (this.answered) {
            return;
        }
        this.answered = true;
        if (this.currentWordIndex === 1) {
            this.currentWordIndex = 2;
            Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["timer"])(2000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["take"])(1)).subscribe(function (value) {
                _this.answered = false;
            });
            this.audio.playSfx('476178__unadamlar__correct-choice.wav');
        }
        else {
            _super.prototype.correctPointsAnimationAndReturnToGame.call(this);
            _super.prototype.addPoints.call(this);
        }
    };
    SpellingCardComponent.prototype.wrongAnswer = function () {
        var _this = this;
        if (this.answered) {
            return;
        }
        this.answered = true;
        this.pointsToReward--;
        if (this.currentWordIndex === 1) {
            this.currentWordIndex = 0;
            Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["timer"])(2000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["take"])(1)).subscribe(function (value) {
                _this.answered = false;
            });
        }
        else {
            if (this.pointsToReward > 0) {
                _super.prototype.addPoints.call(this);
                this.correctPointsAnimationAndReturnToGame();
            }
            else {
                _super.prototype.returnToGame.call(this);
            }
        }
        _super.prototype.wrongAnimation.call(this);
    };
    SpellingCardComponent.ctorParameters = function () { return [
        { type: _shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__["CardService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"] },
        { type: _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_5__["AudioService"] },
        { type: _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_8__["FirebaseAnalytics"] }
    ]; };
    SpellingCardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-spelling-card',
            template: __importDefault(__webpack_require__(/*! raw-loader!./spelling-card.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/spelling-card/spelling-card.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./spelling-card.component.scss */ "./src/app/cards/components/spelling-card/spelling-card.component.scss")).default]
        }),
        __metadata("design:paramtypes", [_shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__["CardService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _shared_services_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"], _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_5__["AudioService"],
            _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_8__["FirebaseAnalytics"]])
    ], SpellingCardComponent);
    return SpellingCardComponent;
}(_directives_card_directive__WEBPACK_IMPORTED_MODULE_2__["CardDirective"]));



/***/ }),

/***/ "./src/app/cards/components/wrong-button/wrong-button.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/cards/components/wrong-button/wrong-button.component.scss ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NhcmRzL2NvbXBvbmVudHMvd3JvbmctYnV0dG9uL3dyb25nLWJ1dHRvbi5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/cards/components/wrong-button/wrong-button.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/cards/components/wrong-button/wrong-button.component.ts ***!
  \*************************************************************************/
/*! exports provided: WrongButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WrongButtonComponent", function() { return WrongButtonComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

var WrongButtonComponent = /** @class */ (function () {
    function WrongButtonComponent() {
    }
    WrongButtonComponent.prototype.ngOnInit = function () {
    };
    WrongButtonComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-wrong-button',
            template: __importDefault(__webpack_require__(/*! raw-loader!./wrong-button.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cards/components/wrong-button/wrong-button.component.html")).default,
            styles: [__importDefault(__webpack_require__(/*! ./wrong-button.component.scss */ "./src/app/cards/components/wrong-button/wrong-button.component.scss")).default]
        }),
        __metadata("design:paramtypes", [])
    ], WrongButtonComponent);
    return WrongButtonComponent;
}());



/***/ }),

/***/ "./src/app/cards/directives/card.directive.ts":
/*!****************************************************!*\
  !*** ./src/app/cards/directives/card.directive.ts ***!
  \****************************************************/
/*! exports provided: CardDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardDirective", function() { return CardDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/services/card.service */ "./src/app/shared/services/card.service.ts");
/* harmony import */ var _shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/game.service */ "./src/app/shared/services/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/services/audio.service */ "./src/app/shared/services/audio.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/firebase-analytics/ngx */ "./node_modules/@ionic-native/firebase-analytics/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};








var CardDirective = /** @class */ (function () {
    function CardDirective(cardService, gameService, router, audio, firebaseAnalytics) {
        this.cardService = cardService;
        this.gameService = gameService;
        this.router = router;
        this.audio = audio;
        this.firebaseAnalytics = firebaseAnalytics;
    }
    CardDirective.prototype.ngOnInit = function () {
        this.card = this.cardService.currentCard;
        this.cardService.shoeExtraTexts.next(false);
        this.pointsToReward = 2;
    };
    CardDirective.prototype.correctPointsAnimationAndReturnToGame = function () {
        var _this = this;
        this.cardService.playStarAnimation.emit(this.pointsToReward);
        if (this.pointsToReward === 1) {
            this.audio.playSfx('476178__unadamlar__correct-choice.wav');
        }
        else {
            this.audio.playSfx('131660__bertrof__game-sound-correct.wav');
        }
        Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["timer"])(2000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1)).subscribe(function (value) {
            _this.returnToGame();
        });
    };
    CardDirective.prototype.addPoints = function () {
        // this.firebaseAnalytics.logEvent('solved_card', {'game_id': this.gameService.gameId, 'card_name': this.card.cardName,
        //                                                               'game_mode': this.gameService.gameMode,
        //                                                               'level': this.card.level, 'points': this.pointsToReward});
        this.gameService.addPoints(this.pointsToReward);
    };
    CardDirective.prototype.wrongAnimation = function () {
        this.audio.playSfx('131657__bertrof__game-sound-wrong.wav');
    };
    CardDirective.prototype.onClickOptions = function () {
        if (this.answered) {
            return;
        }
        this.audio.playClick();
        this.pointsToReward--;
    };
    CardDirective.prototype.returnToGame = function () {
        this.cardService.currentCardIsAnswered = true;
        this.gameService.backToGame();
    };
    CardDirective.ctorParameters = function () { return [
        { type: _shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__["CardService"] },
        { type: _shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_4__["AudioService"] },
        { type: _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_7__["FirebaseAnalytics"] }
    ]; };
    CardDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appCard]'
        }),
        __metadata("design:paramtypes", [_shared_services_card_service__WEBPACK_IMPORTED_MODULE_1__["CardService"], _shared_services_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _shared_services_audio_service__WEBPACK_IMPORTED_MODULE_4__["AudioService"],
            _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_7__["FirebaseAnalytics"]])
    ], CardDirective);
    return CardDirective;
}());



/***/ }),

/***/ "./src/app/guards/exit-from-card.guard.ts":
/*!************************************************!*\
  !*** ./src/app/guards/exit-from-card.guard.ts ***!
  \************************************************/
/*! exports provided: ExitFromCardGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExitFromCardGuard", function() { return ExitFromCardGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

var ExitFromCardGuard = /** @class */ (function () {
    function ExitFromCardGuard() {
    }
    ExitFromCardGuard.prototype.canActivate = function (next, state) {
        return true;
    };
    ExitFromCardGuard.prototype.canDeactivate = function (component, currentRoute, currentState, nextState) {
        return nextState.url.includes('settings') || component.canDeactivate();
    };
    ExitFromCardGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        })
    ], ExitFromCardGuard);
    return ExitFromCardGuard;
}());



/***/ })

}]);
//# sourceMappingURL=cards-cards-module.js.map