import { app, BrowserWindow, ipcMain } from 'electron';
import { machineId, machineIdSync } from 'node-machine-id';
import * as path from 'path';
import * as url from 'url';
import * as fs from 'fs';
import * as crypto from 'crypto';

const algorithm = 'aes-256-ctr';
let key = crypto.createHash('sha256').update('divermente').digest('base64').substr(0, 32);

const encrypt = (buffer) => {
    const iv = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv(algorithm, key, iv);
    const result = Buffer.concat([iv, cipher.update(buffer), cipher.final()]);
    return result;
};

const decrypt = (encrypted) => {
   const iv = encrypted.slice(0, 16);
   encrypted = encrypted.slice(16);
   const decipher = crypto.createDecipheriv(algorithm, key, iv);
   const result = Buffer.concat([decipher.update(encrypted), decipher.final()]);
   return result;
};

let win: BrowserWindow;

function createWindow() {
   win = new BrowserWindow({
     width: 1080,
     height: 720,
     webPreferences: {
        nodeIntegration: true
      }
   });
   win.setMenuBarVisibility(false);
   // win.webContents.openDevTools();

   let id = machineIdSync(true);

   fs.readFile(path.join(__dirname, `/../../../system.dm`), function (err, data) {
     if (err) {
        win.loadURL(
            url.format({
                pathname: path.join(__dirname, `/licence.html`),
                protocol: 'file:',
                slashes: true
            })
        );
     } else {
        try {
          let datu = decrypt(data);
          var dat = JSON.parse(datu.toString());
          if(dat.uuid === id) {
            win.loadURL(
                url.format({
                    pathname: path.join(__dirname, `/../../dist/app/index.html`),
                    protocol: 'file:',
                    slashes: true
                })
            );
          } else {
            fs.unlink(path.join(__dirname, `/../../../system.dm`), (err) => {
              if (err) throw err;
            });
            win.loadURL(
                url.format({
                    pathname: path.join(__dirname, `/licence.html`),
                    protocol: 'file:',
                    slashes: true
                })
            );
          }
        } catch(error) {
          fs.unlink(path.join(__dirname, `/../../../system.dm`), (err) => {
            if (err) throw err;
          });
          win.loadURL(
              url.format({
                  pathname: path.join(__dirname, `/licence.html`),
                  protocol: 'file:',
                  slashes: true
              })
          );
        }
     }
      
   });

    win.on('closed', () => {
        win = null;
    });
}

app.on('ready', createWindow);

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
})

ipcMain.on('request-mainprocess-register', (event, arg) => {
  var hey = encrypt(Buffer.from(JSON.stringify(arg)));
  fs.writeFile(path.join(__dirname, `/../../../system.dm`), hey, (err) => {
    if (err) {
        return;
    }
  });
});
ipcMain.on('request-mainprocess-action', (event, arg) => {
    win.loadURL(
        url.format({
            pathname: path.join(__dirname, `/../../dist/app/index.html`),
            protocol: 'file:',
            slashes: true
        })
    );
});

ipcMain.on('request-mainprocess-delete', (event, arg) => {
  fs.readFile(path.join(__dirname, `/../../../system.dm`), function (err, data) {
    if (err) {
       return;
    } else {
       try {
          fs.unlink(path.join(__dirname, `/../../../system.dm`), (err) => {
            if (err) throw err;
          });
        } catch(error) {
          return;
        }
    }
  });      
});