"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var node_machine_id_1 = require("node-machine-id");
var path = require("path");
var url = require("url");
var fs = require("fs");
var crypto = require("crypto");
var algorithm = 'aes-256-ctr';
var key = crypto.createHash('sha256').update('divermente').digest('base64').substr(0, 32);
var encrypt = function (buffer) {
    var iv = crypto.randomBytes(16);
    var cipher = crypto.createCipheriv(algorithm, key, iv);
    var result = Buffer.concat([iv, cipher.update(buffer), cipher.final()]);
    return result;
};
var decrypt = function (encrypted) {
    var iv = encrypted.slice(0, 16);
    encrypted = encrypted.slice(16);
    var decipher = crypto.createDecipheriv(algorithm, key, iv);
    var result = Buffer.concat([decipher.update(encrypted), decipher.final()]);
    return result;
};
var win;
function createWindow() {
    win = new electron_1.BrowserWindow({
        width: 1080,
        height: 720,
        webPreferences: {
            nodeIntegration: true
        }
    });
    win.setMenuBarVisibility(false);
    // win.webContents.openDevTools();
    var id = node_machine_id_1.machineIdSync(true);
    fs.readFile(path.join(__dirname, "/../../../system.dm"), function (err, data) {
        if (err) {
            win.loadURL(url.format({
                pathname: path.join(__dirname, "/licence.html"),
                protocol: 'file:',
                slashes: true
            }));
        }
        else {
            try {
                var datu = decrypt(data);
                var dat = JSON.parse(datu.toString());
                if (dat.uuid === id) {
                    win.loadURL(url.format({
                        pathname: path.join(__dirname, "/../../dist/app/index.html"),
                        protocol: 'file:',
                        slashes: true
                    }));
                }
                else {
                    fs.unlink(path.join(__dirname, "/../../../system.dm"), function (err) {
                        if (err)
                            throw err;
                    });
                    win.loadURL(url.format({
                        pathname: path.join(__dirname, "/licence.html"),
                        protocol: 'file:',
                        slashes: true
                    }));
                }
            }
            catch (error) {
                fs.unlink(path.join(__dirname, "/../../../system.dm"), function (err) {
                    if (err)
                        throw err;
                });
                win.loadURL(url.format({
                    pathname: path.join(__dirname, "/licence.html"),
                    protocol: 'file:',
                    slashes: true
                }));
            }
        }
    });
    win.on('closed', function () {
        win = null;
    });
}
electron_1.app.on('ready', createWindow);
electron_1.app.on('activate', function () {
    if (win === null) {
        createWindow();
    }
});
electron_1.ipcMain.on('request-mainprocess-register', function (event, arg) {
    var hey = encrypt(Buffer.from(JSON.stringify(arg)));
    fs.writeFile(path.join(__dirname, "/../../../system.dm"), hey, function (err) {
        if (err) {
            return;
        }
    });
});
electron_1.ipcMain.on('request-mainprocess-action', function (event, arg) {
    win.loadURL(url.format({
        pathname: path.join(__dirname, "/../../dist/app/index.html"),
        protocol: 'file:',
        slashes: true
    }));
});
electron_1.ipcMain.on('request-mainprocess-delete', function (event, arg) {
    fs.readFile(path.join(__dirname, "/../../../system.dm"), function (err, data) {
        if (err) {
            return;
        }
        else {
            try {
                fs.unlink(path.join(__dirname, "/../../../system.dm"), function (err) {
                    if (err)
                        throw err;
                });
            }
            catch (error) {
                return;
            }
        }
    });
});
//# sourceMappingURL=main.js.map