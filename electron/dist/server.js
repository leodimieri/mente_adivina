let data = {};
var butValid = document.getElementById('botonValidar');
var ibu = document.getElementById('initJuego');
var tryA = document.getElementById('tryA');
tryA.onclick = function() {
    ipcRenderer.send('request-mainprocess-delete');
    data = {};
    document.getElementById('incorrect').style.display = 'none';
    document.getElementById('mainContent').style.display = 'block';
};
butValid.onclick = function() {
    data = {
        email: document.getElementById('emailField').value,
        key: document.getElementById('keyField').value
    };

    if(data.email!="" && data.key!="") {
        document.getElementById('mainContent').style.display = 'none';
        document.getElementById('loader').style.display = 'block';

        axios.post('https://divermente.com/licencias/api/activate', {
            email: data.email,
            key: data.key,
            uuid: machineIdSync(true)
        }).then(function(res) {
            console.log(res);
            if(res.data.active) {
                ipcRenderer.send('request-mainprocess-register', {email: data.email, key: data.key, uuid: machineIdSync(true)});
                toastr.success("¡Listo! Registraste el juego correctamente.");
                document.getElementById('correct').style.display = 'block';
                document.getElementById('loader').style.display = 'none';
            } else {
                toastr.error('La clave es incorrecta o expiró.');
                document.getElementById('incorrect').style.display = 'block';
                document.getElementById('loader').style.display = 'none';    
            }
        }).catch(function(error) {
            console.log(error);
            toastr.error('La clave es incorrecta o expiró.');
            document.getElementById('incorrect').style.display = 'block';
            document.getElementById('loader').style.display = 'none';
        })

    } else {
        toastr.error('Faltan datos');
    }
    
    //console.log(data);
}

ibu.onclick = function() {
    ipcRenderer.send('request-mainprocess-action');
}